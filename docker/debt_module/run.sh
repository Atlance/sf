#!/bin/sh

# Detect action
ACTION="$1"

IMAGE_NAME="$2"
if [ x$IMAGE_NAME = x ] ; then
	IMAGE_NAME="debt_module_app"
fi

CONTAINER_NAME="$3"
if [ x$CONTAINER_NAME = x ] ; then
	CONTAINER_NAME="debt_module_app"
fi

# Web server nginx port
WEBPORT="$4"
if [ x$WEBPORT = x ] ; then
	WEBPORT=8080
fi

# SSH port
SSHPORT="$5"
if [ x$SSHPORT = x ] ; then
	SSHPORT=2220
fi

WEBROOT=`pwd`

if [ "$ACTION" = "clean" ] ; then
    docker stop $CONTAINER_NAME && docker rm $CONTAINER_NAME && docker rmi $IMAGE_NAME
elif [ "$ACTION" = "build" ] ; then
    notify-send 'Build start...'
	docker build -f docker/Dockerfile -t $IMAGE_NAME .

	docker run -d -p $WEBPORT:80 -p $SSHPORT:22 -i -t --name=$CONTAINER_NAME $IMAGE_NAME /bin/bash
	docker exec -i -t $CONTAINER_NAME /start_scripts/start.sh

	notify-send 'Composer install...'
    docker exec -u www-data $CONTAINER_NAME bash -c "cd /var/www/debt_module && composer install"

    notify-send 'Database install...'
    docker exec -u www-data $CONTAINER_NAME bash -c "cd /var/www/debt_module && php bin/console cache:clear --env=prod"
    docker exec -u www-data $CONTAINER_NAME bash -c "cd /var/www/debt_module && php bin/console d:d:c --env=prod"

    notify-send 'Build complete'
else
	echo "Main commands:"
	echo " - create docker image and run container:"
	echo "   ./run.sh build [webport] [sshport]"
	echo "   [webport] is optional, default is 8080 (for nginx)"
	echo "   [sshport] is optional, default is 2220 (for sshd)"
	echo " - run docker container:"
	echo "   ./run.sh container [webport] [sshport]"
	echo "   [port] is optional, default is 8080 (for nginx)"
	echo "   [sshport] is optional, default is 2220 (for sshd)"
	echo " - start container:"
	echo "   ./run.sh start"
	echo " - stop container:"
	echo "   ./run.sh stop"
	echo " - connect to container:"
	echo "   ./run.sh connect"
	echo " - remove container:"
	echo "   ./run.sh remove"
	echo " - stop and remove container, then remove docker image:"
	echo "   ./run.sh clean"
fi
