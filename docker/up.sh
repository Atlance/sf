#!/bin/sh

nginx_port="8083"
database_port="3317"
xdebug_port="9090"
xdebug_enabled=""
OS="`uname`"

while test $# -gt 0; do
    case "$1" in
        -n|--nginx-port)
            shift
            if [ ! -z "$1" ]; then
                nginx_port=$1
            fi
            shift
            ;;
        -p|--database-port)
            shift
            if [ ! -z "$1" ]; then
                database_port=$1
            fi
            shift
            ;;
        --xdebug-port)
            shift
            if [ ! -z "$1" ]; then
                xdebug_port=$1
            fi
            shift
            ;;
        --xdebug-enabled)
            shift
            if [ ! -z "$1" ]; then
                xdebug_enabled="RUN pecl install xdebug-2.5.0 \&\& docker-php-ext-enable xdebug"
            fi
            shift
            ;;
        *)
            echo "Unknown $1 param"
            exit
            break
            ;;
    esac
done

# avoid permission conflicts with user at host machine
uid=$(id -u)
if [ "$uid" -gt 100000 ]; then
	uid=1000
fi

cp -n ../.env.dist ../.env
cp debt_module/Dockerfile.dist debt_module/Dockerfile
cp nginx/Dockerfile.dist nginx/Dockerfile
cp docker-compose.yml.dist docker-compose.yml

if [ "$OS" == "Darwin" ]; then
    sed -i "" -e "s/\$XDEBUG_INSTALL/$xdebug_enabled/g" app/Dockerfile
    sed -i "" -e "s/\$USER_ID/$uid/g" app/Dockerfile
    sed -i "" -e "s/\$NGINX_PORT/$nginx_port/g" docker-compose.yml
    sed -i "" -e "s/\$XDEBUG_REMOTE_PORT/$xdebug_port/g" docker-compose.yml
    sed -i "" -e "s/\$DATABASE_PORT/$database_port/g" docker-compose.yml
    sed -i "" -e "s/:nocopy//g" docker-compose.yml

    cp docker-compose-dev.yml.dist docker-compose-dev.yml
    sed -i "" -e "s/\$APP_VOLUME/.\//g" docker-compose.yml
    sed -i "" -e "s/\$APP_VOLUME/lkk_app_sync/g" docker-compose-dev.yml

    docker-sync start
    docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d
else
    sed -i "s/\$XDEBUG_INSTALL/$xdebug_enabled/g" debt_module/Dockerfile
    sed -i "s/\$USER_ID/$uid/g" debt_module/Dockerfile
    sed -i "s/\$NGINX_PORT/$nginx_port/g" docker-compose.yml
    sed -i "s/\$XDEBUG_REMOTE_PORT/$xdebug_port/g" docker-compose.yml
    sed -i "s/\$DATABASE_PORT/$database_port/g" docker-compose.yml
    sed -i "s/:nocopy//g" docker-compose.yml
    sed -i "s/\$APP_VOLUME/.\//g" docker-compose.yml

    docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d --build --force-recreate
fi

docker-compose exec -T debt_module_app bash -c "rm -r var/cache/*"
docker-compose exec -T debt_module_app bash -c "sudo -uwww-data composer install"
docker-compose exec -T debt_module_app bash -c "bin/console d:d:c --if-not-exists"
docker-compose exec -T debt_module_app bash -c "bin/console d:m:m"
docker-compose exec -T debt_module_app bash -c "cp -n .env.dist .env"
