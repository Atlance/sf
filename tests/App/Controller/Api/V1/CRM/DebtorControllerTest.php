<?php

declare(strict_types=1);

namespace App\Tests\App\Controller\Api\V1;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DebtorControllerTest.
 */
class DebtorControllerTest extends WebTestCase
{
    /**
     * @throws \Exception
     */
    public function testListAction()
    {
        $client = static::createClient([], [
            'HTTP_AUTHORIZATION' => 'Basic '.\base64_encode('test:'.\getenv('APP_SECRET')),
            'CONTENT_TYPE' => 'application/json',
        ]);
        $uri = '/api/v1/crm/debtor/list';

        $client->request('GET', $uri);
        $response = $client->getResponse();

        $this->assertJson($response->getContent(), 'response content not json');
        $this->assertSame(Response::HTTP_OK, $response->getStatusCode(), 'unexpected response status code');
        $content = \json_decode($client->getResponse()->getContent());
        $this->assertTrue(true === $content->success, 'unexpected response');
        $this->assertTrue(\array_key_exists('items', $content->data), 'missing expected argument');
        $this->assertTrue(\array_key_exists('total', $content->data), 'missing expected argument');
    }
}
