<?php

declare(strict_types=1);

namespace App\Tests\App\Controller\Api\V1;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PackControllerTest.
 */
class PackControllerTest extends WebTestCase
{
    /**
     * @throws \Exception
     */
    public function testCreateAction()
    {
        $client = static::createClient([], [
            'HTTP_AUTHORIZATION' => 'Basic '.\base64_encode('test:'.\getenv('APP_SECRET')),
            'CONTENT_TYPE' => 'application/json',
        ]);

        $uri = '/api/v1/generator/pack/create';

        // Validation filed: version not UUID, metadata not exist.
        $content = [
            'version' => 123,
        ];
        $client->request('POST', $uri, [], [], [], \json_encode($content));
        $response = $client->getResponse();

        $this->assertJson($response->getContent(), 'response content not json');
        $this->assertSame(Response::HTTP_BAD_REQUEST, $response->getStatusCode(), 'unexpected response status code');
        $content = \json_decode($client->getResponse()->getContent());
        $this->assertTrue(false === $content->success, 'unexpected response');
        $this->assertSame(
            Response::HTTP_BAD_REQUEST,
            $content->errors[0]->status,
            'api problem status != response status code'
        );
        $violationsPropertyPath = \array_column($content->errors[0]->violations, 'propertyPath');
        $this->assertTrue(\in_array('version', $violationsPropertyPath), 'missing expected argument');
        $this->assertTrue(\in_array('metadata', $violationsPropertyPath), 'missing expected argument');
    }
}
