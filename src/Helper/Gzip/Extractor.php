<?php

declare(strict_types=1);

namespace App\Helper\Gzip;

class Extractor
{
    /**
     * @param string $gzFilePath
     * @param string $outputFilePath
     * @param bool   $unlinkGzFile
     */
    public static function extract(string $gzFilePath, string $outputFilePath, bool $unlinkGzFile = false)
    {
        if (!$input = @\gzopen($gzFilePath, 'rb')) {
            throw new \UnexpectedValueException("Could not open gzip file: $gzFilePath");
        }

        if (!$output = @\fopen($outputFilePath, 'w')) {
            \gzclose($input);
            throw new \UnexpectedValueException("Could not open destination file: $outputFilePath");
        }

        \flock($input, LOCK_EX);
        \flock($output, LOCK_EX);
        \stream_copy_to_stream($input, $output);
        \flock($input, LOCK_UN);
        \flock($output, LOCK_UN);

        \gzclose($input);
        \fclose($output);

        if ($unlinkGzFile) {
            \unlink($gzFilePath);
        }
    }
}
