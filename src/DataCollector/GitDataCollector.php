<?php

declare(strict_types=1);

namespace App\DataCollector;

use App\BranchLoader\GitLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;

/**
 * GitDataCollector.
 */
class GitDataCollector extends DataCollector
{
    /** @var GitLoader */
    private $gitLoader;

    /**
     * @param GitLoader $gitLoader
     */
    public function __construct(GitLoader $gitLoader)
    {
        $this->gitLoader = $gitLoader;
    }

    /**
     * @param Request         $request
     * @param Response        $response
     * @param \Exception|null $exception
     */
    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        // We add the git informations in $data[]
        $this->data = [
            'git_branch' => $this->gitLoader->getBranchName(),
            'last_commit_message' => $this->gitLoader->getLastCommitMessage(),
            'logs' => $this->gitLoader->getLastCommitDetail(),
        ];
    }

    /**
     * Returns the name of the collector.
     *
     * @return string The collector name
     */
    public function getName()
    {
        return 'app.git_data_collector';
    }

    public function reset()
    {
        $this->data = [];
    }

    //Some helpers to access more easily to infos in the template

    /**
     * @return mixed
     */
    public function getGitBranch()
    {
        return $this->data['git_branch'];
    }

    /**
     * @return mixed
     */
    public function getLastCommitMessage()
    {
        return $this->data['last_commit_message'];
    }

    /**
     * @return mixed
     */
    public function getLastCommitAuthor()
    {
        return $this->data['logs']['author'];
    }

    /**
     * @return mixed
     */
    public function getLastCommitDate()
    {
        return $this->data['logs']['date'];
    }
}
