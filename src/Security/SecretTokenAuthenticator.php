<?php

declare(strict_types=1);

namespace App\Security;

use App\Repository\SecretTokenRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;

class SecretTokenAuthenticator extends AbstractFormLoginAuthenticator
{
    /**
     * @var SecretTokenRepository
     */
    private $secretTokenRepository;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(SecretTokenRepository $secretTokenRepository, UrlGeneratorInterface $urlGenerator)
    {
        $this->secretTokenRepository = $secretTokenRepository;
        $this->urlGenerator = $urlGenerator;
    }

    public function supports(Request $request)
    {
        return $request->attributes->get('_route') === 'app_secret_token_login'
            && $request->isMethod('GET');
    }

    public function getCredentials(Request $request)
    {
        $params = $request->attributes->get('_route_params');
        if (!\array_key_exists('token', $params)) {
            return false;
        }

        return [
            'token' => $params['token'],
        ];
    }

    /**
     * @param mixed                 $credentials
     * @param UserProviderInterface $userProvider
     *
     * @return \App\Entity\User|UserInterface|null
     *
     * @throws \Exception
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $user = null;
        if ($secretToken = $this->secretTokenRepository->findOneBy(['token' => $credentials['token']])) {
            if ($secretToken->getExpiredAt() > new \DateTime()) {
                $user = $secretToken->getUser();
            }
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    public function supportsRememberMe()
    {
        return false;
    }

    protected function getLoginUrl()
    {
        return $this->urlGenerator->generate('app_secret_token_unauthorized');
    }
}
