<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\CrmStatus;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class CrmStatusFixtures.
 */
class CrmStatusFixtures extends BaseFixture
{
    /**
     * @param ObjectManager $manager
     */
    public function loadData(ObjectManager $manager)
    {
        $this->createMany(CrmStatus::class, 10, function (CrmStatus $crmStatus, $count) {
            $crmStatus
                ->setAlias("in_work_{$count}")
                ->setDescription("crm status {$count} - {$this->faker->buildingNumber}");
        });

        $manager->flush();
    }
}
