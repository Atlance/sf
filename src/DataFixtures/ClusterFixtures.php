<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Cluster;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class ClusterFixtures.
 */
class ClusterFixtures extends BaseFixture
{
    /**
     * @param ObjectManager $manager
     */
    public function loadData(ObjectManager $manager)
    {
        $this->createMany(Cluster::class, 10, function (Cluster $cluster, $count) {
            $cluster->setDescription("Кластер {$count} {$this->faker->colorName}");
        });

        $manager->flush();
    }
}
