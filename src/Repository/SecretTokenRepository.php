<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\SecretToken;
use App\Repository\Traits\PaginatorTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Knp\Bundle\PaginatorBundle\Definition\PaginatorAwareInterface;

/**
 * @method SecretToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method SecretToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method SecretToken[]    findAll()
 * @method SecretToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SecretTokenRepository extends ServiceEntityRepository implements PaginatorAwareInterface
{
    use PaginatorTrait;

    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SecretToken::class);
    }
}
