<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\DistrictOperator;
use App\Repository\Traits\PaginatorTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Knp\Bundle\PaginatorBundle\Definition\PaginatorAwareInterface;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;

/**
 * DistrictOperatorRepository.
 */
class DistrictOperatorRepository extends ServiceEntityRepository implements PaginatorAwareInterface
{
    use PaginatorTrait;

    /**
     * {@inheritdoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DistrictOperator::class);
    }

    /**
     * @param array $conditions
     *
     * @return SlidingPagination
     */
    public function findByConditions(array $conditions = [])
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select(['district_operator'])
            ->from(DistrictOperator::class, 'district_operator')
        ;

        return $this->getPaginatorByQueryBuilder($qb, $conditions);
    }
}
