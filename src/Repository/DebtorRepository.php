<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Cluster;
use App\Entity\CrmStatus;
use App\Entity\Debtor;
use App\Entity\DistrictOperator;
use App\Entity\Filial;
use App\Entity\GeneratorPackInfo;
use App\Entity\HouseType;
use App\Repository\Traits\PaginatorTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Knp\Bundle\PaginatorBundle\Definition\PaginatorAwareInterface;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;

/**
 * DebtorRepository.
 */
class DebtorRepository extends ServiceEntityRepository implements PaginatorAwareInterface
{
    use PaginatorTrait;

    /**
     * {@inheritdoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Debtor::class);
    }

    /**
     * @param array $conditions
     *
     * @return SlidingPagination
     *
     * @throws NonUniqueResultException
     */
    public function findByConditions(array $conditions = [])
    {
        $qb = $this->_em->createQueryBuilder();
        $expr = $this->_em->getExpressionBuilder();
        $qb->select(['debtors'])
            ->from(Debtor::class, 'debtors');
        self::leftJoin($qb);

        if (\array_key_exists('fias_address_guid', $conditions)) {
            $qb->where(
                $expr->orX(
                    $expr->eq('debtors.houseFiasGuid', "'{$conditions['fias_address_guid']}'"),
                    $expr->eq('debtors.streetFiasGuid', "'{$conditions['fias_address_guid']}'"),
                    $expr->eq('debtors.cityFiasGuid', "'{$conditions['fias_address_guid']}'")
                )
            );

            unset($conditions['fias_address_guid']);
        }

        if (\array_key_exists('total_debtor_gaz', $conditions)) {
            $paginator = $this->getPaginatorByQueryBuilder($qb, $conditions);
            $paginator->setCustomParameters(
                [
                    'total_debtor_gaz' => (int) $this
                        ->_em
                        ->createQueryBuilder()
                        ->select($expr->count('debtors'))
                        ->from(Debtor::class, 'debtors')
                        ->where($expr->gt('debtors.saldoGaz', 0))
                        ->getQuery()
                        ->getSingleScalarResult(),
                ]
            );

            return $paginator;
        }

        return $this->getPaginatorByQueryBuilder($qb, $conditions);
    }

    /**
     * @param \DateTime   $dateTime
     * @param string|null $packVersion
     * @param int         $page
     * @param int         $limit
     *
     * @return SlidingPagination
     */
    public function findBySaldoGazOrSaldoTOByCheckBalanceAt(
        \DateTime $dateTime,
        string $packVersion = null,
        $page = 1,
        $limit = 100
    ) {
        $conditions = ['page' => $page, 'limit' => $limit];
        $qb = $this->_em->createQueryBuilder();
        $expr = $qb->expr();
        $qb->select(['debtors'])
            ->from(Debtor::class, 'debtors');
        self::leftJoin($qb);
        $qb
            ->where(
                $expr->andX(
                    $expr->orX(
                        $expr->neq('debtors.cluster', Cluster::PAYED_DEBT_GAZ_CLUSTER),
                        $expr->isNull('debtors.cluster')
                    ),
                    $expr->lte('debtors.checkBalanceAt', ':dateTime')
                )
            )
            ->setParameter('dateTime', $dateTime)
        ;

        if ($packVersion) {
            $qb->andWhere($expr->eq('debtors.pack', ':pack'))
                ->setParameter('pack', $packVersion);
        }

        return $this->getPaginatorByQueryBuilder($qb, $conditions);
    }

    /**
     * @param int|string|null $filial
     * @param int|string|null $districtOperator
     *
     * @return array
     *
     * @throws DBALException
     */
    public function findByReport($filial = null, $districtOperator = null)
    {
        $qb = $this->_em->createQueryBuilder();
        $connection = $qb->getEntityManager()->getConnection();

        $debtSmsSql = 'SELECT count(abon_code) as queue_count,SUM(saldo_gaz) as queue_sum FROM debtors WHERE sms_channel=1 AND cluster_id!='.Cluster::PAYED_DEBT_GAZ_CLUSTER;
        $smsSql = 'SELECT count(abon_code) as pay_count,SUM(saldo_gaz) as pay_sum FROM debtors WHERE sms_channel=1 AND cluster_id='.Cluster::PAYED_DEBT_GAZ_CLUSTER;
        $debtEmailSql = 'SELECT count(abon_code) as queue_count,SUM(saldo_gaz) as queue_sum FROM debtors WHERE email_channel=1 AND cluster_id!='.Cluster::PAYED_DEBT_GAZ_CLUSTER;
        $emailSql = 'SELECT count(abon_code) as pay_count,SUM(saldo_gaz) as pay_sum FROM debtors WHERE email_channel=1 AND cluster_id='.Cluster::PAYED_DEBT_GAZ_CLUSTER;
        $debtCrmSql = 'SELECT count(abon_code) as queue_count,SUM(saldo_gaz) as queue_sum FROM debtors WHERE crm_channel=1 AND crm_status_id IS NULL AND cluster_id!='.Cluster::PAYED_DEBT_GAZ_CLUSTER;
        $crmSql = 'SELECT count(abon_code) as pay_count,SUM(saldo_gaz) as pay_sum FROM debtors WHERE crm_channel=1 AND crm_status_id IS NULL AND cluster_id='.Cluster::PAYED_DEBT_GAZ_CLUSTER;

        if ($filial) {
            $filialSql = "  AND filial_id={$filial}";
            $debtSmsSql .= $filialSql;
            $smsSql .= $filialSql;
            $debtEmailSql .= $filialSql;
            $emailSql .= $filialSql;
            $debtCrmSql .= $filialSql;
            $crmSql .= $filialSql;
        }
        if ($districtOperator) {
            $districtOperatorSql = " AND district_operator_id = {$districtOperator}";
            $debtSmsSql .= $districtOperatorSql;
            $smsSql .= $districtOperatorSql;
            $debtEmailSql .= $districtOperatorSql;
            $emailSql .= $districtOperatorSql;
            $debtCrmSql .= $districtOperatorSql;
            $crmSql .= $districtOperatorSql;
        }

        $debtEmailStmt = $connection->prepare($debtEmailSql);
        $emailStmt = $connection->prepare($emailSql);
        $debtSmsStmt = $connection->prepare($debtSmsSql);
        $smsStmt = $connection->prepare($smsSql);
        $debtCrmStmt = $connection->prepare($debtCrmSql);
        $crmStmt = $connection->prepare($crmSql);

        $debtCrmStmt->execute();
        $crmStmt->execute();
        $debtSmsStmt->execute();
        $smsStmt->execute();
        $debtEmailStmt->execute();
        $emailStmt->execute();

        return [
            'crm_channel' => $debtCrmStmt->fetchAll()[0] + $crmStmt->fetchAll()[0],
            'sms_channel' => $debtSmsStmt->fetchAll()[0] + $smsStmt->fetchAll()[0],
            'email_channel' => $debtEmailStmt->fetchAll()[0] + $emailStmt->fetchAll()[0],
        ];
    }

    /**
     * @param QueryBuilder $qb
     */
    private function leftJoin(QueryBuilder $qb): void
    {
        $qb
            ->leftJoin(Filial::class, 'filial', Join::WITH, 'debtors.filial = filial.id')
            ->leftJoin(Cluster::class, 'cluster', Join::WITH, 'debtors.cluster = cluster.id')
            ->leftJoin(CrmStatus::class, 'crm_status', Join::WITH, 'debtors.crmStatus = crm_status.id')
            ->leftJoin(GeneratorPackInfo::class, 'pack', Join::WITH, 'debtors.pack = pack.version')
            ->leftJoin(
                DistrictOperator::class,
                'distinct_operator',
                Join::WITH,
                'debtors.districtOperator = distinct_operator.id'
            )
            ->leftJoin(HouseType::class, 'house_type', Join::WITH, 'debtors.houseType = house_type.id');
    }
}
