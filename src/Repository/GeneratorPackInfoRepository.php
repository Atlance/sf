<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\GeneratorPackInfo;
use App\Repository\Traits\PaginatorTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Knp\Bundle\PaginatorBundle\Definition\PaginatorAwareInterface;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;

/**
 * GeneratorPackInfoRepository.
 */
class GeneratorPackInfoRepository extends ServiceEntityRepository implements PaginatorAwareInterface
{
    use PaginatorTrait;

    /**
     * {@inheritdoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GeneratorPackInfo::class);
    }

    /**
     * @param array $conditions
     *
     * @return SlidingPagination
     */
    public function findByConditions(array $conditions = [])
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select(['generator_pack_info'])
            ->from(GeneratorPackInfo::class, 'generator_pack_info')
        ;

        return $this->getPaginatorByQueryBuilder($qb, $conditions);
    }
}
