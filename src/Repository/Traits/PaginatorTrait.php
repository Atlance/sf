<?php

declare(strict_types=1);

namespace App\Repository\Traits;

use App\Service\Helper\OnlyObject;
use Atlance\Doctrine\ORM\Query\Helper\Filter;
use Atlance\Doctrine\ORM\Query\Helper\Sort;
use Doctrine\ORM\QueryBuilder;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Knp\Component\Pager\Paginator;

trait PaginatorTrait
{
    /**
     * @var Paginator
     */
    private $paginator;

    public function setPaginator(Paginator $paginator)
    {
        $this->paginator = $paginator;
    }

    public function getPaginator(): Paginator
    {
        return $this->paginator;
    }

    public function getPaginatorByQueryBuilder(QueryBuilder $qb, array $conditions = []): SlidingPagination
    {
        Sort::process($qb, $conditions['order'] ?? []);
        Filter::process($qb, $conditions['where'] ?? []);
        /** @var SlidingPagination $paginator */
        $paginator = $this->getPaginator()
            ->paginate($qb->getQuery(), $conditions['page'] ?? 1, $conditions['limit'] ?? 20);
        $result = $paginator->getItems();
        $objects = [];
        OnlyObject::select($result, $objects);
        $paginator->setItems($objects);

        return $paginator;
    }
}
