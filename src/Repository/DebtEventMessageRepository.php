<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\DebtEventMessage;
use App\Entity\Debtor;
use App\Entity\EventMessage;
use App\Repository\Traits\PaginatorTrait;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;
use Knp\Bundle\PaginatorBundle\Definition\PaginatorAwareInterface;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;

/**
 * DebtEventMessageRepository.
 */
class DebtEventMessageRepository extends ServiceEntityRepository implements PaginatorAwareInterface
{
    use PaginatorTrait;

    /**
     * {@inheritdoc}
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventMessage::class);
    }

    /**
     * @param array $conditions
     *
     * @return SlidingPagination
     */
    public function findByConditions(array $conditions = [])
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select(['event_message'])
            ->from(DebtEventMessage::class, 'event_message')
            ->leftJoin(Debtor::class, 'debtor', Join::WITH, 'event_message.parent = debtor.abonCode')
        ;

        return $this->getPaginatorByQueryBuilder($qb, $conditions);
    }
}
