<?php

declare(strict_types=1);

namespace App\Service\WebSaupg;

use App\Entity\Cluster;
use App\Entity\Debtor;
use App\Entity\DistrictOperator;
use App\Service\Http\AbstractHttpClient;
use App\Service\LKK\DebtorNotifier;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Сервис взаимодействия с web-saupg на тему должников.
 */
class DebtorService
{
    /**
     * @var AbstractHttpClient
     */
    private $webSaubgHttpClient;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Cluster|object|null
     */
    private $payedGazCluster;

    /**
     * @var DebtorNotifier
     */
    private $notifier;

    /**
     * @var string
     */
    private $url;

    /**
     * @param AbstractHttpClient     $webSaubgHttpClient
     * @param EntityManagerInterface $em
     * @param DebtorNotifier         $notifier
     * @param LoggerInterface        $logger
     */
    public function __construct(
        AbstractHttpClient $webSaubgHttpClient,
        string $url,
        EntityManagerInterface $em,
        DebtorNotifier $notifier,
        LoggerInterface $logger
    ) {
        $this->webSaubgHttpClient = $webSaubgHttpClient;
        $this->url = $url;
        $this->em = $em;
        $this->notifier = $notifier;
        $this->logger = $logger;
        $this->payedGazCluster = $this->em->getRepository(Cluster::class)->find(Cluster::PAYED_DEBT_GAZ_CLUSTER);
    }

    /**
     * @param \DateTime            $date
     * @param string|null          $packVersion
     * @param int                  $page
     * @param int                  $limit
     * @param OutputInterface|null $output
     *
     * @throws \Exception
     */
    public function checkBalanceByDate(
        \DateTime $date,
        string $packVersion = null,
        $page = 1,
        $limit = 500,
        OutputInterface $output = null
    ): void {
        $progressBar = $output instanceof OutputInterface;
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $debtorRepository = $this->em->getRepository(Debtor::class);
        $paginator = $debtorRepository->findBySaldoGazOrSaldoTOByCheckBalanceAt($date, $packVersion, $page, $limit);

        if ($output) {
            $progressBar = new ProgressBar($output, $paginator->getTotalItemCount());
            $datetime = (new \DateTime())->format('Y-m-d');
            $progressBar->setFormat("$datetime - %current%/%max% | %percent:3s%% | %estimated:-6s% | %memory:6s%");

            $progressBar->start();
        }

        if ($debtors = $paginator->getItems()) {
            self::getSaubgDataByPack($debtors);
            $this->notifier->notifyByDebtors($debtors);

            if ($output) {
                $progressBar->advance(\count($debtors));
            }

            $page = $paginator->getCurrentPageNumber();
            $pageCount = $paginator->getPageCount();

            while ($page <= $pageCount) {
                ++$page;
                $paginator = $debtorRepository
                    ->findBySaldoGazOrSaldoTOByCheckBalanceAt($date, $packVersion, $page, $limit);
                if ($debtors = $paginator->getItems()) {
                    self::getSaubgDataByPack($debtors);
                    $this->notifier->notifyByDebtors($debtors);

                    if ($output) {
                        $progressBar->advance(\count($debtors));
                    }
                }
            }
            $progressBar->finish();
        }
    }

    /**
     * @param Debtor[] $debtors
     */
    public function getSaubgDataByPack(array &$debtors)
    {
        $abonCodes = [];
        $undefinedDebtors = new ArrayCollection($debtors);

        foreach ($debtors as $debtor) {
            /* @var Debtor $debtor */
            \array_push($abonCodes, $debtor->getAbonCode());
        }

        try {
            /** @var ResponseInterface $response */
            $response = $this->webSaubgHttpClient->request('POST', $this->url, ['body' => \json_encode($abonCodes)]);

            $content = \json_decode((string) $response->getBody(), true);

            if (\in_array($response->getStatusCode(), [Response::HTTP_OK, Response::HTTP_CREATED])) {
                if (\is_array($content) && \array_key_exists('success', $content) && true === $content['success']) {
                    if (\array_key_exists('data', $content) && \array_key_exists('items', $content['data'])) {
                        foreach ($content['data']['items'] as $item) {
                            [$contract, $balance, $debtorData, $districtOperator] = \array_values($item);
                            /** @var Debtor $debtor */
                            foreach ($debtors as $i => $debtor) {
                                if ($contract === $debtor->getAbonCode()) {
                                    if (\is_array($debtorData)) {
                                        if (\array_key_exists('fias_city_id', $debtorData)) {
                                            $debtor->setCityFiasGuid($debtorData['fias_city_id']);
                                        }
                                        if (\array_key_exists('fias_street_id', $debtorData)) {
                                            $debtor->setStreetFiasGuid($debtorData['fias_street_id']);
                                        }
                                        if (\array_key_exists('fias_house_id', $debtorData)) {
                                            $debtor->setHouseFiasGuid($debtorData['fias_house_id']);
                                        }
                                        if (\array_key_exists('email', $debtorData)) {
                                            $debtor->setEmail($debtorData['email']);
                                        }
                                        if (\array_key_exists('phone', $debtorData)) {
                                            $debtor->setPhone($debtorData['phone']);
                                        }
                                    }
                                    if (\is_array($districtOperator) && \array_key_exists('id', $districtOperator)) {
                                        $debtor->setDistrictOperator(
                                            $this->em->getRepository(DistrictOperator::class)
                                                ->find($districtOperator['id'])
                                        );
                                    }
                                    if ($balance > 0) {
                                        $debtor
                                            // мы теперь не обнуляем задолженность а просто ставим кластер
                                            // что бы понимать сколько оплатили бывшие должники
//                                            ->setSaldoGaz(0)
//                                            ->setSaldoTo(0)
                                            ->setCheckBalanceAt(null)
                                            ->setCluster($this->payedGazCluster)
                                        ;
                                        $this->logger->info($contract);
                                    }
                                    $undefinedDebtors->removeElement($debtor);
                                }
                                $this->em->merge($debtor);
                            }
                        }
                    }
                } else {
                    $this->logger->error(\json_encode($response, JSON_UNESCAPED_UNICODE));
                }
            }
            $this->em->flush();
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }

        if ($undefinedDebtors->count()) {
            $codes = $undefinedDebtors->map(function (Debtor $debtor) {
                return $debtor->getAbonCode();
            });
            $this->logger->notice('Undefined debtors in Web-Saupg:'.\json_encode($codes, JSON_UNESCAPED_UNICODE));
        }
    }
}
