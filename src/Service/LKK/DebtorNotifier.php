<?php

declare(strict_types=1);

namespace App\Service\LKK;

use App\Entity\Debtor;
use App\Service\Http\AbstractHttpClient;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Сервис по уведомлению ЛКК о задолжниках.
 */
class DebtorNotifier
{
    const EMAIL_CHANNEL = 'email';
    const SMS_CHANNEL = 'sms';
    const CRM_CHANNEL = 'crm';
    const PAYED_CHANNEL = 'payed';

    const LKK_NOTIFICATION_COUNT_LIMIT = 2;

    /**
     * @var AbstractHttpClient
     */
    private $httpClient;

    /**
     * @var string
     */
    private $packPath;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $checkBalanceAtInterval;

    /**
     * @var string
     */
    private $url;

    /**
     * @param AbstractHttpClient     $httpClient
     * @param string                 $url
     * @param string                 $packPath
     * @param EntityManagerInterface $em
     * @param LoggerInterface        $logger
     * @param string                 $checkBalanceAtInterval
     */
    public function __construct(
        AbstractHttpClient $httpClient,
        string $url,
        string $packPath,
        EntityManagerInterface $em,
        LoggerInterface $logger,
        string $checkBalanceAtInterval
    ) {
        $this->httpClient = $httpClient;
        $this->url = $url;
        $this->packPath = $packPath;
        $this->em = $em;
        $this->logger = $logger;
        $this->checkBalanceAtInterval = $checkBalanceAtInterval;
    }

    /**
     * @param Debtor[] $debtors
     *
     * @throws \Exception
     */
    public function notifyByDebtors(array &$debtors)
    {
        $sortedDebtors = $this->sortDebtorsByNotificationChannels($debtors);

        if ($gzPath = $this->createDebtorsPack($sortedDebtors[self::EMAIL_CHANNEL], self::EMAIL_CHANNEL)) {
            $this->sendDebtorPack(self::EMAIL_CHANNEL, $gzPath, $sortedDebtors[self::EMAIL_CHANNEL]);
        }

        if ($gzPath = $this->createDebtorsPack($sortedDebtors[self::SMS_CHANNEL], self::SMS_CHANNEL)) {
            $this->sendDebtorPack(self::SMS_CHANNEL, $gzPath, $sortedDebtors[self::SMS_CHANNEL]);
        }

        if ($gzPath = $this->createPayedPack($sortedDebtors[self::PAYED_CHANNEL], self::PAYED_CHANNEL)) {
            $this->sendPayedPack(self::PAYED_CHANNEL, $gzPath, $sortedDebtors[self::PAYED_CHANNEL]);
        }

        if ($sortedDebtors[self::CRM_CHANNEL]) {
            foreach ($sortedDebtors[self::CRM_CHANNEL] as $debtor) {
                $this->em->merge($debtor);
            }

            $this->em->flush();
        }
    }

    /**
     * @param array  $debtors
     * @param string $channel
     *
     * @return string|null
     *
     * @throws \Exception
     */
    private function createDebtorsPack(array $debtors, string $channel)
    {
        if ($debtors) {
            $date = (new \DateTime())->format('Y:m:d');

            if (!\file_exists("{$this->packPath}/{$channel}")) {
                \mkdir("{$this->packPath}/{$channel}", 0775);
            }

            if ($zp = \gzopen("{$this->packPath}/{$channel}/{$date}.gz", 'w9')) {
                \fputcsv($zp, ['contract', 'saldoGas'], ';');
                foreach ($debtors as $debtor) {
                    \fputcsv($zp, [$debtor->getAbonCode(), $debtor->getSaldoGaz()], ';');
                }
                \gzclose($zp);

                return "{$this->packPath}/{$channel}/{$date}.gz";
            }
        }

        return null;
    }

    /**
     * @param array  $debtors
     * @param string $channel
     *
     * @return string|null
     *
     * @throws \Exception
     */
    private function createPayedPack(array $debtors, string $channel)
    {
        if ($debtors) {
            $date = (new \DateTime())->format('Y:m:d');

            if (!\file_exists("{$this->packPath}/{$channel}")) {
                \mkdir("{$this->packPath}/{$channel}", 0775);
            }

            if ($zp = \gzopen("{$this->packPath}/{$channel}/{$date}.gz", 'w9')) {
                \fputcsv($zp, ['contract', 'emailChannel', 'smsChannel', 'crmChannel'], ';');
                /** @var Debtor $debtor */
                foreach ($debtors as $debtor) {
                    \fputcsv(
                        $zp,
                        [
                            $debtor->getAbonCode(),
                            (int) $debtor->isEmailChannel(),
                            (int) $debtor->isSmsChannel(),
                            (int) $debtor->isCrmChannel(),
                        ]
                    );
                }
                \gzclose($zp);

                return "{$this->packPath}/{$channel}/{$date}.gz";
            }
        }

        return null;
    }

    /**
     * @param Debtor[] $debtors
     *
     * @return array
     *
     * @throws \Exception
     */
    private function sortDebtorsByNotificationChannels(array $debtors): array
    {
        $sortedDebtors = [
            self::EMAIL_CHANNEL => [],
            self::SMS_CHANNEL => [],
            self::CRM_CHANNEL => [],
            self::PAYED_CHANNEL => [],
        ];

        /** @var Debtor $debtor */
        foreach ($debtors as $debtor) {
            $lkkNotificationCount = $debtor->getLkkNotificationCount();
            if ($debtor->getSaldoGaz()) {
                if (0 === $lkkNotificationCount) {
                    $debtor
                        ->incLkkNotificationCount()
                        ->setEmailChannel(true)
                        ->setSmsChannel(false)
                        ->setCrmChannel(false);

                    \array_push($sortedDebtors[self::EMAIL_CHANNEL], $debtor);
                } elseif (0 < $lkkNotificationCount && self::LKK_NOTIFICATION_COUNT_LIMIT > $lkkNotificationCount) {
                    $debtor
                        ->incLkkNotificationCount()
                        ->setEmailChannel(false)
                        ->setSmsChannel(true)
                        ->setCrmChannel(false);

                    \array_push($sortedDebtors[self::SMS_CHANNEL], $debtor);
                } elseif (self::LKK_NOTIFICATION_COUNT_LIMIT === $lkkNotificationCount) {
                    $debtor
                        ->setEmailChannel(false)
                        ->setSmsChannel(false)
                        ->setCrmChannel(true)
                        ->setCheckBalanceAt(new \DateTime('+1 day'))
                    ;

                    \array_push($sortedDebtors[self::CRM_CHANNEL], $debtor);
                }
            } else {
                \array_push($sortedDebtors[self::PAYED_CHANNEL], $debtor);
            }
        }

        return $sortedDebtors;
    }

    /**
     * @param string   $channel
     * @param string   $gzPath
     * @param Debtor[] $debtors
     * @param string   $method
     *
     * @return bool
     */
    private function sendDebtorPack(string $channel, string $gzPath, array $debtors, string $method = 'POST')
    {
        try {
            /** @var ResponseInterface $response */
            $response = $this->httpClient->request(
                $method,
                "{$this->url}/{$channel}",
                ['body' => \fopen($gzPath, 'r'), 'headers' => ['Content-Type: gzip']]
            );

            if (\in_array($response->getStatusCode(), [Response::HTTP_OK, Response::HTTP_CREATED])) {
                foreach ($debtors as $debtor) {
                    if ($debtor->getSaldoGaz()) {
                        $this->em->merge($debtor->setCheckBalanceAt(new \DateTime($this->checkBalanceAtInterval)));
                    } else {
                        $this->em->merge($debtor->setCheckBalanceAt(null));
                    }
                }

                $this->em->flush();

                return true;
            } else {
                $this->logger->notice(__METHOD__." - {$response->getStatusCode()} - {$response->getBody()->getContents()}");
            }
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }

        return false;
    }

    /**
     * @param string   $channel
     * @param string   $gzPath
     * @param Debtor[] $debtors
     * @param string   $method
     *
     * @return bool
     */
    private function sendPayedPack(string $channel, string $gzPath, array $debtors, string $method = 'POST')
    {
        try {
            /** @var ResponseInterface $response */
            $response = $this->httpClient->request(
                $method,
                "{$this->url}/{$channel}",
                ['body' => \fopen($gzPath, 'r'), 'headers' => ['Content-Type: gzip']]
            );

            if (\in_array($response->getStatusCode(), [Response::HTTP_OK, Response::HTTP_CREATED])) {
                foreach ($debtors as $debtor) {
                    $this->em->persist($debtor->setCheckBalanceAt(null));
                }
            } else {
                $this->logger->notice(__METHOD__." - {$response->getStatusCode()} - {$response->getBody()->getContents()}");
            }

            $this->em->flush();
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }

        return false;
    }
}
