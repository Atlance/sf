<?php

declare(strict_types=1);

namespace App\Service\Http;

use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

/**
 * Class AbstractHttpClient.
 */
abstract class AbstractHttpClient implements HttpClientInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Ответ от внешнего сервиса.
     *
     * @var ResponseInterface|mixed
     */
    protected $response;

    /**
     * Код ошибки при запросе.
     *
     * @var int
     */
    protected $errorCode;

    /**
     * Тест ошибки.
     *
     * @var string
     */
    protected $errorText;

    /**
     * Заголовки.
     *
     * @var string
     */
    protected $headers;

    /**
     * Уникальный Id Http Request
     * Ни на что не влияет, нужен только для логгера.
     *
     * @var int
     */
    protected $uniqId;

    /**
     * Идентификатор HTTP клиента, при логировании.
     *
     * @var string
     */
    protected $clientName = 'AbstractHttpClient';

    /**
     * Массив с данными о каждом запросе и ответе для DataCollector.
     *
     * @var RequestInfo[]
     */
    protected $requestCollection;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->requestCollection = [];
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array  $options
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function request(string $method, string $uri, array $options = [])
    {
        $this->uniqId = Uuid::uuid4()->toString();
        $this->beforeRequest($method, $uri, $options);
        $this->response = $this->executeRequest($method, $uri, $options);
        $this->afterRequest($method, $uri, $options);

        return $this->response;
    }

    /**
     * @return mixed
     */
    public function getErrorText()
    {
        return $this->errorText;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return string
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * @param string $clientName
     */
    public function setClientName($clientName)
    {
        $this->clientName = $clientName;
    }

    /**
     * @return RequestInfo[]
     */
    public function getRequestCollection()
    {
        return $this->requestCollection;
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array  $options
     *
     * @return mixed
     */
    abstract protected function executeRequest(string $method, string $uri, array $options = []);

    /**
     * @param array $params
     * @param mixed $type
     *
     * @return string
     */
    protected function getLoggerMessage($type, array $params = [])
    {
        $messageParams = [
            'request_id' => $this->uniqId,
            'type' => $type,
            'http_client' => $this->getClientName(),
            'params' => $params,
        ];

        $message = \json_encode($messageParams);

        if ($message === false) {
            unset($messageParams['params']);
            $message = \json_encode($messageParams);
        }

        return $message;
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array  $options
     */
    protected function beforeRequest(string $method, string $uri, array $options = [])
    {
        $params = [
            'method' => $method,
            'uri' => $uri,
            'options' => $options,
        ];
        $message = $this->getLoggerMessage('beforeRequest', $params);

        $this->logger->debug($message);
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array  $options
     */
    protected function afterRequest(string $method, string $uri, array $options = [])
    {
        $this->addRequest($method, $uri, $options);

        $params = [
            'response' => [
                'status_code' => $this->response->getStatusCode(),
                'content_type' => $this->response->getHeader('Content-Type'),
            ],
        ];

        $message = $this->getLoggerMessage('afterRequest', $params);

        $this->logger->debug($message);
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array  $options
     */
    protected function addRequest(string $method, string $uri, array $options = [])
    {
        $requestInfo = new RequestInfo();
        $requestInfo
            ->setRequestId($this->uniqId)
            ->setClientName($this->getClientName())
            ->setMethod($method)
            ->setUri($uri)
            ->setOptions($options)
            ->setResponse($this->response)
            ->setHeaders($this->getHeaders())
            ->setErrorCode($this->getErrorCode())
            ->setErrorText($this->getErrorText());

        $this->requestCollection[] = $requestInfo;
    }
}
