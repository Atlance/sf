<?php

declare(strict_types=1);

namespace App\Service\Http;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;

/**
 * Class GuzzleHttpClient.
 */
class GuzzleHttpClient extends AbstractHttpClient
{
    /**
     * {@inheritdoc}
     */
    protected $clientName = 'GuzzleHttpClient';

    /**
     * @var HttpClient
     */
    protected $httpClient;

    /**
     * @param LoggerInterface $logger
     * @param string          $baseUri
     * @param array           $headers
     */
    public function __construct(LoggerInterface $logger, string $baseUri = '', array $headers = [])
    {
        parent::__construct($logger);
        $this->httpClient = new HttpClient([
            'exceptions' => false,
            'base_uri' => $baseUri,
            'headers' => $headers,
        ]);
    }

    /**
     * @return HttpClient
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * {@inheritdoc}
     */
    protected function executeRequest(string $method, string $uri, array $options = [])
    {
        try {
            $response = $this->getHttpClient()->request($method, $uri, $options);

            return $response;
        } catch (\Throwable | GuzzleException $e) {
            $this->logger->error($e->getMessage());
        }

        return '';
    }
}
