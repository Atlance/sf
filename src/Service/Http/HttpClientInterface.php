<?php

declare(strict_types=1);

namespace App\Service\Http;

/**
 * HttpClientInterface.
 */
interface HttpClientInterface
{
    /**
     * @param string $method
     * @param string $uri
     * @param array  $options Default []
     *
     * @return string|false
     */
    public function request(string $method, string $uri, array $options = []);

    public function getErrorText();

    public function getErrorCode();

    public function getHeaders();

    public function getClientName();

    /**
     * @param $clientName
     *
     * @return mixed
     */
    public function setClientName($clientName);
}
