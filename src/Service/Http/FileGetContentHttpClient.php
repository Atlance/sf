<?php

declare(strict_types=1);

namespace App\Service\Http;

/**
 * FileGetContentHttpClient.
 */
class FileGetContentHttpClient extends AbstractHttpClient
{
    /**
     * {@inheritdoc}
     */
    protected $clientName = 'FileGetContentHttpClient';

    /**
     * {@inheritdoc}
     */
    protected function executeRequest(string $method, string $uri, array $streamContextOptions = [])
    {
        if ($streamContextOptions) {
            $context = \stream_context_create($streamContextOptions);

            return \file_get_contents($uri, false, $context);
        }

        return \file_get_contents($uri);
    }
}
