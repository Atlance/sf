<?php

declare(strict_types=1);

namespace App\Service\Http;

/**
 * CurlHttpClient.
 */
class CurlHttpClient extends AbstractHttpClient
{
    /**
     * {@inheritdoc}
     */
    protected $clientName = 'CurlHttpClient';

    /**
     * {@inheritdoc}
     */
    protected function executeRequest(string $method, string $uri, array $options = [])
    {
        $ch = \curl_init();
        \curl_setopt($ch, CURLOPT_URL, $uri);

        foreach ($options as $option => $value) {
            \curl_setopt($ch, $option, $value);
        }

        $response = \curl_exec($ch);
        $this->errorCode = \curl_errno($ch);
        $this->errorText = \curl_error($ch);

        if (isset($options[CURLOPT_HEADER])) {
            $header_size = \curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $this->headers = \substr($response, 0, $header_size);
        } else {
            $this->headers = '';
        }

        \curl_close($ch);

        return $response;
    }
}
