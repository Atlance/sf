<?php

declare(strict_types=1);

namespace App\Service;

use App\Model\Address;
use App\Service\Http\GuzzleHttpClient;
use GuzzleHttp\RequestOptions;
use JMS\Serializer\SerializerInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DadataService.
 */
class DadataService
{
    public const SUGGEST_TYPE_FIO = 'fio';
    public const SUGGEST_TYPE_ADDRESS = 'address';
    public const SUGGEST_TYPE_COMPANY = 'party';
    public const SUGGEST_TYPE_BANK = 'bank';
    public const SUGGEST_TYPE_EMAIL = 'email';
    public const SUGGEST_SUPPORTED_TYPES = [
        self::SUGGEST_TYPE_FIO,
        self::SUGGEST_TYPE_ADDRESS,
        self::SUGGEST_TYPE_COMPANY,
        self::SUGGEST_TYPE_BANK,
        self::SUGGEST_TYPE_EMAIL,
    ];

    /**
     * Http client.
     *
     * @var GuzzleHttpClient
     */
    protected $httpClient;

    /**
     * Serializer.
     *
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * DadataService constructor.
     *
     * @param GuzzleHttpClient    $httpClient
     * @param SerializerInterface $serializer
     */
    public function __construct(GuzzleHttpClient $httpClient, SerializerInterface $serializer)
    {
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
    }

    /**
     * Get address info.
     *
     * @param $address
     *
     * @return Address
     *
     * @throws \Exception
     */
    public function getAddress($address)
    {
        $uri = '/api/v2/clean/'.self::SUGGEST_TYPE_ADDRESS;

        $parameters = [
            'body' => \json_encode([$address]),
        ];

        /** @var ResponseInterface $response */
        $response = $this->httpClient->request('POST', $uri, $parameters);

        if (Response::HTTP_ACCEPTED === $response->getStatusCode()) {
            try {
                $address = $this->serializer->deserialize(
                    $response->getBody()->getContents(),
                    'array<'.Address::class.'>',
                    'json'
                );
            } catch (\Exception $e) {
                $a = 1;
            }

            return \current($address);
        }

        return null;
    }

    /**
     * @param string $address
     *
     * @return Address[]
     *
     * @throws \Exception
     */
    public function getAddresses(string $address)
    {
        if (!$suggestions = self::getSuggestions(self::SUGGEST_TYPE_ADDRESS, $address)) {
            return [];
        }

        $suggestions = \array_map(function ($suggest) {
            return $suggest['data'];
        }, $suggestions);

        return $this->serializer->deserialize(\json_encode($suggestions), 'array<'.Address::class.'>', 'json');
    }

    /**
     * Подсказки.
     *
     * @see https://dadata.ru/api/suggest/
     *
     * @param string $type
     * @param string $term
     * @param int    $count
     *
     * @throws \InvalidArgumentException
     * @throws \Exception
     *
     * @return array|null
     */
    public function getSuggestions(string $type, string $term, int $count = 20)
    {
        if (!\in_array($type, self::SUGGEST_SUPPORTED_TYPES, true)) {
            throw new \InvalidArgumentException("https://dadata.ru not supported $type suggest");
        }

        $url = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/$type";

        /** @var ResponseInterface $response */
        if ($response = $this->httpClient->request('POST', $url, $this->prepareSuggestionsRequest($term, $count))) {
            return $this->parseSuggestions($response->getBody()->getContents());
        }

        return null;
    }

    /**
     * @param string $term
     * @param int    $count
     *
     * @return array
     */
    private function prepareSuggestionsRequest(string $term, int $count = 20)
    {
        return [
            RequestOptions::BODY => \json_encode([
                'count' => $count,
                'locations' => [['region' => 'Москва'], ['kladr_id' => '50']],
                RequestOptions::QUERY => $term,
            ]),
        ];
    }

    /**
     * @param string $response
     *
     * @return array|null
     */
    private function parseSuggestions(string $response)
    {
        $content = \json_decode($response, true);

        if (isset($content['suggestions']) &&
            \is_array($content['suggestions']) &&
            \count($content['suggestions']) > 0
        ) {
            return $content['suggestions'];
        }

        return null;
    }
}
