<?php

declare(strict_types=1);

namespace App\Service\Helper;

class Reflection
{
    public static function getPropertyByClass($class): ?array
    {
        if (\class_exists($class)) {
            $reflection = new \ReflectionClass($class);
            $vars = $reflection->getProperties(\ReflectionProperty::IS_PRIVATE);

            foreach ($vars as $key => $val) {
                $vars[$key] = $val->getName();
            }

            return $vars;
        }

        return null;
    }
}
