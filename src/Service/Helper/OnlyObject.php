<?php

declare(strict_types=1);

namespace App\Service\Helper;

class OnlyObject
{
    public static function select(&$array, &$arrayObjects = null)
    {
        foreach ($array as &$value) {
            if (\is_array($arrayObjects) && \is_object($value)) {
                $arrayObjects[] = $value;
            }

            if (\is_array($value)) {
                self::select($value, $arrayObjects);
            }
        }
    }
}
