<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DebtEventMessageRepository")
 * @DiscriminatorMap({"debt": "DebtEventMessage"})
 *
 * @JMS\ExclusionPolicy("all")
 */
class DebtEventMessage extends EventMessage
{
    /**
     * @var Debtor|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Debtor", fetch="EAGER", inversedBy="eventsMessages")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     *
     * @JMS\Expose
     * @JMS\Type("App\Entity\Debtor")
     * @JMS\Groups({"private"})
     */
    protected $parent;

    /**
     * @return Debtor|null
     */
    public function getParent(): ?Debtor
    {
        return $this->parent;
    }

    /**
     * @param Debtor $parent
     *
     * @return $this
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return array
     */
    public function getArrayVariables()
    {
        if ($this->parent instanceof Debtor) {
            return [
                'abonCode' => $this->parent->getAbonCode(),
                'saldoGaz' => $this->parent->getSaldoGaz(),
                'saldoTo' => $this->parent->getSaldoTo(),
                'cluster' => $this->parent->getCluster(),
            ];
        }

        return [
            'abonCode' => null,
            'saldoGaz' => null,
            'saldoTo' => null,
            'cluster' => null,
        ];
    }
}
