<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @DiscriminatorMap({"user": "UserEventMessage"})
 *
 * @JMS\ExclusionPolicy("all")
 */
class UserEventMessage extends EventMessage
{
    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", fetch="EAGER", inversedBy="eventsMessages")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     *
     * @JMS\Expose
     * @JMS\Type("App\Entity\User")
     * @JMS\Groups({"private"})
     */
    protected $parent;

    /**
     * @return User|null
     */
    public function getParent(): ?User
    {
        return $this->parent;
    }

    /**
     * @param User $parent
     *
     * @return $this
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return array
     */
    public function getArrayVariables()
    {
        if ($this->parent instanceof User) {
            return [
                'id' => $this->parent->getId(),
                'username' => $this->parent->getUsername(),
                'roles' => $this->parent->getRoles(),
            ];
        }

        return [
            'id' => null,
            'username' => null,
            'roles' => null,
        ];
    }
}
