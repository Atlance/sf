<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Traits\TimestampableEntity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Действие.
 *
 * @ORM\Entity
 * @ORM\Table(name="actions")
 *
 * @JMS\ExclusionPolicy("all")
 */
class Action
{
    use TimestampableEntity;

    /**
     * Идентификатор.
     *
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Expose
     * @JMS\Type("integer")
     * @JMS\Groups({"public", "private"})
     */
    private $id;

    /**
     * Описание.
     *
     * @var string|null
     *
     * @ORM\Column(
     *     name="description",
     *     type="string",
     *     nullable=true,
     *     options={"comment": "Описание"}
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $description;

    /**
     * Алиас.
     *
     * @var string|null
     *
     * @ORM\Column(
     *     name="alias",
     *     type="string",
     *     unique=true,
     *     nullable=true,
     *     options={"comment": "Алиас"}
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $alias;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAlias(): ?string
    {
        return $this->alias;
    }

    /**
     * @param string|null $alias
     *
     * @return $this
     */
    public function setAlias(?string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }
}
