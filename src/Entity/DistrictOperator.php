<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * РЭС-ки МОГ.
 *
 * @ORM\Entity(repositoryClass="App\Repository\DistrictOperatorRepository")
 * @ORM\Table(name="district_operators")
 *
 * @JMS\ExclusionPolicy("all")
 */
class DistrictOperator
{
    /**
     * Идентификатор
     *
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     *
     * @JMS\Expose
     * @JMS\Type("integer")
     * @JMS\Groups({"public", "private"})
     */
    private $id;

    /**
     * Активность.
     *
     * @var bool
     *
     * @ORM\Column(
     *     name="active",
     *     type="boolean",
     *     nullable=false,
     *     options={"default": true, "comment": "Активный"}
     * )
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @JMS\Groups({"public", "private"})
     */
    private $active;

    /**
     * Название.
     *
     * @var string|null
     *
     * @ORM\Column(
     *     name="title",
     *     type="string",
     *     nullable=true,
     *     options={"comment": "Название"}
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $title;

    /**
     * Родитель.
     *
     * @ORM\ManyToOne(targetEntity="DistrictOperator", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @JMS\Expose
     * @JMS\Type("App\Entity\DistrictOperator")
     * @JMS\Groups({"public", "private"})
     */
    private $parent;

    /**
     * Дети.
     *
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="DistrictOperator", mappedBy="parent")
     *
     * @JMS\Expose
     * @JMS\Type("array<App\Entity\DistrictOperator>")
     * @JMS\Accessor(getter="getChildren", setter="setChildren")
     * @JMS\Groups({"public", "private"})
     */
    private $children;

    /**
     * Должники.
     *
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Debtor", mappedBy="districtOperator", fetch="EXTRA_LAZY")
     */
    private $debtors;

    /**
     * Production constructor.
     */
    public function __construct()
    {
        $this->active = true;
        $this->debtors = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * Get Id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Id.
     *
     * @param int $id
     *
     * @return DistrictOperator
     */
    public function setId($id)
    {
        $this->id = (int) $id;

        return $this;
    }

    /**
     * Get Title.
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set Title.
     *
     * @param string|null $title
     *
     * @return DistrictOperator
     */
    public function setTitle(?string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get Active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set Active.
     *
     * @param bool $active
     *
     * @return DistrictOperator
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get Parent.
     *
     * @return DistrictOperator|null
     */
    public function getParent(): ? self
    {
        return $this->parent;
    }

    /**
     * Set Parent.
     *
     * @param DistrictOperator|null $parent
     *
     * @return $this
     */
    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return array
     */
    public function getChildren()
    {
        return $this->children->filter(function ($child) {
            return $child->active;
        })->toArray();
    }

    /**
     * @param DistrictOperator $children
     *
     * @return DistrictOperator
     */
    public function setChildren(self $children): self
    {
        if ($this->children->contains($children)) {
            return $this;
        }
        $this->children->add($children);

        return $this;
    }
}
