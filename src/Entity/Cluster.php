<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Traits\TimestampableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Кластер.
 *
 * @ORM\Entity(repositoryClass="App\Repository\ClusterRepository")
 * @ORM\Table(name="clusters")
 *
 * @JMS\ExclusionPolicy("all")
 */
class Cluster
{
    use TimestampableEntity;

    const MALICIOUS_TYPE = 30000; // Злостные должники

    const MALICIOUS_CLUSTER = 1;
    const ORDINARY_CLUSTER = 2;
    const PAYED_DEBT_GAZ_CLUSTER = 3;

    /**
     * Идентификатор.
     *
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Expose
     * @JMS\Type("integer")
     * @JMS\Groups({"public", "private"})
     */
    private $id;

    /**
     * Описание.
     *
     * @var string
     *
     * @ORM\Column(
     *     name="description",
     *     type="string",
     *     options={
     *         "comment": "Описание"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $description;

    /**
     * Должники.
     *
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Debtor", mappedBy="cluster", fetch="EXTRA_LAZY")
     */
    private $debtors;

    /**
     * Действия.
     *
     * @var Action[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Action")
     * @ORM\JoinTable(
     *     name="clusters_actions",
     *     joinColumns={@ORM\JoinColumn(name="action_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="cluster_id", referencedColumnName="id")}
     * )
     *
     * @JMS\Expose
     * @JMS\Type("array<App\Entity\Action>")
     * @JMS\Groups({"public", "private"})
     */
    private $actions;

    public function __construct()
    {
        $this->debtors = new ArrayCollection();
        $this->actions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param Debtor $debtor
     *
     * @return $this
     */
    public function addDebtor(Debtor $debtor): self
    {
        if ($this->debtors->contains($debtor)) {
            return $this;
        }

        $this->debtors->add($debtor);

        return $this;
    }

    /**
     * @return Debtor[]
     */
    public function getDebtors(): array
    {
        return $this->debtors->toArray();
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param Action $action
     *
     * @return $this
     */
    public function addAction(Action $action): self
    {
        if ($this->actions->contains($action)) {
            return $this;
        }

        $this->actions->add($action);

        return $this;
    }

    /**
     * @return Action[]
     */
    public function getActions(): array
    {
        return $this->actions->toArray();
    }
}
