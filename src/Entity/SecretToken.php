<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="secret_tokens")
 * @ORM\Entity(repositoryClass="App\Repository\SecretTokenRepository")
 */
class SecretToken
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @JMS\Groups({"default_read"})
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expired_at", type="datetime")
     * @JMS\Groups({"default_read"})
     */
    private $expiredAt;

    /**
     * @ORM\Column(name="token", type="string")
     * @JMS\Groups({"default_read"})
     */
    private $token;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", mappedBy="secretToken")
     */
    private $user;

    /**
     * @var string
     */
    private $secretKey;

    /**
     * @param string    $secretKey
     * @param User|null $user
     *
     * @throws \Exception
     */
    public function __construct(string $secretKey, User $user = null)
    {
        $this->secretKey = $secretKey;
        $this->user = $user;
        $this->expiredAt = new \DateTime('+1 min');
    }

    public function __toString()
    {
        return (string) $this->token;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return SecretToken
     *
     * @throws \Exception
     */
    public function refreshToken(): self
    {
        $this->token = \md5($this->secretKey.$this->expiredAt->getTimestamp().$this->user->getUsername());
        $this->expiredAt = new \DateTime('+1 min');

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getSecretKey(): string
    {
        return $this->secretKey;
    }

    public function setSecretKey(string $secretKey): self
    {
        $this->secretKey = $secretKey;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpiredAt(): \DateTime
    {
        return $this->expiredAt;
    }

    /**
     * @param \DateTime $expiredAt
     *
     * @return SecretToken
     */
    public function setExpiredAt(\DateTime $expiredAt): self
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }
}
