<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Филиал.
 *
 * @ORM\Table(name="filials")
 * @ORM\Entity
 *
 * @JMS\ExclusionPolicy("all")
 */
class Filial
{
    public const DEFAULT = self::CENTRAL_FILIAL_ID;

    /**
     * Центральный филиал (АУП).
     */
    public const CENTRAL_FILIAL_ID = 14;

    /**
     * Идентификатор.
     *
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", unique=true, options={"unsigned": true}, nullable=false)
     *
     * @JMS\Expose
     * @JMS\Type("integer")
     * @JMS\Groups({"public", "private"})
     */
    private $id;

    /**
     * Название.
     *
     * @var string
     *
     * @ORM\Column(
     *     name="title",
     *     type="string",
     *     unique=true,
     *     nullable=false,
     *     options={
     *         "comment": "Название"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $title;

    /**
     * Должники.
     *
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Debtor", mappedBy="filial", fetch="EXTRA_LAZY")
     */
    private $debtors;

    public function __construct()
    {
        $this->debtors = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param Debtor $debtor
     *
     * @return $this
     */
    public function addDebtor(Debtor $debtor): self
    {
        if ($this->debtors->contains($debtor)) {
            return $this;
        }

        $this->debtors->add($debtor);

        return $this;
    }

    /**
     * @return Debtor[]
     */
    public function getDebtors(): array
    {
        return $this->debtors->toArray();
    }
}
