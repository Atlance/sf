<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Событие.
 *
 * @ORM\Table(name="events")
 * @ORM\Entity
 *
 * @JMS\ExclusionPolicy("all")
 */
class Event
{
    /**
     * Стал должником за Газ.
     */
    public const DEBTOR_GAZ_ALIAS = 'debtor_gaz';

    /**
     * Стал должником за Техническое обслуживание.
     */
    public const DEBTOR_TO_ALIAS = 'debtor_to';

    /**
     * Перестал быть должником за Газ.
     */
    public const OUT_OF_GAZ_DEBT_ALIAS = 'out_of_gaz_debt';

    /**
     * Перестал быть должником за Техническое обслуживание.
     */
    public const OUT_OF_TO_DEBT_ALIAS = 'out_of_to_debt';

    /**
     * Должнику присвоен кластер.
     */
    public const ADD_CLUSTER_TO_DEBTOR = 'add_cluster_to_debtor';

    /**
     * Отправлено автоматическое уведомление о долге.
     */
    public const LKK_NOTIFICATION_TO_DEBTOR = 'lkk_notification_to_debtor';

    /**
     * Отправлена задача на взыскание долга в СУВК.
     */
    public const CRM_NOTIFICATION_TO_DEBTOR = 'crm_notification_to_debtor';

    /**
     * Должник остается должником.
     */
    public const DEBTOR_AS_DEBTOR = 'debtor_as_debtor';

    /**
     * Идентификатор.
     *
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(
     *     name="alias",
     *     type="string",
     *     options={"comment": "Алиас"}
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $alias;

    /**
     * Описание.
     *
     * @var string
     *
     * @ORM\Column(
     *     name="description",
     *     type="string",
     *     length=255,
     *     nullable=true,
     *     options={"comment": "Описание"}
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $description;

    /**
     * Твиг шаблон.
     *
     * @var string
     *
     * @ORM\Column(
     *     name="tpl",
     *     type="string",
     *     length=255,
     *     nullable=true,
     *     options={"comment": "Твиг шаблон. Для генерации сообщения"}
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"private"})
     */
    private $tpl;

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     *
     * @return Event
     */
    public function setAlias(string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Event
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getTpl(): string
    {
        return $this->tpl;
    }

    /**
     * @param string $tpl
     *
     * @return $this
     */
    public function setTpl(string $tpl): self
    {
        $this->tpl = $tpl;

        return $this;
    }
}
