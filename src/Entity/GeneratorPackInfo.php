<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Traits\TimestampableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Ramsey\Uuid\Uuid;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Пакет данных от генератора.
 *
 * @ORM\Entity(repositoryClass="App\Repository\GeneratorPackInfoRepository")
 * @ORM\Table
 *
 * @JMS\ExclusionPolicy("all")
 *
 * @UniqueEntity(fields={"version"})
 */
class GeneratorPackInfo
{
    use TimestampableEntity;

    /**
     * Версия.
     *
     * @var Uuid
     *
     * @ORM\Id
     * @ORM\Column(
     *     name="version",
     *     type="uuid",
     *     unique=true,
     *     options={
     *         "comment": "Версия пакета"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Accessor(getter="getVersion", setter="setVersion")
     * @JMS\Groups({"public", "private"})
     *
     * @Assert\Uuid
     * @Assert\NotBlank
     */
    private $version;

    /**
     * Изначальное количество строк.
     *
     * @var int|null
     *
     * @ORM\Column(
     *     name="init_rows_count",
     *     type="integer",
     *     nullable=true,
     *     options={
     *         "unsigned": true,
     *         "comment": "Изначальное количество строк"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getInitRowsCount", setter="setInitRowsCount")
     * @JMS\Groups({"private"})
     */
    private $initRowsCount;

    /**
     * Количество обработанных строк.
     *
     * @var int|null
     *
     * @ORM\Column(
     *     name="processed_rows_count",
     *     type="integer",
     *     nullable=false,
     *     options={
     *         "unsigned": true,
     *         "comment": "Количество обработанных строк"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getProcessedRowsCount", setter="setProcessedRowsCount")
     * @JMS\Groups({"private"})
     */
    private $processedRowsCount;

    /**
     * Загружен ли архив.
     *
     * @var bool
     *
     * @ORM\Column(
     *     name="is_loaded",
     *     type="boolean",
     *     nullable=false,
     *     options={"comment": "Загружен ли архив"}
     * )
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @JMS\Accessor(getter="isLoaded", setter="setLoaded")
     * @JMS\Groups({"private"})
     */
    private $loaded;

    /**
     * Распакован ли архив.
     *
     * @var bool
     *
     * @ORM\Column(
     *     name="is_unpacked",
     *     type="boolean",
     *     nullable=false,
     *     options={"comment": "Распакован ли архив"}
     * )
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @JMS\Accessor(getter="isUnpacked", setter="seUnpacked")
     * @JMS\Groups({"private"})
     */
    private $unpacked;

    /**
     * Обработан ли пакет.
     *
     * @var bool
     *
     * @ORM\Column(
     *     name="is_processed",
     *     type="boolean",
     *     nullable=false,
     *     options={"comment": "Обработан ли пакет"}
     * )
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @JMS\Accessor(getter="isProcessed", setter="setProcessed")
     * @JMS\Groups({"private"})
     */
    private $processed;

    /**
     * Метаданные.
     *
     * @var array|null
     *
     * @ORM\Column(
     *     name="metadata",
     *     type="simple_array",
     *     nullable=true,
     *     options={"comment": "Метаданные. Переменные в пакете"}
     * )
     *
     * @JMS\Expose
     * @JMS\Type("array")
     * @JMS\Accessor(getter="getMetadata", setter="setMetadata")
     * @JMS\Groups({"public", "private"})
     *
     * @Assert\NotBlank
     */
    private $metadata;

    /**
     * Должники.
     *
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Debtor", mappedBy="pack", fetch="EXTRA_LAZY")
     */
    private $debtors;

    /**
     * @var string
     */
    private $uploadDir = '';

    public function __construct()
    {
        $this->loaded = false;
        $this->unpacked = false;
        $this->processed = false;
        $this->processedRowsCount = 0;
        $this->debtors = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getVersion()
    {
        if ($this->version instanceof Uuid) {
            return $this->version->toString();
        }

        return $this->version;
    }

    /**
     * @param string $version
     *
     * @return $this
     */
    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getInitRowsCount(): ?int
    {
        return $this->initRowsCount;
    }

    /**
     * @param int|null $initRowsCount
     *
     * @return $this
     */
    public function setInitRowsCount(?int $initRowsCount): self
    {
        $this->initRowsCount = $initRowsCount;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getProcessedRowsCount(): ?int
    {
        return $this->processedRowsCount;
    }

    /**
     * @param int|null $processedRowsCount
     *
     * @return $this
     */
    public function setProcessedRowsCount(?int $processedRowsCount): self
    {
        $this->processedRowsCount = $processedRowsCount;

        return $this;
    }

    /**
     * @param string $uploadDir
     *
     * @return $this
     */
    public function setUploadDir(string $uploadDir): self
    {
        $this->uploadDir = $uploadDir;

        return $this;
    }

    /**
     * @return string
     */
    public function getUploadDir()
    {
        return $this->uploadDir;
    }

    /**
     * @param string $format
     *
     * @return string
     */
    public function getFileFullPathByFormat(string $format)
    {
        return "$this->uploadDir/{$this->getVersion()}.$format";
    }

    /**
     * @return bool
     */
    public function isLoaded(): bool
    {
        return $this->loaded;
    }

    /**
     * @param bool $loaded
     *
     * @return $this
     */
    public function setLoaded(bool $loaded): self
    {
        $this->loaded = $loaded;

        return $this;
    }

    /**
     * @return bool
     */
    public function isUnpacked(): bool
    {
        return $this->unpacked;
    }

    /**
     * @param bool $unpacked
     *
     * @return $this
     */
    public function setUnpacked(bool $unpacked): self
    {
        $this->unpacked = $unpacked;

        return $this;
    }

    /**
     * @return bool
     */
    public function isProcessed(): bool
    {
        return $this->processed;
    }

    /**
     * @param bool $processed
     *
     * @return $this
     */
    public function setProcessed(bool $processed): self
    {
        $this->processed = $processed;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getMetadata(): ?array
    {
        return $this->metadata;
    }

    /**
     * @param array|null $metadata
     *
     * @return $this
     */
    public function setMetadata(?array $metadata): self
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * @param Debtor $debtor
     *
     * @return $this
     */
    public function addDebtor(Debtor $debtor): self
    {
        if ($this->debtors->contains($debtor)) {
            return $this;
        }

        $this->debtors->add($debtor);

        return $this;
    }

    /**
     * @return Debtor[]
     */
    public function getDebtors(): array
    {
        return $this->debtors->toArray();
    }
}
