<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CRM статус.
 *
 * @ORM\Table(name="crm_statuses")
 * @ORM\Entity(repositoryClass="App\Repository\CrmStatusRepository")
 *
 * @UniqueEntity(fields={"alias"})
 *
 * @JMS\ExclusionPolicy("all")
 */
class CrmStatus
{
    /**
     * Идентификатор.
     *
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Expose
     * @JMS\Type("integer")
     * @JMS\Groups({"public", "private"})
     */
    private $id;

    /**
     * Алиас.
     *
     * @var string|null
     *
     * @ORM\Column(
     *     name="alias",
     *     type="string",
     *     unique=true,
     *     nullable=true,
     *     options={
     *         "comment": "Алиас"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     *
     * @Assert\NotBlank
     */
    private $alias;

    /**
     * Описание.
     *
     * @var string|null
     *
     * @ORM\Column(
     *     name="description",
     *     type="string",
     *     length=255,
     *     nullable=true,
     *     options={
     *         "comment": "Описание"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $description;

    /**
     * Должники.
     *
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Debtor", mappedBy="crmStatus", fetch="EXTRA_LAZY")
     */
    private $debtors;

    public function __construct()
    {
        $this->debtors = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getAlias(): ?string
    {
        return $this->alias;
    }

    /**
     * @param string|null $alias
     *
     * @return $this
     */
    public function setAlias(?string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param Debtor $debtor
     *
     * @return $this
     */
    public function addDebtor(Debtor $debtor): self
    {
        if ($this->debtors->contains($debtor)) {
            return $this;
        }

        $this->debtors->add($debtor);

        return $this;
    }

    /**
     * @return Debtor[]
     */
    public function getDebtors(): array
    {
        return $this->debtors->toArray();
    }
}
