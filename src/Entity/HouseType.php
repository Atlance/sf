<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Traits\TimestampableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Тип жилья.
 *
 * @ORM\Entity(repositoryClass="App\Repository\HouseTypeRepository")
 * @ORM\Table(name="house_types")
 *
 * @JMS\ExclusionPolicy("all")
 */
class HouseType
{
    use TimestampableEntity;

    /**
     * Идентификатор.
     *
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @JMS\Expose
     * @JMS\Type("integer")
     * @JMS\Groups({"public", "private"})
     */
    private $id;

    /**
     * Полное название.
     *
     * @var string
     *
     * @ORM\Column(name="full_name", type="string")
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $fullName;

    /**
     * Полное название.
     *
     * @var string
     *
     * @ORM\Column(name="short_name", type="string")
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $shortName;

    /**
     * Должники.
     *
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Debtor", mappedBy="houseType", fetch="EXTRA_LAZY")
     */
    private $debtors;

    public function __construct()
    {
        $this->debtors = new ArrayCollection();
    }
}
