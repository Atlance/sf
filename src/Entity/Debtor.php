<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Traits\TimestampableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Должник.
 *
 * @ORM\Entity(repositoryClass="App\Repository\DebtorRepository")
 * @ORM\Table(
 *     name="debtors",
 *     indexes={
 *         @ORM\Index(name="house_idx", columns={"house_fias_guid"}),
 *         @ORM\Index(name="street_idx", columns={"street_fias_guid"}),
 *         @ORM\Index(name="city_idx", columns={"city_fias_guid"}),
 *     }
 * )
 * @ORM\HasLifecycleCallbacks
 *
 * @JMS\ExclusionPolicy("all")
 */
class Debtor
{
    use TimestampableEntity;

    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @JMS\Groups({"default_read"})
     */
    private $id;

    /**
     * Лицевой счёт абонента.
     *
     * @var string
     *
     * @ORM\Column(
     *     name="abon_code",
     *     type="string",
     *     options={
     *         "comment": "Лицевой счёт абонента"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $abonCode;

    /**
     * Филиал.
     *
     * @var Filial|null
     *
     * @ORM\ManyToOne(targetEntity="Filial", inversedBy="debtors")
     * @ORM\JoinColumn(name="filial_id", referencedColumnName="id")
     *
     * @JMS\Expose
     * @JMS\Type("App\Entity\Filial")
     * @JMS\Groups({"public", "private"})
     */
    private $filial;

    /**
     * Кластер.
     *
     * @var Cluster|null
     *
     * @ORM\ManyToOne(targetEntity="Cluster", inversedBy="debtors", cascade={"persist"})
     * @ORM\JoinColumn(name="cluster_id", referencedColumnName="id")
     *
     * @JMS\Expose
     * @JMS\Type("App\Entity\Cluster")
     * @JMS\Groups({"public", "private"})
     */
    private $cluster;

    /**
     * CRM статус.
     *
     * @var CrmStatus|null
     *
     * @ORM\ManyToOne(targetEntity="CrmStatus", inversedBy="debtors")
     * @ORM\JoinColumn(name="crm_status_id", referencedColumnName="id", onDelete="SET NULL")
     *
     * @JMS\Expose
     * @JMS\Type("App\Entity\CrmStatus")
     * @JMS\Groups({"public", "private"})
     */
    private $crmStatus;

    /**
     * Пакет.
     *
     * @var GeneratorPackInfo|null
     *
     * @ORM\ManyToOne(targetEntity="GeneratorPackInfo", inversedBy="debtors", cascade={"persist"})
     * @ORM\JoinColumn(name="pack_version", referencedColumnName="version")
     *
     * @JMS\Expose
     * @JMS\Type("App\Entity\GeneratorPackInfo")
     * @JMS\Groups({"public", "private"})
     */
    private $pack;

    /**
     * Задолженность за поставку газа.
     *
     * @var float|null
     *
     * @ORM\Column(
     *     name="saldo_gaz",
     *     type="decimal",
     *     scale=2,
     *     nullable=true,
     *     options={
     *         "unsigned": true,
     *         "default": null,
     *         "comment": "Задолженность за поставку газа"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $saldoGaz;

    /**
     * Дата возникновения задолженности за поставку газа.
     *
     * @var \DateTime|null
     *
     * @ORM\Column(
     *     name="saldo_date_gaz",
     *     type="datetime",
     *     nullable=true,
     *     options={
     *         "comment": "Дата возникновения задолженности за поставку газа"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("DateTime")
     * @JMS\Groups({"public", "private"})
     */
    private $saldoDateGaz;

    /**
     * Задолженность за тех. обслуживание.
     *
     * @var float|null
     *
     * @ORM\Column(
     *     name="saldo_to",
     *     type="decimal",
     *     scale=2,
     *     nullable=true,
     *     options={
     *         "unsigned": true,
     *         "default": null,
     *         "comment": "Задолженность за тех. обслуживание"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $saldoTo;

    /**
     * Дата возникновения задолженности за тех. обслуживание.
     *
     * @var \DateTime|null
     *
     * @ORM\Column(
     *     name="saldo_date_to",
     *     type="datetime",
     *     nullable=true,
     *     options={
     *         "comment": "Дата возникновения задолженности за тех. обслуживание"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("DateTime")
     * @JMS\Accessor(getter="getSaldoDateTo", setter="setSaldoDateTo")
     * @JMS\Groups({"public", "private"})
     */
    private $saldoDateTo;

    /**
     * Дата проверки баланса в веб саупг.
     *
     * @var \DateTime
     *
     * @ORM\Column(
     *     name="check_balance_at",
     *     type="date",
     *     nullable=true,
     *     options={
     *         "comment": "Дата проверки баланса в веб саупг"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("DateTime")
     * @JMS\Groups({"public", "private"})
     */
    private $checkBalanceAt;

    /**
     * Количество удачных запросов в лкк на уведомление.
     *
     * @var int
     *
     * @ORM\Column(
     *     name="lkk_notification_count",
     *     type="smallint",
     *     options={
     *         "unsigned": true,
     *         "default": 0,
     *         "comment": "Количество удачных запросов в лкк на уведомление"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("integer")
     * @JMS\Groups({"public", "private"})
     */
    private $lkkNotificationCount;

    /**
     * Уведомления через канал смс.
     *
     * @var bool
     *
     * @ORM\Column(
     *     name="sms_channel",
     *     type="boolean",
     *     options={
     *         "default": false,
     *         "comment": "Уведомления через канал смс"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @JMS\Groups({"public", "private"})
     */
    private $smsChannel;

    /**
     * Уведомления через CRM(САУПГ).
     *
     * @var bool
     *
     * @ORM\Column(
     *     name="crm_channel",
     *     type="boolean",
     *     options={
     *         "default": false,
     *         "comment": "Уведомления через CRM(САУПГ)"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @JMS\Groups({"public", "private"})
     */
    private $crmChannel;

    /**
     * Уведомления через канал почты.
     *
     * @var bool
     *
     * @ORM\Column(
     *     name="email_channel",
     *     type="boolean",
     *     options={
     *         "default": false,
     *         "comment": "Уведомления через канал почты"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @JMS\Groups({"public", "private"})
     */
    private $emailChannel;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="DebtEventMessage", mappedBy="parent", fetch="EAGER")
     *
     * @JMS\Expose
     * @JMS\Type("App\Entity\DebtEventMessage")
     * @JMS\Groups({"public", "private"})
     */
    private $eventsMessages;

    /**
     * ФИАС guid дома.
     *
     * @var string|null
     *
     * @ORM\Column(
     *     name="house_fias_guid",
     *     type="guid",
     *     nullable=true,
     *     options={"comment": "ФИАС guid дома"}
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $houseFiasGuid;

    /**
     * ФИАС guid улицы.
     *
     * @var string|null
     *
     * @ORM\Column(
     *     name="street_fias_guid",
     *     type="guid",
     *     nullable=true,
     *     options={"comment": "ФИАС guid улицы"}
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $streetFiasGuid;

    /**
     * ФИАС guid населённого пункта.
     *
     * @var string|null
     *
     * @ORM\Column(
     *     name="city_fias_guid",
     *     type="guid",
     *     nullable=true,
     *     options={"comment": "ФИАС guid населённого пункта"}
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $cityFiasGuid;

    /**
     * РЭС.
     *
     * @var DistrictOperator|null
     *
     * @ORM\ManyToOne(targetEntity="DistrictOperator", inversedBy="debtors")
     * @ORM\JoinColumn(name="district_operator_id", referencedColumnName="id")
     *
     * @JMS\Expose
     * @JMS\Type("App\Entity\DistrictOperator")
     * @JMS\Groups({"public", "private"})
     */
    private $districtOperator;

    /**
     * Тип жилья.
     *
     * @var HouseType|null
     *
     * @ORM\ManyToOne(targetEntity="HouseType", inversedBy="debtors")
     * @ORM\JoinColumn(name="house_type_id", referencedColumnName="id")
     *
     * @JMS\Expose
     * @JMS\Type("App\Entity\HouseType")
     * @JMS\Groups({"public", "private"})
     */
    private $houseType;

    /**
     * Удаленный доступ по GPRS.
     *
     * @var bool
     *
     * @ORM\Column(
     *     name="gprs",
     *     type="boolean",
     *     options={
     *         "default": false,
     *         "comment": "Удаленный доступ по GPRS"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @JMS\Groups({"public", "private"})
     */
    private $gprs;

    /**
     * Email.
     *
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", nullable=true, length=255)
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $email;

    /**
     * Телефон в САУПГ.
     *
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", nullable=true, length=64)
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    private $phone;

    public function __construct()
    {
        $this->smsChannel = false;
        $this->emailChannel = false;
        $this->crmChannel = false;
        $this->gprs = false;
        $this->lkkNotificationCount = 0;
        $this->eventsMessages = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAbonCode(): ?string
    {
        return $this->abonCode;
    }

    /**
     * @param string $abonCode
     *
     * @return $this
     */
    public function setAbonCode(string $abonCode): self
    {
        $this->abonCode = $abonCode;

        return $this;
    }

    /**
     * @return Filial
     */
    public function getFilial(): ?Filial
    {
        return $this->filial;
    }

    /**
     * @param Filial $filial
     *
     * @return $this
     */
    public function setFilial(?Filial $filial): self
    {
        $this->filial = $filial;

        return $this;
    }

    /**
     * @return Cluster
     */
    public function getCluster(): ?Cluster
    {
        return $this->cluster;
    }

    /**
     * @param Cluster $cluster
     *
     * @return $this
     */
    public function setCluster(?Cluster $cluster): self
    {
        $this->cluster = $cluster;

        return $this;
    }

    /**
     * @return float
     */
    public function getSaldoGaz(): float
    {
        return (float) $this->saldoGaz;
    }

    /**
     * @param mixed $saldoGaz
     *
     * @return $this
     */
    public function setSaldoGaz($saldoGaz): self
    {
        $this->saldoGaz = (float) \number_format((float) $saldoGaz, 2, '.', '');

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getSaldoDateGaz(): ?\DateTime
    {
        return $this->saldoDateGaz;
    }

    /**
     * @param \DateTime|null $saldoDateGaz
     *
     * @return $this
     */
    public function setSaldoDateGaz(?\DateTime $saldoDateGaz): self
    {
        $this->saldoDateGaz = $saldoDateGaz;

        return $this;
    }

    /**
     * @return float
     */
    public function getSaldoTo(): float
    {
        return (float) $this->saldoTo;
    }

    /**
     * @param mixed $saldoTo
     *
     * @return $this
     */
    public function setSaldoTo($saldoTo): self
    {
        $this->saldoTo = (float) \number_format((float) $saldoTo, 2, '.', '');

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getSaldoDateTo(): ?\DateTime
    {
        return $this->saldoDateTo;
    }

    /**
     * @param \DateTime|null $saldoDateTo
     *
     * @return $this
     */
    public function setSaldoDateTo($saldoDateTo): self
    {
        $this->saldoDateTo = $saldoDateTo;

        return $this;
    }

    /**
     * @return GeneratorPackInfo|null
     */
    public function getPack(): ?GeneratorPackInfo
    {
        return $this->pack;
    }

    /**
     * @param GeneratorPackInfo|null $pack
     *
     * @return $this
     */
    public function setPack(?GeneratorPackInfo $pack): self
    {
        $this->pack = $pack;

        return $this;
    }

    /**
     * @return CrmStatus|null
     */
    public function getCrmStatus(): ?CrmStatus
    {
        return $this->crmStatus;
    }

    /**
     * @param CrmStatus|null $crmStatus
     *
     * @return $this
     */
    public function setCrmStatus(?CrmStatus $crmStatus): self
    {
        $this->crmStatus = $crmStatus;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCheckBalanceAt(): ?\DateTime
    {
        return $this->checkBalanceAt;
    }

    /**
     * @param \DateTime $checkBalanceAt
     *
     * @return $this
     */
    public function setCheckBalanceAt(?\DateTime $checkBalanceAt): self
    {
        $this->checkBalanceAt = $checkBalanceAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getLkkNotificationCount(): int
    {
        return $this->lkkNotificationCount;
    }

    /**
     * @return $this
     */
    public function incLkkNotificationCount(): self
    {
        ++$this->lkkNotificationCount;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSmsChannel(): bool
    {
        return $this->smsChannel;
    }

    /**
     * @param bool $smsChannel
     *
     * @return $this
     */
    public function setSmsChannel(bool $smsChannel): self
    {
        $this->smsChannel = $smsChannel;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEmailChannel(): bool
    {
        return $this->emailChannel;
    }

    /**
     * @param bool $emailChannel
     *
     * @return $this
     */
    public function setEmailChannel(bool $emailChannel): self
    {
        $this->emailChannel = $emailChannel;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCrmChannel(): bool
    {
        return $this->crmChannel;
    }

    /**
     * @param bool $crmChannel
     *
     * @return $this
     */
    public function setCrmChannel(bool $crmChannel): self
    {
        $this->crmChannel = $crmChannel;

        return $this;
    }

    /**
     * @return DistrictOperator|null
     */
    public function getDistrictOperator(): ?DistrictOperator
    {
        return $this->districtOperator;
    }

    /**
     * @param DistrictOperator|null $districtOperator
     *
     * @return $this
     */
    public function setDistrictOperator(?DistrictOperator $districtOperator): self
    {
        $this->districtOperator = $districtOperator;

        return $this;
    }

    /**
     * @return HouseType|null
     */
    public function getHouseType(): ?HouseType
    {
        return $this->houseType;
    }

    /**
     * @param HouseType|null $houseType
     *
     * @return $this
     */
    public function setHouseType(?HouseType $houseType): self
    {
        $this->houseType = $houseType;

        return $this;
    }

    /**
     * @return bool
     */
    public function isGprs(): bool
    {
        return $this->gprs;
    }

    /**
     * @param bool $gprs
     *
     * @return $this
     */
    public function setGprs(bool $gprs): self
    {
        $this->gprs = $gprs;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHouseFiasGuid(): ?string
    {
        return $this->houseFiasGuid;
    }

    /**
     * @param string|null $houseFiasGuid
     *
     * @return $this
     */
    public function setHouseFiasGuid(?string $houseFiasGuid): self
    {
        $this->houseFiasGuid = $houseFiasGuid;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStreetFiasGuid(): ?string
    {
        return $this->streetFiasGuid;
    }

    /**
     * @param string|null $streetFiasGuid
     *
     * @return $this
     */
    public function setStreetFiasGuid(?string $streetFiasGuid): self
    {
        $this->streetFiasGuid = $streetFiasGuid;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCityFiasGuid(): ?string
    {
        return $this->cityFiasGuid;
    }

    /**
     * @param string|null $cityFiasGuid
     *
     * @return $this
     */
    public function setCityFiasGuid(?string $cityFiasGuid): self
    {
        $this->cityFiasGuid = $cityFiasGuid;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     *
     * @return $this
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     *
     * @return $this
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }
}
