<?php

declare(strict_types=1);

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;

/**
 * Trait TimestampableEntity.
 */
trait TimestampableEntity
{
    /**
     * Дата создания.
     *
     * @var \DateTime
     *
     * @ORM\Column(
     *     name="created_at",
     *     type="datetime",
     *     options={
     *         "comment": "Дата создания"
     *     }
     * )
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @JMS\Expose
     * @JMS\Type("DateTime")
     * @JMS\Accessor(getter="getCreatedAt", setter="setCreatedAt")
     * @JMS\Groups({"public", "private"})
     */
    private $createdAt;

    /**
     * Дата обновления.
     *
     * @var \DateTime
     *
     * @ORM\Column(
     *     name="updated_at",
     *     type="datetime",
     *     options={
     *         "comment": "Дата обновления"
     *     }
     * )
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @JMS\Expose
     * @JMS\Type("DateTime")
     * @JMS\Accessor(getter="getUpdatedAt", setter="setUpdatedAt")
     * @JMS\Groups({"public", "private"})
     */
    private $updatedAt;

    /**
     * Sets createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Returns createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Sets updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Returns updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
