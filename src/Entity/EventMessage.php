<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Traits\TimestampableEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\InheritanceType;
use JMS\Serializer\Annotation as JMS;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Table(
 *     name="events_messages",
 *     indexes={
 *         @ORM\Index(name="parent_idx", columns={"parent_id"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\EventMessageRepository")
 *
 * @InheritanceType("SINGLE_TABLE")
 * @DiscriminatorColumn(name="object", type="string")
 *
 * @JMS\ExclusionPolicy("all")
 */
abstract class EventMessage
{
    use TimestampableEntity;

    /**
     * @var Uuid
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="uuid", unique=true)
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    protected $id;

    /**
     * @var Event|null
     *
     * @ORM\ManyToOne(targetEntity="Event")
     * @ORM\JoinColumn(name="event", referencedColumnName="alias", nullable=true)
     *
     * @JMS\Expose
     * @JMS\Type("App\Entity\Event")
     * @JMS\Groups({"public", "private"})
     */
    protected $event;

    /**
     * @var int|null
     *
     * @ORM\Column(
     *     name="type",
     *     type="smallint",
     *     nullable=true,
     *     options={
     *         "unsigned": true,
     *         "comment": "Переменная для управлением типом сообщения"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("integer")
     * @JMS\Groups({"private"})
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name="message",
     *     type="text",
     *     options={"comment": "Текст сообщения"}
     * )
     *
     * @JMS\Expose
     * @JMS\Type("string")
     * @JMS\Groups({"public", "private"})
     */
    protected $message;

    /**
     * Обработано ли сообщение.
     *
     * @var bool
     *
     * @ORM\Column(
     *     name="is_resolved",
     *     type="boolean",
     *     nullable=false,
     *     options={
     *         "default": false,
     *         "comment": "Обработано ли сообщение"
     *     }
     * )
     *
     * @JMS\Expose
     * @JMS\Type("boolean")
     * @JMS\Groups({"private"})
     */
    protected $resolved;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->id = (Uuid::uuid1())->toString();
        $this->resolved = false;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id->toString();
    }

    /**
     * @return Event|null
     */
    public function getEvent(): ?Event
    {
        return $this->event;
    }

    /**
     * @param Event|null $event
     *
     * @return $this
     */
    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return $this
     */
    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param $message
     *
     * @return $this
     */
    public function setMessage($message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return bool
     */
    public function isResolved(): bool
    {
        return $this->resolved;
    }

    /**
     * @param bool $resolved
     *
     * @return $this
     */
    public function setResolved(bool $resolved): self
    {
        $this->resolved = $resolved;

        return $this;
    }

    /**
     * @param $parent
     *
     * @return mixed
     */
    abstract public function setParent($parent);
}
