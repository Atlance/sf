<?php

declare(strict_types=1);

namespace App\Controller\Api\V1\Demo;

use App\Controller\Api\AbstractController;
use App\Entity\DebtEventMessage;
use App\Entity\Debtor;
use App\Entity\EventMessage;
use App\Entity\GeneratorPackInfo;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @SWG\Tag(name="Demo")
 */
class DemoController extends AbstractController
{
    const DEMO_GENERATOR_PACK_INFO_VERSION = 'c4257f69-f0f7-42b9-ba4e-1c2520fa026d';

    /**
     * Скачать пакет у Генератора, распаковать его и уведомить должников по почте через ЛКК.
     *
     * @Route("/email", name="api_v1_crm_demo_download_and_unpack_and_send_email", methods={"GET"})
     *
     * @SWG\Get(description="Скачать пакет, распаковать и уведомить по почте")
     *
     * @SWG\Response(response=200, description="HTTP_OK")
     * @SWG\Response(response=400, description="HTTP_BAD_REQUEST", ref="#/definitions/400")
     * @SWG\Response(response=401, description="HTTP_UNAUTHORIZED", ref="#/definitions/401")
     * @SWG\Response(response=403, description="HTTP_FORBIDDEN", ref="#/definitions/403")
     * @SWG\Response(response=500, description="HTTP_INTERNAL_SERVER_ERROR", ref="#/definitions/500")
     *
     * @param KernelInterface $kernel
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function demoEmailAction(KernelInterface $kernel): JsonResponse
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => 'generator:pack:download',
            '--ver' => self::DEMO_GENERATOR_PACK_INFO_VERSION,
        ]);
        $output = new BufferedOutput();
        $application->run($input, $output);

        $input = new ArrayInput([
            'command' => 'generator:pack:extract',
            '--ver' => self::DEMO_GENERATOR_PACK_INFO_VERSION,
        ]);
        $output = new BufferedOutput();
        $application->run($input, $output);

        $input = new ArrayInput([
            'command' => 'generator:pack:flush',
            '--ver' => self::DEMO_GENERATOR_PACK_INFO_VERSION,
        ]);
        $output = new BufferedOutput();
        $application->run($input, $output);

        return new JsonResponse();
    }

    /**
     * Получить данные по должникам в веб-саупг, проверить их баланс и выслать смс через ЛКК.
     *
     * @Route("/sms", name="api_v1_crm_demo_check_balance_and_send_sms", methods={"GET"})
     *
     * @SWG\Get(description="Проверить баланс и выслать смс")
     *
     * @SWG\Response(response=200, description="HTTP_OK")
     * @SWG\Response(response=400, description="HTTP_BAD_REQUEST", ref="#/definitions/400")
     * @SWG\Response(response=401, description="HTTP_UNAUTHORIZED", ref="#/definitions/401")
     * @SWG\Response(response=403, description="HTTP_FORBIDDEN", ref="#/definitions/403")
     * @SWG\Response(response=500, description="HTTP_INTERNAL_SERVER_ERROR", ref="#/definitions/500")
     *
     * @param KernelInterface $kernel
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function demoSmsAction(KernelInterface $kernel): JsonResponse
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput([
            'command' => 'web-saupg:check-balance',
            '--ver' => self::DEMO_GENERATOR_PACK_INFO_VERSION,
        ]);
        $output = new BufferedOutput();
        $application->run($input, $output);

        return new JsonResponse();
    }

    /**
     * Удалить должников из базы и обнулить данные по пакету.
     *
     * @Route("/clear", name="api_v1_crm_demo_clear", methods={"GET"})
     *
     * @SWG\Get(description="Очистить")
     *
     * @SWG\Response(response=200, description="HTTP_OK")
     * @SWG\Response(response=400, description="HTTP_BAD_REQUEST", ref="#/definitions/400")
     * @SWG\Response(response=401, description="HTTP_UNAUTHORIZED", ref="#/definitions/401")
     * @SWG\Response(response=403, description="HTTP_FORBIDDEN", ref="#/definitions/403")
     * @SWG\Response(response=500, description="HTTP_INTERNAL_SERVER_ERROR", ref="#/definitions/500")
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function demoClearAction(): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $debtors = $em->getRepository(Debtor::class)
            ->findByConditions(['where' => ['eq' => ['pack_version' => self::DEMO_GENERATOR_PACK_INFO_VERSION]]])
            ->getItems()
        ;
        /** @var Debtor $debtor */
        foreach ($debtors as $debtor) {
            $messages = $em->getRepository(DebtEventMessage::class)
                ->findByConditions(['where' => ['eq' => ['debtor_abon_code' => $debtor->getAbonCode()]]])
                ->getItems();

            /** @var DebtEventMessage $message */
            foreach ($messages as $message) {
                $em->remove($message);
            }

            $em->remove($debtor);
        }

        /** @var GeneratorPackInfo $pack */
        if ($pack = $em->getRepository(GeneratorPackInfo::class)->find(self::DEMO_GENERATOR_PACK_INFO_VERSION)) {
            $em->persist(
                $pack->setLoaded(false)
                    ->setProcessed(false)
                    ->setUnpacked(false)
                    ->setInitRowsCount(0)
                    ->setProcessedRowsCount(0)
            );
        }

        $em->flush();

        return new JsonResponse();
    }

    /**
     * Получить полный список должников.
     *
     * @Route("/debtor/list", name="api_v1_crm_demo_debtor_list", methods={"GET"})
     *
     * @SWG\Get(description="Получить полный список должников")
     *
     * @SWG\Response(
     *     response=200,
     *     description="HTTP_OK",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Debtor::class, groups={"public"}))
     *     )
     * )
     *
     * @SWG\Response(response=400, description="HTTP_BAD_REQUEST", ref="#/definitions/400")
     * @SWG\Response(response=401, description="HTTP_UNAUTHORIZED", ref="#/definitions/401")
     * @SWG\Response(response=403, description="HTTP_FORBIDDEN", ref="#/definitions/403")
     * @SWG\Response(response=500, description="HTTP_INTERNAL_SERVER_ERROR", ref="#/definitions/500")
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function debtorListAction(): JsonResponse
    {
        return $this->jsonCollection(
            $this->getDoctrine()->getManager()->getRepository(Debtor::class)->findByConditions(
                ['where' => ['eq' => ['pack_version' => self::DEMO_GENERATOR_PACK_INFO_VERSION]]]
            )
        );
    }

    /**
     * Получить полный список событий по должникам.
     *
     * @Route("/debtor/events", name="api_v1_crm_demo_debtor_events_list", methods={"GET"})
     *
     * @SWG\Parameter(ref="#/parameters/limit")
     * @SWG\Parameter(ref="#/parameters/page")
     * @SWG\Parameter(ref="#/parameters/abon_code")
     *
     * @SWG\Get(description="Получить полный список событий по должникам")
     *
     * @SWG\Response(
     *     response=200,
     *     description="HTTP_OK",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=EventMessage::class, groups={"public"}))
     *     )
     * )
     *
     * @SWG\Response(response=400, description="HTTP_BAD_REQUEST", ref="#/definitions/400")
     * @SWG\Response(response=401, description="HTTP_UNAUTHORIZED", ref="#/definitions/401")
     * @SWG\Response(response=403, description="HTTP_FORBIDDEN", ref="#/definitions/403")
     * @SWG\Response(response=500, description="HTTP_INTERNAL_SERVER_ERROR", ref="#/definitions/500")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function debtorEventsListAction(Request $request): JsonResponse
    {
        $conditions = [
            'page' => $request->query->get('page', 1),
            'limit' => $request->query->get('limit', 15),
            'where' => ['eq' => ['pack_version' => self::DEMO_GENERATOR_PACK_INFO_VERSION]],
            'order' => ['event_message_created_at' => 'asc'],
        ];
        if ($abonCode = $request->query->get('abon_code', null)) {
            $conditions['where']['eq']['debtor_abon_code'] = $abonCode;
        }

        return $this->jsonCollection(
            $this->getDoctrine()->getManager()->getRepository(DebtEventMessage::class)->findByConditions($conditions)
        );
    }
}
