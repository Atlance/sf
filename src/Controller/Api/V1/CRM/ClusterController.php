<?php

declare(strict_types=1);

namespace App\Controller\Api\V1\CRM;

use App\Controller\Api\AbstractController;
use App\Entity\Cluster;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/cluster")
 * @SWG\Tag(name="CRM")
 */
class ClusterController extends AbstractController
{
    /**
     * Получить список кластеров.
     *
     * @Route("/list", name="api_v1_crm_cluster_list", methods={"GET"})
     *
     * @SWG\Get(description="Получить список кластеров")
     *
     * @SWG\Parameter(ref="#/parameters/limit")
     * @SWG\Parameter(ref="#/parameters/page")
     *
     * @SWG\Response(
     *     response=200,
     *     description="HTTP_OK",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Cluster::class, groups={"public"}))
     *     )
     * )
     *
     * @SWG\Response(response=400, description="HTTP_BAD_REQUEST", ref="#/definitions/400")
     * @SWG\Response(response=401, description="HTTP_UNAUTHORIZED", ref="#/definitions/401")
     * @SWG\Response(response=403, description="HTTP_FORBIDDEN", ref="#/definitions/403")
     * @SWG\Response(response=500, description="HTTP_INTERNAL_SERVER_ERROR", ref="#/definitions/500")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function listAction(Request $request): JsonResponse
    {
        $conditions = [
            'page' => $request->query->get('page', 1),
            'limit' => $request->query->get('limit', 15),
        ];

        return $this->jsonCollection(
            $this->getDoctrine()->getRepository(Cluster::class)->findByConditions($conditions)
        );
    }
}
