<?php

declare(strict_types=1);

namespace App\Controller\Api\V1\CRM;

use App\Controller\Api\AbstractController;
use App\Entity\Cluster;
use App\Entity\Debtor;
use App\Form\Crm\SetStatusOnDebtorsType;
use App\Service\LKK\DebtorNotifier;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\NonUniqueResultException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Phpro\ApiProblem\Exception\ApiProblemException;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/debtor")
 * @SWG\Tag(name="CRM")
 */
class DebtorController extends AbstractController
{
    /**
     * Обновить статус должников.
     *
     * @Route("/status/update", name="api_v1_crm_debtors_status_update", methods={"POST"})
     *
     * @SWG\Post(description="Обновить статус должников или сбросить в `null`")
     * @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     required=true,
     *     @SWG\Schema(type="object", ref=@Model(type=SetStatusOnDebtorsType::class))
     * )
     * @SWG\Response(response=200, description="HTTP_OK")
     * @SWG\Response(response=400, description="HTTP_BAD_REQUEST", ref="#/definitions/400")
     * @SWG\Response(response=401, description="HTTP_UNAUTHORIZED", ref="#/definitions/401")
     * @SWG\Response(response=403, description="HTTP_FORBIDDEN", ref="#/definitions/403")
     * @SWG\Response(response=500, description="HTTP_INTERNAL_SERVER_ERROR", ref="#/definitions/500")
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws ApiProblemException
     */
    public function updateStatusAction(Request $request): JsonResponse
    {
        $this->processForm($request, SetStatusOnDebtorsType::class);

        return $this->jsonDocument(null, JsonResponse::HTTP_OK);
    }

    /**
     * Проверить является ли клиент должником.
     *
     * @Route("/check", name="api_v1_crm_is_debtor_check", methods={"POST"})
     *
     * @SWG\Post(description="Проверить является ли клиент должником")
     * @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     required=true,
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(
     *             property="abon_codes",
     *             type="array",
     *             @SWG\Items(type="string")
     *         ),
     *     )
     * )
     *
     * @SWG\Response(response=200, description="HTTP_OK")
     * @SWG\Response(response=400, description="HTTP_BAD_REQUEST", ref="#/definitions/400")
     * @SWG\Response(response=401, description="HTTP_UNAUTHORIZED", ref="#/definitions/401")
     * @SWG\Response(response=403, description="HTTP_FORBIDDEN", ref="#/definitions/403")
     * @SWG\Response(response=500, description="HTTP_INTERNAL_SERVER_ERROR", ref="#/definitions/500")
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws NonUniqueResultException
     */
    public function checkAction(Request $request): JsonResponse
    {
        $paginator = $this->getDoctrine()->getRepository(Debtor::class)->findByConditions([
            'where' => ['in' => ['debtors_abon_code' => \implode(',', $request->get('abon_codes'))]],
        ]);

        $data = [];

        /** @var Debtor $item */
        foreach ($paginator->getItems() as $item) {
            if ($cluster = $item->getCluster()) {
                $data[$item->getAbonCode()] = $cluster->getId() != Cluster::PAYED_DEBT_GAZ_CLUSTER;
            } else {
                $data[$item->getAbonCode()] = $item->getSaldoGaz() > 0;
            }
        }

        return $this->customJsonCollection($data);
    }

    /**
     * Работа со списком должников. На получение и/или оправку им уведомлений.
     *
     * @Route("/filter", name="api_v1_crm_debtors_filter", methods={"GET", "POST"})
     *
     * @SWG\Get(description=" Работа со списком должников. На получение и/или оправку им уведомлений.")
     *
     * @SWG\Parameter(ref="#/parameters/limit")
     * @SWG\Parameter(ref="#/parameters/page")
     * @SWG\Parameter(ref="#/parameters/filial")
     * @SWG\Parameter(ref="#/parameters/abon_code")
     * @SWG\Parameter(
     *     name="status",
     *     description="ID CRM статуса, если в параметере передавать 0 то выбирает все `is null`",
     *     type="integer",
     *     in="query",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="saldo_date_gaz_in_months",
     *     description="Срок задолженности за газ больше (количество месяцев)",
     *     type="integer",
     *     in="query",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="fias_address_guid",
     *     description="ФИАС GUID Адреса",
     *     type="string",
     *     in="query",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="gprs",
     *     description="Удаленный доступ по GPRS",
     *     type="boolean",
     *     in="query",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="sms_channel",
     *     description="Канал смс",
     *     type="boolean",
     *     in="query",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="email_channel",
     *     description="Канал email",
     *     type="boolean",
     *     in="query",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="crm_channel",
     *     description="Канал CRM",
     *     type="boolean",
     *     in="query",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="house_type",
     *     description="ID Тип жилья",
     *     type="integer",
     *     in="query",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="distinct_operator",
     *     description="ID РЭС",
     *     type="integer",
     *     in="query",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="cluster",
     *     description="ID Кластера",
     *     type="integer",
     *     in="query",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="saldo_gaz_gt",
     *     description="Задолженность за газ больше (руб)",
     *     type="string",
     *     in="query",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="order[debtors_saldo_gaz]",
     *     description="Сортировать по размеру задолженности за поставку газа (asc|desc)",
     *     type="string",
     *     in="query",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="order[debtors_saldo_date_gaz]",
     *     description="Сортировать по дате возникновения задолженности за поставку газа (asc|desc)",
     *     type="string",
     *     in="query",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="order[crm_status_id]",
     *     description="Сортировать по ID CRM статуса (asc|desc)",
     *     type="string",
     *     in="query",
     *     required=false
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="HTTP_OK",
     *     @SWG\Schema(
     *         @SWG\Property(property="success", ref="#/definitions/success"),
     *         @SWG\Property(
     *             property="data",
     *             type="array",
     *             @SWG\Items(
     *                 @SWG\Property(
     *                     property="items",
     *                     type="array",
     *                     @SWG\Items(
     *                         ref=@Model(type=Debtor::class, groups={"public"})
     *                     )
     *                 ),
     *                 @SWG\Property(property="page", ref="#/definitions/page"),
     *                 @SWG\Property(property="limit", ref="#/definitions/limit"),
     *                 @SWG\Property(property="page_size", ref="#/definitions/page_size"),
     *                 @SWG\Property(property="total", ref="#/definitions/total"),
     *                 @SWG\Property(property="total_debtor_gaz", ref="#/definitions/total_debtor_gaz")
     *             )
     *         )
     *     )
     * )
     *
     * @SWG\Response(response=400, description="HTTP_BAD_REQUEST", ref="#/definitions/400")
     * @SWG\Response(response=401, description="HTTP_UNAUTHORIZED", ref="#/definitions/401")
     * @SWG\Response(response=403, description="HTTP_FORBIDDEN", ref="#/definitions/403")
     * @SWG\Response(response=500, description="HTTP_INTERNAL_SERVER_ERROR", ref="#/definitions/500")
     *
     * @param Request        $request
     * @param DebtorNotifier $debtorNotifier
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function filterAction(Request $request, DebtorNotifier $debtorNotifier): JsonResponse
    {
        $paginator = $this->getDoctrine()->getRepository(Debtor::class)->findByConditions($request->query->all());
        if ('POST' === $request->getRealMethod()) {
            $debtors = $paginator->getItems();
            $debtorNotifier->notifyByDebtors($debtors);
        }

        return $this->jsonCollection($paginator);
    }

    /**
     * Отчетность.
     *
     * @Route("/report", name="api_v1_crm_debtor_report", methods={"GET"})
     *
     * @SWG\Parameter(
     *     name="filial",
     *     description="ID филиала",
     *     type="integer",
     *     in="query",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="district_operator",
     *     description="ID РЭС",
     *     type="integer",
     *     in="query",
     *     required=false
     * )
     * @SWG\Get(description="Отчетность")
     *
     * @SWG\Response(response=200, description="HTTP_OK")
     * @SWG\Response(response=400, description="HTTP_BAD_REQUEST", ref="#/definitions/400")
     * @SWG\Response(response=401, description="HTTP_UNAUTHORIZED", ref="#/definitions/401")
     * @SWG\Response(response=403, description="HTTP_FORBIDDEN", ref="#/definitions/403")
     * @SWG\Response(response=500, description="HTTP_INTERNAL_SERVER_ERROR", ref="#/definitions/500")
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws DBALException
     */
    public function reportAction(Request $request): JsonResponse
    {
        return $this->jsonDocument(
            $this->getDoctrine()
                ->getRepository(Debtor::class)
                ->findByReport(
                    $request->query->get('filial', null),
                    $request->query->get('district_operator', null)
                )
        );
    }

    /**
     * Проверить сумму задолженности за газ у должников.
     *
     * @Route("/check/saldo-gaz", name="api_v1_crm_debtor_check_saldo_gaz", methods={"POST"})
     *
     * @SWG\Post(description="Проверить сумму задолженности за газ у должников")
     * @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     required=true,
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Property(
     *             property="abon_codes",
     *             type="array",
     *             @SWG\Items(type="string")
     *         ),
     *     )
     * )
     *
     * @SWG\Response(response=200, description="HTTP_OK")
     * @SWG\Response(response=400, description="HTTP_BAD_REQUEST", ref="#/definitions/400")
     * @SWG\Response(response=401, description="HTTP_UNAUTHORIZED", ref="#/definitions/401")
     * @SWG\Response(response=403, description="HTTP_FORBIDDEN", ref="#/definitions/403")
     * @SWG\Response(response=500, description="HTTP_INTERNAL_SERVER_ERROR", ref="#/definitions/500")
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws NonUniqueResultException
     */
    public function checkSaldoGazAction(Request $request): JsonResponse
    {
        $paginator = $this->getDoctrine()->getRepository(Debtor::class)->findByConditions([
            'where' => ['in' => ['debtors_abon_code' => \implode(',', $request->get('abon_codes'))]],
        ]);

        $data = [];

        /** @var Debtor $item */
        foreach ($paginator->getItems() as $item) {
            if ($cluster = $item->getCluster()) {
                if ($cluster->getId() === Cluster::PAYED_DEBT_GAZ_CLUSTER) {
                    $data[$item->getAbonCode()] = null;

                    continue;
                }
            }

            $data[$item->getAbonCode()] = $item->getSaldoGaz();
        }

        return $this->customJsonCollection($data);
    }
}
