<?php

declare(strict_types=1);

namespace App\Controller\Api\V1\CRM;

use App\Controller\Api\AbstractController;
use App\Entity\DistrictOperator;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/district-operator")
 * @SWG\Tag(name="CRM")
 */
class DistrictOperatorController extends AbstractController
{
    /**
     * Получить список РЭС.
     *
     * @Route("/list", name="api_v1_crm_district_operator_list", methods={"GET"})
     *
     * @SWG\Get(description="Получить список РЭС")
     *
     * @SWG\Response(
     *     response=200,
     *     description="HTTP_OK",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=DistrictOperator::class, groups={"public"}))
     *     )
     * )
     *
     * @SWG\Response(response=400, description="HTTP_BAD_REQUEST", ref="#/definitions/400")
     * @SWG\Response(response=401, description="HTTP_UNAUTHORIZED", ref="#/definitions/401")
     * @SWG\Response(response=403, description="HTTP_FORBIDDEN", ref="#/definitions/403")
     * @SWG\Response(response=500, description="HTTP_INTERNAL_SERVER_ERROR", ref="#/definitions/500")
     *
     * @return JsonResponse
     */
    public function listAction(): JsonResponse
    {
        return $this->customJsonCollection(
            $this->getDoctrine()->getRepository(DistrictOperator::class)->findBy(['active' => true])
        );
    }
}
