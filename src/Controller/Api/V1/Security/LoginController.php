<?php

declare(strict_types=1);

namespace App\Controller\Api\V1\Security;

use App\Controller\Api\AbstractController;
use App\Entity\SecretToken;
use App\Entity\User;
use App\Form\SecretKeyAndUsernameType;
use Doctrine\ORM\EntityManagerInterface;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/login")
 * @SWG\Tag(name="Авторизация")
 */
class LoginController extends AbstractController
{
    /**
     * Получить ссылку на авторизацию имея username и secret_key, действительна 1 минуту.
     *
     * @Route("/secret-url", name="api_v1_login_secret_token_login_url", methods={"POST"})
     * @SWG\Post(description="Получить ссылку на авторизацию")
     * @SWG\Parameter(name="X-AUTH-TOKEN", in="header", type="string", required=true)
     * @SWG\Parameter(ref="#/parameters/username")
     *
     * @SWG\Response(response=200, description="HTTP_OK", ref="#/definitions/login_url_via_secret_key")
     * @SWG\Response(response=400, description="HTTP_BAD_REQUEST", ref="#/definitions/400")
     * @SWG\Response(response=500, description="HTTP_INTERNAL_SERVER_ERROR", ref="#/definitions/500")
     *
     * @param Request                $request
     * @param EntityManagerInterface $em
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function secretLoginCheck(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $request->request->set('X-AUTH-TOKEN', $request->headers->get('X-AUTH-TOKEN', null));
        [$username, $secretKey] = \array_values($this->processForm($request, SecretKeyAndUsernameType::class));
        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneBy(['username' => $username]);
        if (!$user->getSecretToken()) {
            $user->setSecretToken(new SecretToken($secretKey, $user));
        }

        $em->persist($user->getSecretToken()->refreshToken());
        $em->flush();

        return $this->json(
            [
                'success' => true,
                'url' => $this->generateUrl(
                    'app_secret_token_login',
                    ['token' => $user->getSecretToken()->getToken()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
            ]
        );
    }
}
