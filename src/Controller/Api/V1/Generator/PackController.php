<?php

declare(strict_types=1);

namespace App\Controller\Api\V1\Generator;

use App\Controller\Api\AbstractController;
use App\Entity\GeneratorPackInfo;
use App\Form\Pack\GeneratorPackInfoType;
use Nelmio\ApiDocBundle\Annotation\Model;
use Phpro\ApiProblem\Exception\ApiProblemException;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/pack")
 * @SWG\Tag(name="generator")
 */
class PackController extends AbstractController
{
    /**
     * Получить список пакетов.
     *
     * @Route("/list", name="api_v1_generator_pack_list", methods={"GET"})
     *
     * @SWG\Get(description="Получить список пакетов")
     *
     * @SWG\Parameter(ref="#/parameters/limit")
     * @SWG\Parameter(ref="#/parameters/page")
     *
     * @SWG\Response(
     *     response=200,
     *     description="HTTP_OK",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=GeneratorPackInfo::class, groups={"public"}))
     *     )
     * )
     *
     * @SWG\Response(response=400, description="HTTP_BAD_REQUEST", ref="#/definitions/400")
     * @SWG\Response(response=401, description="HTTP_UNAUTHORIZED", ref="#/definitions/401")
     * @SWG\Response(response=403, description="HTTP_FORBIDDEN", ref="#/definitions/403")
     * @SWG\Response(response=500, description="HTTP_INTERNAL_SERVER_ERROR", ref="#/definitions/500")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function listAction(Request $request): JsonResponse
    {
        $conditions = [
            'page' => $request->query->get('page', 1),
            'limit' => $request->query->get('limit', 15),
        ];

        return $this->jsonCollection(
            $this->getDoctrine()->getRepository(GeneratorPackInfo::class)->findByConditions($conditions)
        );
    }

    /**
     * Создание в бд информации о пакете.
     *
     * @Route("/create", name="api_v1_generator_pack_create", methods={"POST"})
     *
     * @SWG\Post(description="Генератор уведомляет о появлении у него нового пакета данных, на этот эндпоинт мы
     *                                  получаем версию и метаданные. Метаданные - это массив названий полей данных")
     * @SWG\Parameter(
     *     description="Информация о пакете",
     *     in="body",
     *     name="body",
     *     required=true,
     *     @SWG\Schema(type="object", ref=@Model(type=GeneratorPackInfoType::class))
     * )
     * @SWG\Response(
     *     response=201,
     *     description="HTTP_CREATED",
     *     @SWG\Schema(type="object", ref=@Model(type=GeneratorPackInfo::class, groups={"public"}))
     * )
     * @SWG\Response(response=400, description="HTTP_BAD_REQUEST", ref="#/definitions/400")
     * @SWG\Response(response=401, description="HTTP_UNAUTHORIZED", ref="#/definitions/401")
     * @SWG\Response(response=403, description="HTTP_FORBIDDEN", ref="#/definitions/403")
     * @SWG\Response(response=500, description="HTTP_INTERNAL_SERVER_ERROR", ref="#/definitions/500")
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws ApiProblemException
     */
    public function createAction(Request $request): JsonResponse
    {
        return $this->jsonDocument(
            $this->processForm($request, GeneratorPackInfoType::class, true),
            JsonResponse::HTTP_CREATED
        );
    }
}
