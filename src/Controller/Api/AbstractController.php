<?php

declare(strict_types=1);

namespace App\Controller\Api;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Phpro\ApiProblem\Exception\ApiProblemException;
use Phpro\ApiProblem\Http\ExceptionApiProblem;
use Phpro\ApiProblem\Http\ValidationApiProblem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as SymfonyAbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class AbstractController.
 */
class AbstractController extends SymfonyAbstractController
{
    /**
     * {@inheritdoc}
     */
    protected function jsonDocument(
        $data,
        int $status = JsonResponse::HTTP_OK,
        array $headers = [],
        array $context = ['public']
    ): JsonResponse {
        return new JsonResponse(
            SerializerBuilder::create()
                ->build()->serialize(
                    ['success' => true, 'data' => $data],
                    'json',
                    SerializationContext::create()->setGroups($context)->setSerializeNull(true)
                ),
            $status,
            $headers + ['Content-Type' => 'application/vnd.api+json'],
            true
        );
    }

    /**
     * @param SlidingPagination $paginator
     * @param int               $status
     * @param array             $headers
     * @param array             $context
     *
     * @return JsonResponse
     */
    protected function jsonCollection(
        SlidingPagination $paginator,
        int $status = JsonResponse::HTTP_OK,
        array $headers = [],
        array $context = ['public']
    ): JsonResponse {
        return $this->jsonDocument(
            [
                'items' => $paginator->getItems(),
                'total' => $paginator->getTotalItemCount(),
                'page' => null === $paginator->getPage() ? 1 : (int) $paginator->getPage(),
                'page_size' => $paginator->getItemNumberPerPage(),
            ] + $paginator->getCustomParameters(),
            $status,
            $headers,
            $context
        );
    }

    /**
     * @param array $items
     * @param int   $status
     * @param array $headers
     * @param array $context
     *
     * @return JsonResponse
     */
    protected function customJsonCollection(
        $items,
        int $status = JsonResponse::HTTP_OK,
        array $headers = [],
        array $context = ['public']
    ): JsonResponse {
        return $this->jsonDocument(
            [
                'items' => $items,
                'total' => \count($items),
            ],
            $status,
            $headers,
            $context
        );
    }

    /**
     * @param Request $request
     * @param string  $type
     * @param bool    $flush
     * @param null    $data
     * @param array   $options
     *
     * @return mixed
     *
     * @throws ApiProblemException
     */
    protected function processForm(Request $request, string $type, $flush = false, $data = null, array $options = [])
    {
        try {
            $form = $this->createForm($type, $data, $options);
            $clearMissing = $request->getMethod() !== 'PATCH';
            $form->submit($request->request->all(), $clearMissing);

            if ($form->isValid()) {
                $data = $form->getData();
                if ($flush) {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($data);
                    $em->flush();
                }

                return $data;
            }
        } catch (\Exception $e) {
            throw new ApiProblemException(new ExceptionApiProblem($e));
        }
        $errors = $violations = [];
        $this->getAllFormErrors($form, $errors);

        foreach ($errors as $property => $_errors) {
            foreach ($_errors as $error) {
                $violations[] = new ConstraintViolation($error, '', [], '', $property, '');
            }
        }

        if (\count($violations)) {
            throw new ApiProblemException(new ValidationApiProblem(new ConstraintViolationList($violations)));
        }
    }

    /**
     * @param FormInterface $form
     * @param array         $errors
     */
    private function getAllFormErrors(FormInterface $form, array &$errors)
    {
        $vars = $form->createView()->vars;
        foreach ($form->getErrors() as $error) {
            $errors[$vars['full_name']][] = $error->getMessage();
        }

        /** @var FormInterface $child */
        foreach ($form as $child) {
            if ($child->count()) {
                $this->getAllFormErrors($child, $errors);
            } else {
                $vars = $child->createView()->vars;
                foreach ($child->getErrors() as $error) {
                    $errors[$vars['full_name']][] = $error->getMessage();
                }
            }
        }
    }
}
