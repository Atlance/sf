<?php

declare(strict_types=1);

namespace App\Controller\Frontend;

use App\Controller\Api\AbstractController;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{
    /**
     * @Route("/secret/{token}/login", name="app_secret_token_login", methods={"GET"})
     *
     * @param UserInterface        $user
     * @param UserManagerInterface $userManager
     *
     * @return Response
     */
    public function loginViaSecretToken(UserInterface $user, UserManagerInterface $userManager)
    {
        $userManager->reloadUser($user);

        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/secret/unauthorized", name="app_secret_token_unauthorized", methods={"GET"})
     *
     * @return Response
     */
    public function unauthorized()
    {
        return new Response('', Response::HTTP_UNAUTHORIZED);
    }
}
