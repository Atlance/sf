<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190822151223 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT IGNORE INTO  generator_pack_info (version, init_rows_count, processed_rows_count, is_loaded, is_unpacked, is_processed, metadata, created_at, updated_at) VALUES ('7be101ce-d7de-4963-9021-6e0c15a865d8', null, 0, 1, 0, 0, null, '2019-08-22 14:54:13', '2019-08-22 18:05:43')");
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM generator_pack_info WHERE `version`= '7be101ce-d7de-4963-9021-6e0c15a865d8'");
    }
}
