<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190405170213 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE events_messages DROP FOREIGN KEY FK_26368695727ACA70');
        $this->addSql('DROP INDEX IDX_26368695727ACA70 ON events_messages');
        $this->addSql('ALTER TABLE events_messages CHANGE parent_id parent_id VARCHAR(255) NOT NULL');
        $this->addSql('CREATE INDEX parent_idx ON events_messages (parent_id)');
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DROP INDEX parent_idx ON events_messages');
        $this->addSql('ALTER TABLE events_messages CHANGE parent_id parent_id VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE events_messages ADD CONSTRAINT FK_26368695727ACA70 FOREIGN KEY (parent_id) REFERENCES debtors (abon_code)');
        $this->addSql('CREATE INDEX IDX_26368695727ACA70 ON events_messages (parent_id)');
    }
}
