<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190530151605 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE debtors ADD district_operator_id INT DEFAULT NULL, ADD house_type_id INT UNSIGNED DEFAULT NULL, ADD address_fias_guid VARCHAR(255) DEFAULT NULL, ADD gprs TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE saldo_to saldo_to NUMERIC(10, 2) UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE debtors ADD CONSTRAINT FK_2A8D8D6874F7B39A FOREIGN KEY (district_operator_id) REFERENCES district_operators (id)');
        $this->addSql('ALTER TABLE debtors ADD CONSTRAINT FK_2A8D8D68519B0A8E FOREIGN KEY (house_type_id) REFERENCES house_types (id)');
        $this->addSql('CREATE INDEX IDX_2A8D8D6874F7B39A ON debtors (district_operator_id)');
        $this->addSql('CREATE INDEX IDX_2A8D8D68519B0A8E ON debtors (house_type_id)');
        $this->addSql('CREATE INDEX fias_idx ON debtors (address_fias_guid)');
        $this->addSql('ALTER TABLE debtors CHANGE saldo_to saldo_to NUMERIC(10, 2) UNSIGNED DEFAULT NULL, CHANGE address_fias_guid address_fias_guid VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE debtors DROP FOREIGN KEY FK_2A8D8D6874F7B39A');
        $this->addSql('ALTER TABLE debtors DROP FOREIGN KEY FK_2A8D8D68519B0A8E');
        $this->addSql('DROP INDEX IDX_2A8D8D6874F7B39A ON debtors');
        $this->addSql('DROP INDEX IDX_2A8D8D68519B0A8E ON debtors');
        $this->addSql('DROP INDEX fias_idx ON debtors');
        $this->addSql('ALTER TABLE debtors DROP district_operator_id, DROP house_type_id, DROP address_fias_guid, DROP gprs, CHANGE saldo_to saldo_to NUMERIC(10, 2) DEFAULT NULL');
    }
}
