<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190530140308 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE IF NOT EXISTS house_types (id INT UNSIGNED NOT NULL, full_name VARCHAR(255) NOT NULL, short_name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $sql = <<<SQL
INSERT IGNORE INTO house_types (id, full_name, short_name, created_at, updated_at) VALUES
                        (0, 'Неопределена', 'Н/Д', NOW(), NOW()),
                        (1, 'Частный сектор', 'ЧС', NOW(), NOW()),
                        (2, 'Муниципальное жилье', 'МЖ', NOW(), NOW()),
                        (3, 'Ведомственное жилье', 'Вед', NOW(), NOW()),
                        (4, 'ЖСК', 'ЖСК', NOW(), NOW()),
                        (5, 'Воинские части', 'В/ч', NOW(), NOW()),
                        (6, 'Нежилые помещения', 'ПНП', NOW(), NOW()),
                        (7, 'Многоквартирные дома', 'Мкв', NOW(), NOW()),
                        (8, 'До 2018 года', 'СТ', NOW(), NOW());
SQL;
        $this->addSql($sql);
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE IF EXISTS house_types');
    }
}
