<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190530165207 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE crm_statuses CHANGE alias alias VARCHAR(255) DEFAULT NULL COMMENT \'Алиас\', CHANGE description description VARCHAR(255) DEFAULT NULL COMMENT \'Описание\'');
        $this->addSql('ALTER TABLE events CHANGE alias alias VARCHAR(255) NOT NULL COMMENT \'Алиас\', CHANGE description description VARCHAR(255) DEFAULT NULL COMMENT \'Описание\', CHANGE tpl tpl VARCHAR(255) DEFAULT NULL COMMENT \'Твиг шаблон. Для генерации сообщения\'');
        $this->addSql('ALTER TABLE events_messages CHANGE event event VARCHAR(255) DEFAULT NULL COMMENT \'Алиас\', CHANGE type type SMALLINT UNSIGNED DEFAULT NULL COMMENT \'Переменная для управлением типом сообщения\', CHANGE message message LONGTEXT NOT NULL COMMENT \'Текст сообщения\', CHANGE is_resolved is_resolved TINYINT(1) DEFAULT \'0\' NOT NULL COMMENT \'Обработано ли сообщение\', CHANGE created_at created_at DATETIME NOT NULL COMMENT \'Дата создания\', CHANGE updated_at updated_at DATETIME NOT NULL COMMENT \'Дата обновления\', CHANGE parent_id parent_id VARCHAR(255) DEFAULT NULL COMMENT \'Лицевой счёт абонента\'');
        $this->addSql('ALTER TABLE clusters CHANGE description description VARCHAR(255) NOT NULL COMMENT \'Описание\', CHANGE created_at created_at DATETIME NOT NULL COMMENT \'Дата создания\', CHANGE updated_at updated_at DATETIME NOT NULL COMMENT \'Дата обновления\'');
        $this->addSql('ALTER TABLE users CHANGE username username VARCHAR(180) NOT NULL COMMENT \'Логин\', CHANGE roles roles JSON NOT NULL COMMENT \'Роли\', CHANGE password password VARCHAR(255) NOT NULL COMMENT \'Захэшированный пароль\'');
        $this->addSql('ALTER TABLE filials CHANGE title title VARCHAR(255) NOT NULL COMMENT \'Название\'');
        $this->addSql('ALTER TABLE district_operators CHANGE active active TINYINT(1) DEFAULT \'1\' NOT NULL COMMENT \'Активный\', CHANGE title title VARCHAR(255) DEFAULT NULL COMMENT \'Название\'');
        $this->addSql('ALTER TABLE debtors CHANGE abon_code abon_code VARCHAR(255) NOT NULL COMMENT \'Лицевой счёт абонента\', CHANGE pack_version pack_version CHAR(36) DEFAULT NULL COMMENT \'Версия пакета(DC2Type:uuid)\', CHANGE saldo_gaz saldo_gaz NUMERIC(10, 2) UNSIGNED DEFAULT NULL COMMENT \'Задолженность за поставку газа\', CHANGE saldo_date_gaz saldo_date_gaz DATETIME DEFAULT NULL COMMENT \'Дата возникновения задолженности за поставку газа\', CHANGE saldo_to saldo_to NUMERIC(10, 2) UNSIGNED DEFAULT NULL COMMENT \'Задолженность за тех. обслуживание\', CHANGE saldo_date_to saldo_date_to DATETIME DEFAULT NULL COMMENT \'Дата возникновения задолженности за тех. обслуживание\', CHANGE created_at created_at DATETIME NOT NULL COMMENT \'Дата создания\', CHANGE updated_at updated_at DATETIME NOT NULL COMMENT \'Дата обновления\', CHANGE check_balance_at check_balance_at DATE DEFAULT NULL COMMENT \'Дата проверки баланса в веб саупг\', CHANGE sms_channel sms_channel TINYINT(1) DEFAULT \'0\' NOT NULL COMMENT \'Уведомления через канал смс\', CHANGE email_channel email_channel TINYINT(1) DEFAULT \'0\' NOT NULL COMMENT \'Уведомления через канал почты\', CHANGE crm_channel crm_channel TINYINT(1) DEFAULT \'0\' NOT NULL COMMENT \'Уведомления через CRM(САУПГ)\', CHANGE lkk_notification_count lkk_notification_count SMALLINT UNSIGNED DEFAULT 0 NOT NULL COMMENT \'Количество удачных запросов в лкк на уведомление\', CHANGE address_fias_guid address_fias_guid VARCHAR(255) DEFAULT NULL COMMENT \'ФИАС guid адреса\', CHANGE gprs gprs TINYINT(1) DEFAULT \'0\' NOT NULL COMMENT \'Удаленный доступ по GPRS\'');
        $this->addSql('ALTER TABLE generator_pack_info CHANGE version version CHAR(36) NOT NULL COMMENT \'Версия пакета(DC2Type:uuid)\', CHANGE init_rows_count init_rows_count INT UNSIGNED DEFAULT NULL COMMENT \'Изначальное количество строк\', CHANGE processed_rows_count processed_rows_count INT UNSIGNED NOT NULL COMMENT \'Количество обработанных строк\', CHANGE is_loaded is_loaded TINYINT(1) NOT NULL COMMENT \'Загружен ли архив\', CHANGE is_unpacked is_unpacked TINYINT(1) NOT NULL COMMENT \'Распакован ли архив\', CHANGE is_processed is_processed TINYINT(1) NOT NULL COMMENT \'Обработан ли пакет\', CHANGE metadata metadata LONGTEXT DEFAULT NULL COMMENT \'Метаданные. Переменные в пакете(DC2Type:simple_array)\', CHANGE created_at created_at DATETIME NOT NULL COMMENT \'Дата создания\', CHANGE updated_at updated_at DATETIME NOT NULL COMMENT \'Дата обновления\'');
        $this->addSql('ALTER TABLE house_types CHANGE created_at created_at DATETIME NOT NULL COMMENT \'Дата создания\', CHANGE updated_at updated_at DATETIME NOT NULL COMMENT \'Дата обновления\'');
        $this->addSql('ALTER TABLE actions CHANGE description description VARCHAR(255) DEFAULT NULL COMMENT \'Описание\', CHANGE alias alias VARCHAR(255) DEFAULT NULL COMMENT \'Алиас\', CHANGE created_at created_at DATETIME NOT NULL COMMENT \'Дата создания\', CHANGE updated_at updated_at DATETIME NOT NULL COMMENT \'Дата обновления\'');
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE actions CHANGE description description VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE alias alias VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE clusters CHANGE description description VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE crm_statuses CHANGE alias alias VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE description description VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE debtors CHANGE abon_code abon_code VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE pack_version pack_version CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:uuid)\', CHANGE saldo_gaz saldo_gaz NUMERIC(10, 2) UNSIGNED DEFAULT NULL, CHANGE saldo_date_gaz saldo_date_gaz DATETIME DEFAULT NULL, CHANGE saldo_to saldo_to NUMERIC(10, 2) UNSIGNED DEFAULT NULL, CHANGE saldo_date_to saldo_date_to DATETIME DEFAULT NULL, CHANGE check_balance_at check_balance_at DATE DEFAULT NULL, CHANGE lkk_notification_count lkk_notification_count SMALLINT UNSIGNED DEFAULT 0 NOT NULL, CHANGE sms_channel sms_channel TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE crm_channel crm_channel TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE email_channel email_channel TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE address_fias_guid address_fias_guid VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE gprs gprs TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE district_operators CHANGE active active TINYINT(1) DEFAULT \'1\' NOT NULL, CHANGE title title VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE events CHANGE alias alias VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE description description VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE tpl tpl VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE events_messages CHANGE event event VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE parent_id parent_id VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE type type SMALLINT UNSIGNED DEFAULT NULL, CHANGE message message LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE is_resolved is_resolved TINYINT(1) DEFAULT \'0\' NOT NULL, CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE filials CHANGE title title VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE generator_pack_info CHANGE version version CHAR(36) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:uuid)\', CHANGE init_rows_count init_rows_count INT UNSIGNED DEFAULT NULL, CHANGE processed_rows_count processed_rows_count INT UNSIGNED NOT NULL, CHANGE is_loaded is_loaded TINYINT(1) NOT NULL, CHANGE is_unpacked is_unpacked TINYINT(1) NOT NULL, CHANGE is_processed is_processed TINYINT(1) NOT NULL, CHANGE metadata metadata LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:simple_array)\', CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE house_types CHANGE created_at created_at DATETIME NOT NULL, CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE users CHANGE username username VARCHAR(180) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE roles roles JSON NOT NULL, CHANGE password password VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
