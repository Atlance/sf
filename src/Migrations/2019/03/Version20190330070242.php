<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190330070242 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE events ADD tpl VARCHAR(255) DEFAULT NULL');
        $this->addSql('DELETE FROM events_messages');
        $this->addSql("UPDATE events SET alias = 'debtor_gaz', tpl = 'Абонент {{ abonCode }}: стал должником за Газ с долгом в размере {{ saldoGaz }}', description = 'Стал должником за Газ' WHERE alias = 'debtor'");
        $this->addSql(
            "INSERT IGNORE INTO events (alias, description, tpl)
VALUES ('debtor_to', 'Стал должником за Техническое обслуживание', 'Абонент {{ abonCode }}: стал должником за Техническое обслуживание с долгом в размере {{ saldoTo }}')"
        );
        $this->addSql("UPDATE events SET tpl = 'Абонент {{abonCode}}: Перестал быть должником' WHERE alias = 'out_of_debt'");
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE events DROP tpl');
    }
}
