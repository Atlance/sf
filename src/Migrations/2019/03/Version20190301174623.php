<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190301174623 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE debt_module.clusters SET description = \'Злостные должники (долг >= 30.000)\' WHERE id = 1');
        $this->addSql('UPDATE debt_module.clusters SET description = \'Общая группа (долг < 30.000)\' WHERE id = 2');
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE debt_module.clusters SET description = \'Злостные должники (долг >= 50.000)\' WHERE id = 1');
        $this->addSql('UPDATE debt_module.clusters SET description = \'Общая группа (долг < 50.000)\' WHERE id = 2');
    }
}
