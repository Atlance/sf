<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190330115302 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT IGNORE INTO events (alias, description, tpl) VALUES ('add_cluster_to_debtor', 'Должнику присвоен кластер', 'Присвоен кластер {{ cluster.description|raw }}')");
        $this->addSql("INSERT IGNORE INTO events (alias, description, tpl) VALUES ('lkk_notification_to_debtor', 'Отправлено автоматическое уведомление о долге', 'Отправлено автоматическое уведомление о долге')");
        $this->addSql("INSERT IGNORE INTO events (alias, description, tpl) VALUES ('crm_notification_to_debtor', 'Отправлена задача на взыскание долга в СУВК', 'Отправлена задача на взыскание долга в СУВК')");
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM events_messages WHERE event IN ('add_cluster_to_debtor', 'lkk_notification_to_debtor', 'crm_notification_to_debtor')");
        $this->addSql("DELETE FROM events WHERE alias IN ('add_cluster_to_debtor', 'lkk_notification_to_debtor', 'crm_notification_to_debtor')");
    }
}
