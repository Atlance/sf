<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190330083125 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql("INSERT IGNORE INTO events (alias, description, tpl) VALUES ('out_of_gaz_debt', 'Перестал быть должником за Газ', 'Абонент {{ abonCode }}: Перестал быть должником за Газ')");
        $this->addSql("INSERT IGNORE INTO events (alias, description, tpl) VALUES ('out_of_to_debt', 'Перестал быть должником за Техническое обслуживание', 'Абонент {{ abonCode }}: Перестал быть должником за Техническое обслуживание')");
        $this->addSql("DELETE FROM events_messages WHERE event = 'out_of_debt'");
        $this->addSql("DELETE FROM events WHERE alias = 'out_of_debt'");
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql("INSERT IGNORE INTO events (alias, description, tpl) VALUES ('out_of_debt', 'Перестал быть должником', 'Абонент {abonCode}: Перестал быть должником')");
        $this->addSql("DELETE FROM events_messages WHERE event IN ('out_of_gaz_debt', 'out_of_to_debt')");
        $this->addSql("DELETE FROM events WHERE alias IN ('out_of_gaz_debt', 'out_of_to_debt')");
    }
}
