<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190319034945 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT IGNORE INTO filials (id, title) VALUES (16, 'Мытищимежрайгаз'), (23,'Одинцовомежрайгаз'), (24,'Коломнамежрайгаз'), (25,'Подольскмежрайгаз'), (26,'Ступиномежрайгаз'), (27,'Ногинскмежрайгаз'), (30,'Балашихамежрайгаз'), (32,'Дмитровмежрайгаз'), (34,'Раменскоемежрайгаз'), (38,'Красногорскмежрайгаз')");
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM filials WHERE id IN (16, 23, 24, 25, 26, 27, 30, 32, 34, 38)');
    }
}
