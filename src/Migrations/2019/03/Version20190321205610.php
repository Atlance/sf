<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190321205610 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE events_messages ADD parent_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE events_messages ADD CONSTRAINT FK_26368695727ACA70 FOREIGN KEY (parent_id) REFERENCES debtors (abon_code)');
        $this->addSql('CREATE INDEX IDX_26368695727ACA70 ON events_messages (parent_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE events_messages DROP FOREIGN KEY FK_26368695727ACA70');
        $this->addSql('DROP INDEX IDX_26368695727ACA70 ON events_messages');
        $this->addSql('ALTER TABLE events_messages DROP parent_id');
    }
}
