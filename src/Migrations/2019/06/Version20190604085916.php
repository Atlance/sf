<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190604085916 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX fias_idx ON debtors');
        $this->addSql('ALTER TABLE debtors ADD house_fias_guid CHAR(36) DEFAULT NULL COMMENT \'ФИАС guid дома(DC2Type:guid)\', ADD street_fias_guid CHAR(36) DEFAULT NULL COMMENT \'ФИАС guid улицы(DC2Type:guid)\', ADD city_fias_guid CHAR(36) DEFAULT NULL COMMENT \'ФИАС guid населённого пункта(DC2Type:guid)\', DROP address_fias_guid');
        $this->addSql('CREATE INDEX house_idx ON debtors (house_fias_guid)');
        $this->addSql('CREATE INDEX street_idx ON debtors (street_fias_guid)');
        $this->addSql('CREATE INDEX city_idx ON debtors (city_fias_guid)');
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX house_idx ON debtors');
        $this->addSql('DROP INDEX street_idx ON debtors');
        $this->addSql('DROP INDEX city_idx ON debtors');
        $this->addSql('ALTER TABLE debtors ADD address_fias_guid VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'ФИАС guid адреса\', DROP house_fias_guid, DROP street_fias_guid, DROP city_fias_guid');
        $this->addSql('CREATE INDEX fias_idx ON debtors (address_fias_guid)');
    }
}
