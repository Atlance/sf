<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190213085855 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql("INSERT IGNORE INTO clusters (id, description, created_at, updated_at) VALUES (1, 'Злостные должники (долг >= 50.000)', '2019-02-13 11:54:27', '2019-02-13 11:54:27')");
        $this->addSql("INSERT IGNORE INTO clusters (id, description, created_at, updated_at) VALUES (2, 'Общая группа (долг < 50.000)', '2019-02-13 11:54:27', '2019-02-13 11:54:27')");
        $this->addSql('UPDATE debtors SET cluster_id = 1 WHERE saldo_gaz >= 50000');
        $this->addSql('UPDATE debtors SET cluster_id = 2 WHERE saldo_gaz < 50000');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE debtors SET cluster_id = NULL');
        $this->addSql('DELETE FROM clusters WHERE `id`= 1');
        $this->addSql('DELETE FROM clusters WHERE `id`= 2');
    }
}
