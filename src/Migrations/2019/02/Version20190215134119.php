<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20190215134119.
 */
final class Version20190215134119 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE IF NOT EXISTS clusters_actions (action_id INT UNSIGNED NOT NULL, cluster_id INT UNSIGNED NOT NULL, INDEX IDX_BB74B6FA9D32F035 (action_id), INDEX IDX_BB74B6FAC36A3328 (cluster_id), PRIMARY KEY(action_id, cluster_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS actions (id INT UNSIGNED AUTO_INCREMENT NOT NULL, description VARCHAR(255) DEFAULT NULL, alias VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_548F1EFE16C6B94 (alias), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE clusters_actions ADD CONSTRAINT FK_BB74B6FA9D32F035 FOREIGN KEY (action_id) REFERENCES clusters (id)');
        $this->addSql('ALTER TABLE clusters_actions ADD CONSTRAINT FK_BB74B6FAC36A3328 FOREIGN KEY (cluster_id) REFERENCES actions (id)');
        $this->addSql("INSERT IGNORE INTO actions (id, description, alias, created_at, updated_at) VALUES (1, 'Автоматическое уведомление', 'auto_notification', NOW(), NOW()), (2, 'Отправка задачи в СУВК', 'task_to_crm', NOW(), NOW())");
        $this->addSql('INSERT IGNORE INTO clusters_actions (action_id, cluster_id) VALUES (1, 1), (1, 2), (2, 1)');
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE clusters_actions DROP FOREIGN KEY FK_BB74B6FAC36A3328');
        $this->addSql('DROP TABLE IF EXISTS clusters_actions');
        $this->addSql('DROP TABLE IF EXISTS actions');
    }
}
