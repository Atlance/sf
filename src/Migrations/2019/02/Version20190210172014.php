<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190210172014 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $sql = <<<SQL
INSERT IGNORE INTO events (alias, description) VALUES (
'debtor', 'Стал должником'
);
INSERT IGNORE INTO events (alias, description) VALUES (
'out_of_debt', 'Перестал быть должником'
);
SQL;
        $this->addSql($sql);
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $sql = <<<SQL
DELETE FROM events WHERE alias in ('debtor', 'out_of_debt');
SQL;

        $this->addSql($sql);
    }
}
