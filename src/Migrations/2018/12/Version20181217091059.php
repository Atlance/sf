<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181217091059 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE IF NOT EXISTS cluster (id INT UNSIGNED AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS filial (id INT UNSIGNED NOT NULL, title VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_F55997592B36786B (title), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS event (alias VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(alias)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS debtor (abon_code VARCHAR(255) NOT NULL, filial_id INT UNSIGNED DEFAULT NULL, cluster_id INT UNSIGNED DEFAULT NULL, pack_version CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', saldo_gaz NUMERIC(10, 4) UNSIGNED DEFAULT NULL, saldo_date_gaz DATETIME DEFAULT NULL, saldo_to NUMERIC(10, 4) UNSIGNED DEFAULT NULL, saldo_date_to DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_EDCC8CAE299B2577 (filial_id), INDEX IDX_EDCC8CAEC36A3328 (cluster_id), INDEX IDX_EDCC8CAE845543BE (pack_version), PRIMARY KEY(abon_code)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS generator_pack_info (version CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', init_rows_count INT UNSIGNED DEFAULT NULL, processed_rows_count INT UNSIGNED NOT NULL, is_loaded TINYINT(1) NOT NULL, is_unpacked TINYINT(1) NOT NULL, is_processed TINYINT(1) NOT NULL, metadata LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(version)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS event_message (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary_ordered_time)\', event VARCHAR(255) DEFAULT NULL, type SMALLINT UNSIGNED DEFAULT NULL, message LONGTEXT NOT NULL, is_resolved TINYINT(1) DEFAULT \'0\' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, object VARCHAR(255) NOT NULL, INDEX IDX_33EA99D03BAE0AA7 (event), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE debtor ADD CONSTRAINT FK_EDCC8CAE299B2577 FOREIGN KEY (filial_id) REFERENCES filial (id)');
        $this->addSql('ALTER TABLE debtor ADD CONSTRAINT FK_EDCC8CAEC36A3328 FOREIGN KEY (cluster_id) REFERENCES cluster (id)');
        $this->addSql('ALTER TABLE debtor ADD CONSTRAINT FK_EDCC8CAE845543BE FOREIGN KEY (pack_version) REFERENCES generator_pack_info (version)');
        $this->addSql('ALTER TABLE event_message ADD CONSTRAINT FK_33EA99D03BAE0AA7 FOREIGN KEY (event) REFERENCES event (alias)');
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE debtor DROP FOREIGN KEY FK_EDCC8CAEC36A3328');
        $this->addSql('ALTER TABLE debtor DROP FOREIGN KEY FK_EDCC8CAE299B2577');
        $this->addSql('ALTER TABLE event_message DROP FOREIGN KEY FK_33EA99D03BAE0AA7');
        $this->addSql('ALTER TABLE debtor DROP FOREIGN KEY FK_EDCC8CAE845543BE');
        $this->addSql('DROP TABLE IF EXISTS cluster');
        $this->addSql('DROP TABLE IF EXISTS filial');
        $this->addSql('DROP TABLE IF EXISTS event');
        $this->addSql('DROP TABLE IF EXISTS debtor');
        $this->addSql('DROP TABLE IF EXISTS generator_pack_info');
        $this->addSql('DROP TABLE IF EXISTS event_message');
    }
}
