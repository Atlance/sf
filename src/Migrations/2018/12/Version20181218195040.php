<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181218195040 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE debtor DROP FOREIGN KEY FK_EDCC8CAEC36A3328');
        $this->addSql('ALTER TABLE event_message DROP FOREIGN KEY FK_33EA99D03BAE0AA7');
        $this->addSql('ALTER TABLE debtor DROP FOREIGN KEY FK_EDCC8CAE299B2577');
        $this->addSql('CREATE TABLE IF NOT EXISTS clusters (id INT UNSIGNED AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS filials (id INT UNSIGNED NOT NULL, title VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_94C7FCC2B36786B (title), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS events (alias VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(alias)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS debtors (abon_code VARCHAR(255) NOT NULL, filial_id INT UNSIGNED DEFAULT NULL, cluster_id INT UNSIGNED DEFAULT NULL, crm_status_id INT UNSIGNED DEFAULT NULL, pack_version CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', saldo_gaz NUMERIC(10, 4) UNSIGNED DEFAULT NULL, saldo_date_gaz DATETIME DEFAULT NULL, saldo_to NUMERIC(10, 4) UNSIGNED DEFAULT NULL, saldo_date_to DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_2A8D8D68299B2577 (filial_id), INDEX IDX_2A8D8D68C36A3328 (cluster_id), INDEX IDX_2A8D8D682A9DFD90 (crm_status_id), INDEX IDX_2A8D8D68845543BE (pack_version), PRIMARY KEY(abon_code)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS events_messages (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary_ordered_time)\', event VARCHAR(255) DEFAULT NULL, type SMALLINT UNSIGNED DEFAULT NULL, message LONGTEXT NOT NULL, is_resolved TINYINT(1) DEFAULT \'0\' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, object VARCHAR(255) NOT NULL, INDEX IDX_263686953BAE0AA7 (event), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS crm_statuses (id INT UNSIGNED AUTO_INCREMENT NOT NULL, alias VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_257753BAE16C6B94 (alias), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE debtors ADD CONSTRAINT FK_2A8D8D68299B2577 FOREIGN KEY (filial_id) REFERENCES filials (id)');
        $this->addSql('ALTER TABLE debtors ADD CONSTRAINT FK_2A8D8D68C36A3328 FOREIGN KEY (cluster_id) REFERENCES clusters (id)');
        $this->addSql('ALTER TABLE debtors ADD CONSTRAINT FK_2A8D8D682A9DFD90 FOREIGN KEY (crm_status_id) REFERENCES crm_statuses (id)');
        $this->addSql('ALTER TABLE debtors ADD CONSTRAINT FK_2A8D8D68845543BE FOREIGN KEY (pack_version) REFERENCES generator_pack_info (version)');
        $this->addSql('ALTER TABLE events_messages ADD CONSTRAINT FK_263686953BAE0AA7 FOREIGN KEY (event) REFERENCES events (alias)');
        $this->addSql('DROP TABLE IF EXISTS cluster');
        $this->addSql('DROP TABLE IF EXISTS debtor');
        $this->addSql('DROP TABLE IF EXISTS event');
        $this->addSql('DROP TABLE IF EXISTS event_message');
        $this->addSql('DROP TABLE IF EXISTS filial');
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE debtors DROP FOREIGN KEY FK_2A8D8D68C36A3328');
        $this->addSql('ALTER TABLE debtors DROP FOREIGN KEY FK_2A8D8D68299B2577');
        $this->addSql('ALTER TABLE events_messages DROP FOREIGN KEY FK_263686953BAE0AA7');
        $this->addSql('ALTER TABLE debtors DROP FOREIGN KEY FK_2A8D8D682A9DFD90');
        $this->addSql('CREATE TABLE IF NOT EXISTS cluster (id INT UNSIGNED AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE IF NOT EXISTS debtor (abon_code VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, filial_id INT UNSIGNED DEFAULT NULL, cluster_id INT UNSIGNED DEFAULT NULL, pack_version CHAR(36) DEFAULT NULL COLLATE utf8mb4_unicode_ci COMMENT \'(DC2Type:guid)\', saldo_gaz NUMERIC(10, 4) UNSIGNED DEFAULT NULL, saldo_date_gaz DATETIME DEFAULT NULL, saldo_to NUMERIC(10, 4) UNSIGNED DEFAULT NULL, saldo_date_to DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, is_crm TINYINT(1) DEFAULT \'0\' NOT NULL, INDEX IDX_EDCC8CAEC36A3328 (cluster_id), INDEX IDX_EDCC8CAE299B2577 (filial_id), INDEX IDX_EDCC8CAE845543BE (pack_version), PRIMARY KEY(abon_code)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE IF NOT EXISTS event (alias VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, description VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(alias)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE IF NOT EXISTS event_message (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid_binary_ordered_time)\', event VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, type SMALLINT UNSIGNED DEFAULT NULL, message LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, is_resolved TINYINT(1) DEFAULT \'0\' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, object VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, INDEX IDX_33EA99D03BAE0AA7 (event), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE IF NOT EXISTS filial (id INT UNSIGNED NOT NULL, title VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, UNIQUE INDEX UNIQ_F55997592B36786B (title), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE debtor ADD CONSTRAINT FK_EDCC8CAE299B2577 FOREIGN KEY (filial_id) REFERENCES filial (id)');
        $this->addSql('ALTER TABLE debtor ADD CONSTRAINT FK_EDCC8CAE845543BE FOREIGN KEY (pack_version) REFERENCES generator_pack_info (version)');
        $this->addSql('ALTER TABLE debtor ADD CONSTRAINT FK_EDCC8CAEC36A3328 FOREIGN KEY (cluster_id) REFERENCES cluster (id)');
        $this->addSql('ALTER TABLE event_message ADD CONSTRAINT FK_33EA99D03BAE0AA7 FOREIGN KEY (event) REFERENCES event (alias)');
        $this->addSql('DROP TABLE IF EXISTS clusters');
        $this->addSql('DROP TABLE IF EXISTS filials');
        $this->addSql('DROP TABLE IF EXISTS events');
        $this->addSql('DROP TABLE IF EXISTS debtors');
        $this->addSql('DROP TABLE IF EXISTS events_messages');
        $this->addSql('DROP TABLE IF EXISTS crm_statuses');
    }
}
