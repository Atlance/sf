<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181222220018 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE debtors DROP FOREIGN KEY FK_2A8D8D682A9DFD90');
        $this->addSql('ALTER TABLE debtors ADD CONSTRAINT FK_2A8D8D682A9DFD90 FOREIGN KEY (crm_status_id) REFERENCES crm_statuses (id) ON DELETE SET NULL');
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE debtors DROP FOREIGN KEY FK_2A8D8D682A9DFD90');
        $this->addSql('ALTER TABLE debtors ADD CONSTRAINT FK_2A8D8D682A9DFD90 FOREIGN KEY (crm_status_id) REFERENCES crm_statuses (id)');
    }
}
