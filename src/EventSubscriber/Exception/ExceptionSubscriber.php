<?php

declare(strict_types=1);

namespace App\EventSubscriber\Exception;

use Lexik\Bundle\JWTAuthenticationBundle\Events as JWTEvents;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\ExpiredTokenException;
use Phpro\ApiProblem\Exception\ApiProblemException;
use Phpro\ApiProblem\Http\ExceptionApiProblem;
use Phpro\ApiProblem\Http\ForbiddenProblem;
use Phpro\ApiProblem\Http\HttpApiProblem;
use Phpro\ApiProblem\Http\UnauthorizedProblem;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

/**
 * ExceptionSubscriber.
 */
class ExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => [
                ['onKernelException', 255],
            ],
            JWTEvents::AUTHENTICATION_FAILURE => [
                ['onKernelException', 255],
            ],
            JWTEvents::JWT_EXPIRED => [
                ['onKernelException', 255],
            ],
        ];
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException($event)
    {
        $e = $event->getException();
        $code = $e instanceof HttpException ? $e->getStatusCode() : $e->getCode();

        if ($e instanceof ApiProblemException) {
            $event->setResponse(
                new JsonResponse(
                    ['success' => false, 'errors' => [$e->getApiProblem()->toArray()]],
                    $e->getCode(),
                    ['Content-Type' => 'application/vnd.api+json;charset=UTF-8']
                )
            );

            return;
        }

        if ($e instanceof BadCredentialsException) {
            $problem = new HttpApiProblem(JsonResponse::HTTP_BAD_REQUEST, ['detail' => $e->getMessage()]);
            $event->setResponse(
                new JsonResponse(
                    ['success' => false, 'errors' => [$problem->toArray()]],
                    JsonResponse::HTTP_BAD_REQUEST,
                    ['Content-Type' => 'application/vnd.api+json;charset=UTF-8']
                )
            );

            return;
        }

        if ($e instanceof AuthenticationCredentialsNotFoundException
            || $e instanceof ExpiredTokenException
        ) {
            $problem = new UnauthorizedProblem($e->getMessage());
            $event->setResponse(
                new JsonResponse(
                    ['success' => false, 'errors' => [$problem->toArray()]],
                    JsonResponse::HTTP_UNAUTHORIZED,
                    ['Content-Type' => 'application/vnd.api+json;charset=UTF-8']
                )
            );

            return;
        }

        if ($code < 100) {
            $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        }

        switch ($code) {
            case JsonResponse::HTTP_UNAUTHORIZED:
                $problem = new UnauthorizedProblem($e->getMessage());
                break;
            case JsonResponse::HTTP_FORBIDDEN:
                $problem = new ForbiddenProblem($e->getMessage());
                break;
            case JsonResponse::HTTP_NOT_FOUND:
                $problem = new HttpApiProblem(JsonResponse::HTTP_NOT_FOUND, ['detail' => $e->getMessage()]);
                break;
            case JsonResponse::HTTP_BAD_REQUEST:
                $problem = new HttpApiProblem(JsonResponse::HTTP_BAD_REQUEST, ['detail' => $e->getMessage()]);
                break;
            default:
                $problem = new ExceptionApiProblem($e);
        }

        $event->setResponse(
            new JsonResponse(
                ['success' => false, 'errors' => [$problem->toArray()]],
                $code,
                ['Content-Type' => 'application/vnd.api+json;charset=UTF-8']
            )
        );
    }
}
