<?php

declare(strict_types=1);

namespace App\EventSubscriber\Security;

use App\Entity\SecretToken;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SecretTokenSubscriber implements EventSubscriberInterface
{
    /**
     * @var string
     */
    private $secretKey;

    public function __construct(string $secretKey)
    {
        $this->secretKey = $secretKey;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            Events::postLoad => ['setSecretKey'],
        ];
    }

    public function setSecretKey(LifecycleEventArgs $args): void
    {
        $secretToken = $args->getObject();

        if (!$secretToken instanceof SecretToken) {
            return;
        }

        $secretToken->setSecretKey($this->secretKey);
    }
}
