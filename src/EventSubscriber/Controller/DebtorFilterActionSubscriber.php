<?php

declare(strict_types=1);

namespace App\EventSubscriber\Controller;

use App\Entity\Cluster;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class DebtorFilterActionSubscriber.
 */
class DebtorFilterActionSubscriber implements EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => [
                ['createConditions', 255],
            ],
        ];
    }

    /**
     * @param FilterControllerEvent $event
     *
     * @throws \Exception
     */
    public function createConditions(FilterControllerEvent $event)
    {
        $request = $event->getRequest();

        if ($request->attributes->get('_route') !== 'api_v1_crm_debtors_filter') {
            return;
        }
        $conditions = [
            'page' => $request->query->get('page', 1),
            'limit' => $request->query->get('limit', 15),
            'where' => [
                'eq' => [],
                'isNull' => [],
                'neq' => ['debtors_cluster_id' => Cluster::PAYED_DEBT_GAZ_CLUSTER],
                'lte_date' => ['debtors_check_balance_at' => (new \DateTime())->format('Y:m:d')],
            ],
            'order' => $request->query->get('order', []),
            'total_debtor_gaz' => null,
        ];

        $gprs = \json_decode($request->query->get('gprs', ''));
        $smsChannel = \json_decode($request->query->get('sms_channel', ''));
        $emailChannel = \json_decode($request->query->get('email_channel', ''));
        $crmChannel = \json_decode($request->query->get('crm_channel', ''));

        if ($filial = $request->query->get('filial', null)) {
            $conditions['where']['eq']['filial_id'] = $filial;
        }

        if ($abonCode = $request->query->get('abon_code', null)) {
            $conditions['where']['eq']['debtors_abon_code'] = $abonCode;
        }

        if ($saldoDateGazInMonths = $request->query->get('saldo_date_gaz_in_months', null)) {
            $conditions['where']['lt_date']['debtors_saldo_date_gaz']
                = (new \DateTime("now -$saldoDateGazInMonths months"))->format('Y-m-d H:i:s');
        }

        if ($status = $request->query->get('status', null)) {
            $conditions['where']['eq']['crm_status_id'] = $status;
        }

        if ($saldoGazGt = $request->query->get('saldo_gaz_gt', null)) {
            $conditions['where']['gt']['debtors_saldo_gaz'] = $saldoGazGt;
        }

        if ($cluster = $request->query->get('cluster', null)) {
            $conditions['where']['eq']['cluster_id'] = $cluster;
        }

        if ($fiasGuid = $request->query->get('fias_address_guid', null)) {
            $conditions['fias_address_guid'] = $fiasGuid;
        }

        if ($houseType = $request->query->get('house_type', null)) {
            $conditions['where']['eq']['house_type_id'] = $houseType;
        }

        if ($distinctOperator = $request->query->get('distinct_operator', null)) {
            $conditions['where']['eq']['distinct_operator_id'] = $distinctOperator;
        }

        if (0 === $status) {
            $conditions['where']['isNull']['crm_status_id'] = $status;
        }

        if (null !== $gprs) {
            $conditions['where']['eq']['debtors_gprs'] = $gprs;
        }

        if (null !== $smsChannel) {
            $conditions['where']['eq']['debtors_sms_channel'] = $smsChannel;
        }

        if (null !== $emailChannel) {
            $conditions['where']['eq']['debtors_email_channel'] = $emailChannel;
        }

        if (null !== $crmChannel) {
            $conditions['where']['eq']['debtors_crm_channel'] = $crmChannel;
        }
        $request->query->replace($conditions);
    }
}
