<?php

declare(strict_types=1);

namespace App\EventSubscriber\Controller;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class BeforeActionSubscriber.
 */
class BeforeActionSubscriber implements EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => [
                ['convertJsonContentToArray', 255],
            ],
        ];
    }

    /**
     * @param FilterControllerEvent $event
     *
     * @throws \Exception
     */
    public function convertJsonContentToArray(FilterControllerEvent $event)
    {
        $request = $event->getRequest();

        if (\in_array($request->getRealMethod(), ['GET', 'DELETE'])) {
            return;
        }

        if (\preg_match('/^\/api(?!\/doc$)/i', $request->getRequestUri())) {
            if ($request->getContent()) {
                if ($request->getContentType() != 'json' || !$request->getContent()) {
                    throw new \Exception('Invalid Content-Type. Expected application/json.', Response::HTTP_BAD_REQUEST);
                }

                $data = \json_decode($request->getContent(), true);
                if (\json_last_error() !== JSON_ERROR_NONE) {
                    throw new \Exception('JSON parse error: '.\json_last_error_msg(), Response::HTTP_BAD_REQUEST);
                }
                $request->request->replace(\is_array($data) ? $data : []);
            }
        }
    }
}
