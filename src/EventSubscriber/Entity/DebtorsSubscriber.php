<?php

declare(strict_types=1);

namespace App\EventSubscriber\Entity;

use App\Entity\Cluster;
use App\Entity\DebtEventMessage;
use App\Entity\Debtor;
use App\Entity\Event;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Twig\Environment;
use Twig\Loader\ArrayLoader;

/**
 * Class DebtorsSubscriber.
 */
class DebtorsSubscriber implements EventSubscriber
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var ArrayCollection */
    private $messages;

    /** @var Environment */
    private $twig;

    /** @var Event */
    private $debtorGazEvent;

    /** @var Event */
    private $debtorToEvent;

    /** @var Event */
    private $outOfGazDebtEvent;

    /** @var Event */
    private $outOfToDebtEvent;

    /** @var Event */
    private $addClusterToDebtorEvent;

    /** @var Event */
    private $lkkNotifyEvent;

    /** @var Event */
    private $crmNotifyEvent;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->messages = new ArrayCollection();
        $this->twig = new Environment(new ArrayLoader());
        $this->debtorGazEvent = $this->em->find(Event::class, Event::DEBTOR_GAZ_ALIAS);
        $this->debtorToEvent = $this->em->find(Event::class, Event::DEBTOR_TO_ALIAS);
        $this->outOfGazDebtEvent = $this->em->find(Event::class, Event::OUT_OF_GAZ_DEBT_ALIAS);
        $this->outOfToDebtEvent = $this->em->find(Event::class, Event::OUT_OF_TO_DEBT_ALIAS);
        $this->addClusterToDebtorEvent = $this->em->find(Event::class, Event::ADD_CLUSTER_TO_DEBTOR);
        $this->lkkNotifyEvent = $this->em->find(Event::class, Event::LKK_NOTIFICATION_TO_DEBTOR);
        $this->crmNotifyEvent = $this->em->find(Event::class, Event::CRM_NOTIFICATION_TO_DEBTOR);
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            Events::preFlush,
            Events::postFlush,
            Events::preUpdate,
        ];
    }

    /**
     * @param PreUpdateEventArgs $args
     *
     * @throws \Exception
     * @throws \Throwable
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof Debtor) {
            return;
        }

        if ($args->hasChangedField('saldoGaz')) {
            $oldSaldoGaz = (float) $args->getOldValue('saldoGaz');
            $newSaldoGaz = (float) $args->getNewValue('saldoGaz');

            // Стал должником за Газ
            if ($oldSaldoGaz == 0 && $newSaldoGaz > 0) {
                $this->makeMessage($entity, $this->debtorGazEvent);
            }

            // Перестал быть должником за Газ
            if ($oldSaldoGaz > 0 && $newSaldoGaz == 0) {
                $this->makeMessage($entity, $this->outOfGazDebtEvent);
            }
        }

        if ($args->hasChangedField('saldoTo')) {
            $oldSaldoTo = (float) $args->getOldValue('saldoTo');
            $newSaldoTo = (float) $args->getNewValue('saldoTo');

            // Стал должником Техничесское обслуживание
            if ($oldSaldoTo == 0 && $newSaldoTo > 0) {
                $this->makeMessage($entity, $this->debtorToEvent);
            }

            // Перестал быть должником за Техничесское обслуживание
            if ($oldSaldoTo > 0 && $newSaldoTo == 0) {
                $this->makeMessage($entity, $this->outOfToDebtEvent);
            }
        }

        // Должнику присвоен кластер
        if ($args->hasChangedField('cluster')) {
            if ($args->getNewValue('cluster') !== null) {
                $this->makeMessage($entity, $this->addClusterToDebtorEvent);
            }
        }

        // Отправлено автоматическое уведомление о долге
        if ($args->hasChangedField('lkkNotificationCount')) {
            $this->makeMessage($entity, $this->lkkNotifyEvent);
        }

        // Отправлена задача на взыскание долга в СУВК
        if ($args->hasChangedField('crmChannel')) {
            if ($args->getNewValue('crmChannel')) {
                $this->makeMessage($entity, $this->crmNotifyEvent);
            }
        }
    }

    /**
     * @param PreFlushEventArgs $args
     *
     * @throws \Exception
     * @throws \Throwable
     */
    public function preFlush(PreFlushEventArgs $args)
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        // Новые должники в бд появляются только из пакета от модуля 'генератор' и у них может быть долг
        // за Газ и/или Техничесское обслуживание.
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof Debtor) {
                // Стал должником за газ
                if ($entity->getSaldoGaz() > 0) {
                    $this->makeMessage($entity, $this->debtorGazEvent);
                }

                // Стал должником за Техничесское обслуживание
                if ($entity->getSaldoTo() > 0) {
                    $this->makeMessage($entity, $this->debtorToEvent);
                }

                // Должнику присвоен кластер
                if ($entity->getCluster() instanceof Cluster) {
                    $this->makeMessage($entity, $this->addClusterToDebtorEvent);
                }

                // Отправлено автоматическое уведомление о долге
                if ($entity->getLkkNotificationCount()) {
                    $this->makeMessage($entity, $this->lkkNotifyEvent);
                }

                // Отправлена задача на взыскание долга в СУВК
                if ($entity->isCrmChannel()) {
                    $this->makeMessage($entity, $this->crmNotifyEvent);
                }
            }
        }
    }

    /**
     * @param PostFlushEventArgs $args
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        $this->flushEventMessages();
    }

    /**
     * @param Debtor $debtor
     * @param Event  $event
     *
     * @throws \Throwable
     */
    private function makeMessage(Debtor $debtor, Event $event): void
    {
        $message = (new DebtEventMessage())->setParent($debtor)->setEvent($event);
        $message->setMessage($this->twig->createTemplate($event->getTpl())->render($message->getArrayVariables()));

        if (!$this->messages->contains($message)) {
            $this->messages->add($message);
        }
    }

    /**
     * Flush EventMessage[].
     */
    private function flushEventMessages()
    {
        if ($this->messages->count()) {
            foreach ($this->messages->toArray() as $message) {
                if ($message instanceof DebtEventMessage) {
                    if ($message->getParent()) {
                        $debtor = $this->em->find(Debtor::class, $message->getParent()->getId());
                        $message->setParent($debtor);
                        $this->em->persist($message);
                        $this->messages->removeElement($message);
                    }
                }
            }

            $this->em->flush();
        }
    }
}
