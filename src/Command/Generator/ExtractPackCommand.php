<?php

declare(strict_types=1);

namespace App\Command\Generator;

use App\Entity\GeneratorPackInfo;
use App\Helper\Gzip\Extractor;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ExtractPackCommand.
 */
class ExtractPackCommand extends Command
{
    use LockableTrait;

    protected static $defaultName = 'generator:pack:extract';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var string
     */
    private $uploadDir;

    /**
     * @param string $uploadDir
     */
    public function __construct(EntityManagerInterface $em, string $uploadDir)
    {
        $this->uploadDir = $uploadDir;
        $this->em = $em;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Распаковать архив пакета')
            ->addOption('ver', null, InputOption::VALUE_OPTIONAL, 'Версия пакета.')
            ->addOption('force', 'f', InputOption::VALUE_REQUIRED, 'после операции удалить архив (0/1)', 0)
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return;
        }

        $pack = null;
        if ($version = $input->getOption('ver')) {
            $pack = $this->em->getRepository(GeneratorPackInfo::class)->find($version);
        }

        if (!$pack instanceof GeneratorPackInfo) {
            $pack = $this->em->getRepository(GeneratorPackInfo::class)->findOneBy(
                [
                    'loaded' => true,
                    'unpacked' => false,
                    'processed' => false,
                ],
                [
                    'createdAt' => 'DESC',
                ]
            );
        }

        if ($pack instanceof GeneratorPackInfo) {
            $pack->setUploadDir($this->uploadDir);
            $gz = $pack->getFileFullPathByFormat('gz');
            $csv = $pack->getFileFullPathByFormat('csv');

            Extractor::extract($gz, $csv, (bool) $input->getOption('force'));
            $csv = $pack->getFileFullPathByFormat('csv');

            if (!\file_exists($csv)) {
                throw new \UnexpectedValueException("Could not open file: $csv");
            }

            if (!$handle = \fopen($csv, 'r')) {
                throw new \UnexpectedValueException("Could not open file: $csv");
            }
            $metadata = \fgetcsv($handle);
            \fclose($handle);

            $file = new \SplFileObject($csv);
            $file->seek($file->getSize());

            $this->em->persist(
                $pack->setUnpacked(true)
                    ->setInitRowsCount($file->key() - 1)
                    ->setMetadata($metadata)
            );

            $this->em->flush();
        }

        $this->release();
    }
}
