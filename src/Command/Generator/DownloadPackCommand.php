<?php

declare(strict_types=1);

namespace App\Command\Generator;

use App\Entity\GeneratorPackInfo;
use App\Service\Http\GuzzleHttpClient;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DownloadPackCommand.
 */
class DownloadPackCommand extends Command
{
    use LockableTrait;

    protected static $defaultName = 'generator:pack:download';

    /**
     * @var string
     */
    private $uploadDir;

    /**
     * @var string
     */
    private $apiUrl;

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var GuzzleHttpClient
     */
    private $client;

    /**
     * @param EntityManagerInterface $em
     * @param GuzzleHttpClient       $client
     * @param string                 $uploadDir
     * @param string                 $apiUrl
     */
    public function __construct(
        EntityManagerInterface $em,
        GuzzleHttpClient $client,
        string $uploadDir,
        string $apiUrl
    ) {
        $this->em = $em;
        $this->client = $client;
        $this->uploadDir = $uploadDir;
        $this->apiUrl = $apiUrl;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setDescription('Скачать актуальный пакет данных')
            ->addOption('ver', null, InputOption::VALUE_OPTIONAL, 'Версия пакета.')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return;
        }

        $httpClient = $this->client->getHttpClient();
        $pack = null;
        if ($version = $input->getOption('ver')) {
            $pack = $this->em->getRepository(GeneratorPackInfo::class)->find($version);
        }

        if (!$pack instanceof GeneratorPackInfo) {
            $pack = $this->em->getRepository(GeneratorPackInfo::class)->findOneBy(
                [
                    'loaded' => false,
                    'unpacked' => false,
                    'processed' => false,
                ],
                [
                    'createdAt' => 'DESC',
                ]
            );
        }

        if ($pack instanceof GeneratorPackInfo) {
            $pack->setUploadDir($this->uploadDir);

            if (!\file_exists($pack->getUploadDir())) {
                \mkdir($pack->getUploadDir(), 0775, true);
            }

            try {
                if ($response = $httpClient->request('GET', "{$this->apiUrl}/{$pack->getVersion()}")) {
                    if ($response->getStatusCode() === JsonResponse::HTTP_OK) {
                        if ($handle = \fopen($pack->getFileFullPathByFormat('gz'), 'w')) {
                            \flock($handle, LOCK_EX);
                            \fwrite($handle, $response->getBody()->getContents());
                            \flock($handle, LOCK_UN);
                            \fclose($handle);

                            $this->em->persist($pack->setLoaded(true));
                            $this->em->flush();
                        }
                    }
                }
            } catch (\Exception | GuzzleException $e) {
                $output->writeln($e->getMessage());
            }
        }

        if (!$pack instanceof GeneratorPackInfo) {
            if (!\file_exists($this->uploadDir)) {
                \mkdir($this->uploadDir, 0775, true);
            }

            try {
                if ($response = $httpClient->request('GET', $this->apiUrl)) {
                    if ($response->getStatusCode() === JsonResponse::HTTP_OK) {
                        if ('application/x-gzip' === $response->getHeader('Content-Type')[0]) {
                            $filename = \explode('=', $response->getHeader('Content-Disposition')[0])[1];
                            $version = \str_replace('.gz', '', $filename);
                            $pack = (new GeneratorPackInfo())->setUploadDir($this->uploadDir)->setVersion($version);

                            if ($handle = \fopen($pack->getFileFullPathByFormat('gz'), 'w')) {
                                \flock($handle, LOCK_EX);
                                \fwrite($handle, $response->getBody()->getContents());
                                \flock($handle, LOCK_UN);
                                \fclose($handle);

                                $this->em->persist($pack->setLoaded(true));
                                $this->em->flush();
                            }
                        }
                    }
                }
            } catch (\Exception | GuzzleException $e) {
                $output->writeln($e->getMessage());
            }
        }

        $this->release();
    }
}
