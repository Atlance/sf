<?php

declare(strict_types=1);

namespace App\Command\Generator;

use App\Entity\Cluster;
use App\Entity\DebtEventMessage;
use App\Entity\Debtor;
use App\Entity\Event;
use App\Entity\GeneratorPackInfo;
use App\Form\DebtorType;
use App\Service\WebSaupg\DebtorService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Class FlushPackCommand.
 */
class FlushPackCommand extends Command
{
    use LockableTrait;

    protected static $defaultName = 'generator:pack:flush';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var string
     */
    private $uploadDir;

    /**
     * @var string
     */
    private $checkBalanceAtInterval;

    /**
     * @var DebtorService
     */
    private $webSupgDebtorService;

    /**
     * @param EntityManagerInterface $em
     * @param FormFactoryInterface   $formFactory
     * @param DebtorService          $webSupgDebtorService
     * @param string                 $uploadDir
     * @param string                 $checkBalanceAtInterval
     */
    public function __construct(
        EntityManagerInterface $em,
        FormFactoryInterface $formFactory,
        DebtorService $webSupgDebtorService,
        string $uploadDir,
        string $checkBalanceAtInterval
    ) {
        $this->em = $em;
        $this->formFactory = $formFactory;
        $this->webSupgDebtorService = $webSupgDebtorService;
        $this->uploadDir = $uploadDir;
        $this->checkBalanceAtInterval = $checkBalanceAtInterval;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Сохранить/обновить данные в бд из пакета')
            ->addOption('ver', null, InputOption::VALUE_OPTIONAL, 'Версия пакета.')
            ->addOption('count', 'c', InputOption::VALUE_REQUIRED, 'Количество строк', 1000)
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     * @throws \Throwable
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return;
        }

        $twig = new \Twig_Environment(new \Twig_Loader_Array());

        $count = (int) $input->getOption('count');
        $pack = null;
        if ($version = $input->getOption('ver')) {
            $pack = $this->em->getRepository(GeneratorPackInfo::class)->find($version);
        }

        if (!$pack instanceof GeneratorPackInfo) {
            $pack = $this->em->getRepository(GeneratorPackInfo::class)->findOneBy(
                [
                    'loaded' => true,
                    'unpacked' => true,
                    'processed' => false,
                ],
                [
                    'createdAt' => 'DESC',
                ]
            );
        }

        if ($pack instanceof GeneratorPackInfo) {
            $progressBar = new ProgressBar($output, $count);
            $datetime = (new \DateTime())->format('Y-m-d');
            $progressBar->setFormat("$datetime - %current%/%max% | %percent:3s%% | %estimated:-6s% | %memory:6s%");
            $malicious = $this->em->getRepository(Cluster::class)->find(Cluster::MALICIOUS_CLUSTER);
            $ordinary = $this->em->getRepository(Cluster::class)->find(Cluster::ORDINARY_CLUSTER);
            $debtorAsDebtorEvent = $this->em->getRepository(Event::class)->find(Event::DEBTOR_AS_DEBTOR);
            $repository = $this->em->getRepository(Debtor::class);
            $pack->setUploadDir($this->uploadDir);
            $csv = $pack->getFileFullPathByFormat('csv');

            if (!\file_exists($csv)) {
                throw new \UnexpectedValueException("Could not open file: $csv");
            }

            $file = new \SplFileObject($csv, 'a+');
            $file->setFlags(7);
            $file->flock(LOCK_EX);

            $temp = new \SplTempFileObject(0);
            $temp->flock(LOCK_EX);

            $meta = $pack->getMetadata();

            $debtorsPackage = [];
            $batchSize = 20;
            $saupgPacketBatchSize = 100;
            $progressBar->start();
            foreach ($file as $i => $line) {
                if ($i === 0) {
                    $temp->fwrite($line.PHP_EOL);

                    continue;
                }
                if ($count >= $i) {
                    try {
                        $data = \array_combine($meta, \str_getcsv($line));

                        $debtor = $repository->find($data['abonCode']);
                        $form = $this->formFactory
                            ->create(
                                DebtorType::class,
                                null === $debtor ? null : clone $debtor,
                                ['pack' => $pack]
                            )
                            ->submit($data, true);
                        /** @var $data Debtor * */
                        $data = $form->getData();
                        $data
                            ->setCheckBalanceAt(new \DateTime($this->checkBalanceAtInterval))
                            ->setEmailChannel(false)
                            ->setSmsChannel(false)
                            ->setCrmChannel(false);

                        if ($debtor instanceof Debtor) {
                            $message = (new DebtEventMessage())
                                ->setParent($debtor)
                                ->setEvent($debtorAsDebtorEvent)
                            ;

                            $message->setMessage(
                                $twig->createTemplate($debtorAsDebtorEvent->getTpl())->render(
                                    $message->getArrayVariables()
                                )
                            );
                            $this->em->persist($message);
                        }

                        null === $debtor ? $this->em->persist($data) : $this->em->merge($data);

                        \array_push($debtorsPackage, $data);

                        if (($i % $batchSize) === 0) {
                            $this->em->flush();
                        }

                        if (($i % $saupgPacketBatchSize) === 0) {
                            // $this->webSupgDebtorService->getSaubgDataByPack($debtorsPackage);
                            $this->em->flush();
                            $progressBar->advance(\count($debtorsPackage));
                            $debtorsPackage = [];
                        }
                    } catch (\Exception $e) {
                        $output->writeln($e->getMessage());
                        $temp->fwrite($line.PHP_EOL);
                    }

                    continue;
                }

                $temp->fwrite($line.PHP_EOL);
            }

            if ($debtorsPackage) {
                //$this->webSupgDebtorService->getSaubgDataByPack($debtorsPackage);
                $this->em->flush();
                $progressBar->advance(\count($debtorsPackage));
            }

            $file->ftruncate(0);
            foreach ($temp as $line) {
                $file->fwrite($line);
            }

            $file->seek($file->getSize());

            if ($pack->getInitRowsCount()) {
                $this->em->persist($pack->setProcessedRowsCount($pack->getInitRowsCount() - $file->key() + 1));
            }

            if (1 === $file->key()) {
                $this->em->persist($pack->setProcessed(true));
            }

            $this->em->flush();

            $temp->flock(LOCK_UN);
            $file->flock(LOCK_UN);
        }

        $this->release();
    }
}
