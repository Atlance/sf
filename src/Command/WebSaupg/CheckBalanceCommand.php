<?php

declare(strict_types=1);

namespace App\Command\WebSaupg;

use App\Service\WebSaupg\DebtorService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CheckBalanceCommand.
 */
class CheckBalanceCommand extends Command
{
    use LockableTrait;

    protected static $defaultName = 'web-saupg:check-balance';

    /**
     * @var DebtorService
     */
    private $saupgDebtorService;

    /**
     * @param DebtorService $saupgDebtorService
     */
    public function __construct(DebtorService $saupgDebtorService)
    {
        $this->saupgDebtorService = $saupgDebtorService;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription(
                'Взять из БД должников на дату проверки баланса `checkBalanceAt`.
                Проверить в Веб-САУПГ баланс должников.
                Если они всё ещё должники уведомить должников.
                Обновить `checkBalanceAt` инкрементировать `checkBalanceCount`'
            )
            ->addArgument('date', InputArgument::OPTIONAL, 'Дата в формате Y-m-d', (new \DateTime())->format('Y-m-d'))
            ->addOption('ver', null, InputOption::VALUE_OPTIONAL, 'Версия пакета.')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return;
        }

        $date = new \DateTime($input->getArgument('date'));
        $version = $input->getOption('ver');

        try {
            $this->saupgDebtorService->checkBalanceByDate($date, $version, 1, 100, $output);
        } catch (\Exception $e) {
            $output->writeln('<error>'.$e->getMessage().'</error>');
        }

        $output->writeln('');

        $this->release();
    }
}
