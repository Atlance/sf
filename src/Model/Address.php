<?php

declare(strict_types=1);

namespace App\Model;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Address.
 */
class Address
{
    /**
     * Страна.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $country;

    /**
     * Индекс
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $postalCode;

    /**
     * Код ФИАС региона.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $regionFiasId;

    /**
     * Код КЛАДР региона.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $regionKladrId;

    /**
     * Регион с типом
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $regionWithType;

    /**
     * Тип региона (сокращенный).
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $regionType;

    /**
     * Тип региона.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $regionTypeFull;

    /**
     * Регион.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $region;

    /**
     * Код ФИАС района в регионе.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $areaFiasId;

    /**
     * Код КЛАДР района в регионе.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $areaKladrId;

    /**
     * Район в регионе с типом
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $areaWithType;

    /**
     * Тип района в регионе (сокращенный).
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $areaType;

    /**
     * Тип района в регионе.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $areaTypeFull;

    /**
     * Район в регионе.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $area;

    /**
     * Код ФИАС города.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $cityFiasId;

    /**
     * Код КЛАДР города.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $cityKladrId;

    /**
     * Город с типом
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $cityWithType;

    /**
     * Тип города (сокращенный).
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $cityType;

    /**
     * Тип города.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $cityTypeFull;

    /**
     * Город.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $city;

    /**
     * Административный округ (только для Москвы).
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $cityArea;

    /**
     * Код ФИАС района города (заполняется, только если район есть в ФИАС).
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $cityDistrictFiasId;

    /**
     * Код КЛАДР района города (не заполняется).
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $cityDistrictKladrId;

    /**
     * Район города с типом
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $cityDistrictWithType;

    /**
     * Тип района города (сокращенный).
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $cityDistrictType;

    /**
     * Тип района города.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $cityDistrictTypeFull;

    /**
     * Район города.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $cityDistrict;

    /**
     * Код ФИАС нас. пункта.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $settlementFiasId;

    /**
     * Код КЛАДР нас. пункта.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $settlementKladrId;

    /**
     * Населенный пункт с типом
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $settlementWithType;

    /**
     * Тип населенного пункта (сокращенный).
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $settlementType;

    /**
     * Тип населенного пункта.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $settlementTypeFull;

    /**
     * Населенный пункт
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $settlement;

    /**
     * Код ФИАС улицы.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $streetFiasId;

    /**
     * Код КЛАДР улицы.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $streetKladrId;

    /**
     * Улица с типом
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $streetWithType;

    /**
     * Тип улицы (сокращенный).
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $streetType;

    /**
     * Тип улицы.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $streetTypeFull;

    /**
     * Улица.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $street;

    /**
     * Код ФИАС дома.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $houseFiasId;

    /**
     * Код КЛАДР дома.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $houseKladrId;

    /**
     * Тип дома (сокращенный).
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $houseType;

    /**
     * Тип дома.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $houseTypeFull;

    /**
     * Дом
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $house;

    /**
     * Тип корпуса/строения (сокращенный).
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $blockType;

    /**
     * Тип корпуса/строения.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $blockTypeFull;

    /**
     * Корпус/строение.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $block;

    /**
     * Тип квартиры (сокращенный).
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $flatType;

    /**
     * Тип квартиры.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $flatTypeFull;

    /**
     * Квартира.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $flat;

    /**
     * Площадь квартиры.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $flatArea;

    /**
     * Рыночная стоимость м².
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $squareMeterPrice;

    /**
     * Рыночная стоимость квартиры.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $flatPrice;

    /**
     * Абонентский ящик.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $postalBox;

    /**
     * Код ФИАС
     *  HOUSE.HOUSEGUID — если дом найден в ФИАС по точному совпадению;
     *  HOUSEINT.INTGUID — если дом найден в ФИАС как часть интервала;
     *  ADDROBJ.AOGUID — в противном случае;.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $fiasId;

    /**
     * Уровень детализации, до которого адрес найден в ФИАС
     *  0 — страна;
     *  1 — регион;
     *  3 — район;
     *  4 — город;
     *  5 — район города;
     *  6 — населенный пункт;
     *  7 — улица;
     *  8 — дом;
     *  90 — доп. территория;
     *  91 — улица в доп. территории;
     *  -1 — иностранный или пустой;.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $fiasLevel;

    /**
     * Код КЛАДР.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $kladrId;

    /**
     * Признак центра района или региона
     *  1 — центр района (Московская обл, Одинцовский р-н, г Одинцово);
     *  2 — центр региона (Новосибирская обл, г Новосибирск);
     *  3 — центр района и региона (Томская обл, г Томск);
     *  4 — центральный район региона (Тюменская обл, Тюменский р-н);
     *  0 — ничего из перечисленного (Московская обл, г Балашиха);.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $capitalMarker;

    /**
     * Код ОКАТО.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $okato;

    /**
     * Код ОКТМО.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $oktmo;

    /**
     * Код ИФНС для физических лиц.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $taxOffice;

    /**
     * Код ИФНС для организаций (не заполняется).
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $taxOfficeLegal;

    /**
     * Часовой пояс
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $timezone;

    /**
     * Координаты: широта.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $geoLat;

    /**
     * Координаты: долгота.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $geoLon;

    /**
     * Внутри кольцевой?
     *  IN_MKAD — внутри МКАД (Москва);
     *  OUT_MKAD — за МКАД (Москва или Московская область);
     *  IN_KAD — внутри КАД (Санкт-Петербург);
     *  OUT_KAD — за КАД (Санкт-Петербург или Ленинградская область);
     *  пусто — в остальных случаях;.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $beltwayHit;

    /**
     * Расстояние от кольцевой в км.
     *  Заполнено, только если beltway_hit = OUT_MKAD или OUT_KAD, иначе пустое.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $beltwayDistance;

    /**
     * Код точности координат
     *  0 — Точные координаты
     *  1 — Ближайший дом
     *  2 — Улица
     *  3 — Населенный пункт
     *  4 — Город
     *  5 — Координаты не определены.
     *
     * @var int
     * @JMS\Type("integer")
     */
    protected $qcGeo;

    /**
     * Код пригодности к рассылке
     *  0 — Да — Пригоден для почтовой рассылки
     *  10 — Под вопросом — Дома нет в ФИАС
     *  5 — Под вопросом — Нет квартиры. Подходит для юридических лиц или частных владений
     *  8 — Под вопросом — До почтового отделения — абонентский ящик или адрес до востребования. Подходит для писем, но не для курьерской доставки.
     *  9 — Под вопросом — Сначала проверьте, правильно ли разобран исходный адрес
     *  1 — Нет — Нет региона
     *  2 — Нет — Нет города
     *  3 — Нет — Нет улицы
     *  4 — Нет — Нет дома
     *  6 — Нет — Адрес неполный
     *  7 — Нет — Иностранный адрес
     *
     * @var int
     * @JMS\Type("integer")
     */
    protected $qcComplete;

    /**
     * Признак наличия дома в ФИАС
     *  2 — Высокая — Дом найден в ФИАС по точному совпадению
     *  3 — Средняя — В ФИАС найден похожий дом; различие в литере, корпусе или строении
     *  4 — Средняя — Дом найден в ФИАС по диапазону
     *  10 — Низкая — Дом не найден в ФИАС
     *
     * @var int
     * @JMS\Type("integer")
     */
    protected $qcHouse;

    /**
     * Код проверки адреса
     *  0 — Адрес распознан уверенно
     *  2 — Адрес пустой или заведомо «мусорный»
     *  1 — Адрес распознан с допущениями или не распознан.
     *
     * @var int
     * @JMS\Type("integer")
     */
    protected $qc;

    /**
     * Нераспознанная часть адреса. Для адреса.
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $unparsedParts;

    /**
     * Get Country.
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set Country.
     *
     * @param string $country
     *
     * @return Address
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get PostalCode.
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set PostalCode.
     *
     * @param string $postalCode
     *
     * @return Address
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get RegionFiasId.
     *
     * @return string
     */
    public function getRegionFiasId()
    {
        return $this->regionFiasId;
    }

    /**
     * Set RegionFiasId.
     *
     * @param string $regionFiasId
     *
     * @return Address
     */
    public function setRegionFiasId($regionFiasId)
    {
        $this->regionFiasId = $regionFiasId;

        return $this;
    }

    /**
     * Get RegionKladrId.
     *
     * @return string
     */
    public function getRegionKladrId()
    {
        return $this->regionKladrId;
    }

    /**
     * Set RegionKladrId.
     *
     * @param string $regionKladrId
     *
     * @return Address
     */
    public function setRegionKladrId($regionKladrId)
    {
        $this->regionKladrId = $regionKladrId;

        return $this;
    }

    /**
     * Get RegionWithType.
     *
     * @return string
     */
    public function getRegionWithType()
    {
        return $this->regionWithType;
    }

    /**
     * Set RegionWithType.
     *
     * @param string $regionWithType
     *
     * @return Address
     */
    public function setRegionWithType($regionWithType)
    {
        $this->regionWithType = $regionWithType;

        return $this;
    }

    /**
     * Get RegionType.
     *
     * @return string
     */
    public function getRegionType()
    {
        return $this->regionType;
    }

    /**
     * Set RegionType.
     *
     * @param string $regionType
     *
     * @return Address
     */
    public function setRegionType($regionType)
    {
        $this->regionType = $regionType;

        return $this;
    }

    /**
     * Get RegionTypeFull.
     *
     * @return string
     */
    public function getRegionTypeFull()
    {
        return $this->regionTypeFull;
    }

    /**
     * Set RegionTypeFull.
     *
     * @param string $regionTypeFull
     *
     * @return Address
     */
    public function setRegionTypeFull($regionTypeFull)
    {
        $this->regionTypeFull = $regionTypeFull;

        return $this;
    }

    /**
     * Get Region.
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set Region.
     *
     * @param string $region
     *
     * @return Address
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get AreaFiasId.
     *
     * @return string
     */
    public function getAreaFiasId()
    {
        return $this->areaFiasId;
    }

    /**
     * Set AreaFiasId.
     *
     * @param string $areaFiasId
     *
     * @return Address
     */
    public function setAreaFiasId($areaFiasId)
    {
        $this->areaFiasId = $areaFiasId;

        return $this;
    }

    /**
     * Get AreaKladrId.
     *
     * @return string
     */
    public function getAreaKladrId()
    {
        return $this->areaKladrId;
    }

    /**
     * Set AreaKladrId.
     *
     * @param string $areaKladrId
     *
     * @return Address
     */
    public function setAreaKladrId($areaKladrId)
    {
        $this->areaKladrId = $areaKladrId;

        return $this;
    }

    /**
     * Get AreaWithType.
     *
     * @return string
     */
    public function getAreaWithType()
    {
        return $this->areaWithType;
    }

    /**
     * Set AreaWithType.
     *
     * @param string $areaWithType
     *
     * @return Address
     */
    public function setAreaWithType($areaWithType)
    {
        $this->areaWithType = $areaWithType;

        return $this;
    }

    /**
     * Get AreaType.
     *
     * @return string
     */
    public function getAreaType()
    {
        return $this->areaType;
    }

    /**
     * Set AreaType.
     *
     * @param string $areaType
     *
     * @return Address
     */
    public function setAreaType($areaType)
    {
        $this->areaType = $areaType;

        return $this;
    }

    /**
     * Get AreaTypeFull.
     *
     * @return string
     */
    public function getAreaTypeFull()
    {
        return $this->areaTypeFull;
    }

    /**
     * Set AreaTypeFull.
     *
     * @param string $areaTypeFull
     *
     * @return Address
     */
    public function setAreaTypeFull($areaTypeFull)
    {
        $this->areaTypeFull = $areaTypeFull;

        return $this;
    }

    /**
     * Get Area.
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set Area.
     *
     * @param string $area
     *
     * @return Address
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get CityFiasId.
     *
     * @return string
     */
    public function getCityFiasId()
    {
        return $this->cityFiasId;
    }

    /**
     * Set CityFiasId.
     *
     * @param string $cityFiasId
     *
     * @return Address
     */
    public function setCityFiasId($cityFiasId)
    {
        $this->cityFiasId = $cityFiasId;

        return $this;
    }

    /**
     * Get CityKladrId.
     *
     * @return string
     */
    public function getCityKladrId()
    {
        return $this->cityKladrId;
    }

    /**
     * Set CityKladrId.
     *
     * @param string $cityKladrId
     *
     * @return Address
     */
    public function setCityKladrId($cityKladrId)
    {
        $this->cityKladrId = $cityKladrId;

        return $this;
    }

    /**
     * Get CityWithType.
     *
     * @return string
     */
    public function getCityWithType()
    {
        return $this->cityWithType;
    }

    /**
     * Set CityWithType.
     *
     * @param string $cityWithType
     *
     * @return Address
     */
    public function setCityWithType($cityWithType)
    {
        $this->cityWithType = $cityWithType;

        return $this;
    }

    /**
     * Get CityType.
     *
     * @return string
     */
    public function getCityType()
    {
        return $this->cityType;
    }

    /**
     * Set CityType.
     *
     * @param string $cityType
     *
     * @return Address
     */
    public function setCityType($cityType)
    {
        $this->cityType = $cityType;

        return $this;
    }

    /**
     * Get CityTypeFull.
     *
     * @return string
     */
    public function getCityTypeFull()
    {
        return $this->cityTypeFull;
    }

    /**
     * Set CityTypeFull.
     *
     * @param string $cityTypeFull
     *
     * @return Address
     */
    public function setCityTypeFull($cityTypeFull)
    {
        $this->cityTypeFull = $cityTypeFull;

        return $this;
    }

    /**
     * Get City.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set City.
     *
     * @param string $city
     *
     * @return Address
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get CityArea.
     *
     * @return string
     */
    public function getCityArea()
    {
        return $this->cityArea;
    }

    /**
     * Set CityArea.
     *
     * @param string $cityArea
     *
     * @return Address
     */
    public function setCityArea($cityArea)
    {
        $this->cityArea = $cityArea;

        return $this;
    }

    /**
     * Get CityDistrictFiasId.
     *
     * @return string
     */
    public function getCityDistrictFiasId()
    {
        return $this->cityDistrictFiasId;
    }

    /**
     * Set CityDistrictFiasId.
     *
     * @param string $cityDistrictFiasId
     *
     * @return Address
     */
    public function setCityDistrictFiasId($cityDistrictFiasId)
    {
        $this->cityDistrictFiasId = $cityDistrictFiasId;

        return $this;
    }

    /**
     * Get CityDistrictKladrId.
     *
     * @return string
     */
    public function getCityDistrictKladrId()
    {
        return $this->cityDistrictKladrId;
    }

    /**
     * Set CityDistrictKladrId.
     *
     * @param string $cityDistrictKladrId
     *
     * @return Address
     */
    public function setCityDistrictKladrId($cityDistrictKladrId)
    {
        $this->cityDistrictKladrId = $cityDistrictKladrId;

        return $this;
    }

    /**
     * Get CityDistrictWithType.
     *
     * @return string
     */
    public function getCityDistrictWithType()
    {
        return $this->cityDistrictWithType;
    }

    /**
     * Set CityDistrictWithType.
     *
     * @param string $cityDistrictWithType
     *
     * @return Address
     */
    public function setCityDistrictWithType($cityDistrictWithType)
    {
        $this->cityDistrictWithType = $cityDistrictWithType;

        return $this;
    }

    /**
     * Get CityDistrictType.
     *
     * @return string
     */
    public function getCityDistrictType()
    {
        return $this->cityDistrictType;
    }

    /**
     * Set CityDistrictType.
     *
     * @param string $cityDistrictType
     *
     * @return Address
     */
    public function setCityDistrictType($cityDistrictType)
    {
        $this->cityDistrictType = $cityDistrictType;

        return $this;
    }

    /**
     * Get CityDistrictTypeFull.
     *
     * @return string
     */
    public function getCityDistrictTypeFull()
    {
        return $this->cityDistrictTypeFull;
    }

    /**
     * Set CityDistrictTypeFull.
     *
     * @param string $cityDistrictTypeFull
     *
     * @return Address
     */
    public function setCityDistrictTypeFull($cityDistrictTypeFull)
    {
        $this->cityDistrictTypeFull = $cityDistrictTypeFull;

        return $this;
    }

    /**
     * Get CityDistrict.
     *
     * @return string
     */
    public function getCityDistrict()
    {
        return $this->cityDistrict;
    }

    /**
     * Set CityDistrict.
     *
     * @param string $cityDistrict
     *
     * @return Address
     */
    public function setCityDistrict($cityDistrict)
    {
        $this->cityDistrict = $cityDistrict;

        return $this;
    }

    /**
     * Get SettlementFiasId.
     *
     * @return string
     */
    public function getSettlementFiasId()
    {
        return $this->settlementFiasId;
    }

    /**
     * Set SettlementFiasId.
     *
     * @param string $settlementFiasId
     *
     * @return Address
     */
    public function setSettlementFiasId($settlementFiasId)
    {
        $this->settlementFiasId = $settlementFiasId;

        return $this;
    }

    /**
     * Get SettlementKladrId.
     *
     * @return string
     */
    public function getSettlementKladrId()
    {
        return $this->settlementKladrId;
    }

    /**
     * Set SettlementKladrId.
     *
     * @param string $settlementKladrId
     *
     * @return Address
     */
    public function setSettlementKladrId($settlementKladrId)
    {
        $this->settlementKladrId = $settlementKladrId;

        return $this;
    }

    /**
     * Get SettlementWithType.
     *
     * @return string
     */
    public function getSettlementWithType()
    {
        return $this->settlementWithType;
    }

    /**
     * Set SettlementWithType.
     *
     * @param string $settlementWithType
     *
     * @return Address
     */
    public function setSettlementWithType($settlementWithType)
    {
        $this->settlementWithType = $settlementWithType;

        return $this;
    }

    /**
     * Get SettlementType.
     *
     * @return string
     */
    public function getSettlementType()
    {
        return $this->settlementType;
    }

    /**
     * Set SettlementType.
     *
     * @param string $settlementType
     *
     * @return Address
     */
    public function setSettlementType($settlementType)
    {
        $this->settlementType = $settlementType;

        return $this;
    }

    /**
     * Get SettlementTypeFull.
     *
     * @return string
     */
    public function getSettlementTypeFull()
    {
        return $this->settlementTypeFull;
    }

    /**
     * Set SettlementTypeFull.
     *
     * @param string $settlementTypeFull
     *
     * @return Address
     */
    public function setSettlementTypeFull($settlementTypeFull)
    {
        $this->settlementTypeFull = $settlementTypeFull;

        return $this;
    }

    /**
     * Get Settlement.
     *
     * @return string
     */
    public function getSettlement()
    {
        return $this->settlement;
    }

    /**
     * Set Settlement.
     *
     * @param string $settlement
     *
     * @return Address
     */
    public function setSettlement($settlement)
    {
        $this->settlement = $settlement;

        return $this;
    }

    /**
     * Get StreetFiasId.
     *
     * @return string
     */
    public function getStreetFiasId()
    {
        return $this->streetFiasId;
    }

    /**
     * Set StreetFiasId.
     *
     * @param string $streetFiasId
     *
     * @return Address
     */
    public function setStreetFiasId($streetFiasId)
    {
        $this->streetFiasId = $streetFiasId;

        return $this;
    }

    /**
     * Get StreetKladrId.
     *
     * @return string
     */
    public function getStreetKladrId()
    {
        return $this->streetKladrId;
    }

    /**
     * Set StreetKladrId.
     *
     * @param string $streetKladrId
     *
     * @return Address
     */
    public function setStreetKladrId($streetKladrId)
    {
        $this->streetKladrId = $streetKladrId;

        return $this;
    }

    /**
     * Get StreetWithType.
     *
     * @return string
     */
    public function getStreetWithType()
    {
        return $this->streetWithType;
    }

    /**
     * Set StreetWithType.
     *
     * @param string $streetWithType
     *
     * @return Address
     */
    public function setStreetWithType($streetWithType)
    {
        $this->streetWithType = $streetWithType;

        return $this;
    }

    /**
     * Get StreetType.
     *
     * @return string
     */
    public function getStreetType()
    {
        return $this->streetType;
    }

    /**
     * Set StreetType.
     *
     * @param string $streetType
     *
     * @return Address
     */
    public function setStreetType($streetType)
    {
        $this->streetType = $streetType;

        return $this;
    }

    /**
     * Get StreetTypeFull.
     *
     * @return string
     */
    public function getStreetTypeFull()
    {
        return $this->streetTypeFull;
    }

    /**
     * Set StreetTypeFull.
     *
     * @param string $streetTypeFull
     *
     * @return Address
     */
    public function setStreetTypeFull($streetTypeFull)
    {
        $this->streetTypeFull = $streetTypeFull;

        return $this;
    }

    /**
     * Get Street.
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set Street.
     *
     * @param string $street
     *
     * @return Address
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get HouseFiasId.
     *
     * @return string
     */
    public function getHouseFiasId()
    {
        return $this->houseFiasId;
    }

    /**
     * Set HouseFiasId.
     *
     * @param string $houseFiasId
     *
     * @return Address
     */
    public function setHouseFiasId($houseFiasId)
    {
        $this->houseFiasId = $houseFiasId;

        return $this;
    }

    /**
     * Get HouseKladrId.
     *
     * @return string
     */
    public function getHouseKladrId()
    {
        return $this->houseKladrId;
    }

    /**
     * Set HouseKladrId.
     *
     * @param string $houseKladrId
     *
     * @return Address
     */
    public function setHouseKladrId($houseKladrId)
    {
        $this->houseKladrId = $houseKladrId;

        return $this;
    }

    /**
     * Get HouseType.
     *
     * @return string
     */
    public function getHouseType()
    {
        return $this->houseType;
    }

    /**
     * Set HouseType.
     *
     * @param string $houseType
     *
     * @return Address
     */
    public function setHouseType($houseType)
    {
        $this->houseType = $houseType;

        return $this;
    }

    /**
     * Get HouseTypeFull.
     *
     * @return string
     */
    public function getHouseTypeFull()
    {
        return $this->houseTypeFull;
    }

    /**
     * Set HouseTypeFull.
     *
     * @param string $houseTypeFull
     *
     * @return Address
     */
    public function setHouseTypeFull($houseTypeFull)
    {
        $this->houseTypeFull = $houseTypeFull;

        return $this;
    }

    /**
     * Get House.
     *
     * @return string
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * Set House.
     *
     * @param string $house
     *
     * @return Address
     */
    public function setHouse($house)
    {
        $this->house = $house;

        return $this;
    }

    /**
     * Get BlockType.
     *
     * @return string
     */
    public function getBlockType()
    {
        return $this->blockType;
    }

    /**
     * Set BlockType.
     *
     * @param string $blockType
     *
     * @return Address
     */
    public function setBlockType($blockType)
    {
        $this->blockType = $blockType;

        return $this;
    }

    /**
     * Get BlockTypeFull.
     *
     * @return string
     */
    public function getBlockTypeFull()
    {
        return $this->blockTypeFull;
    }

    /**
     * Set BlockTypeFull.
     *
     * @param string $blockTypeFull
     *
     * @return Address
     */
    public function setBlockTypeFull($blockTypeFull)
    {
        $this->blockTypeFull = $blockTypeFull;

        return $this;
    }

    /**
     * Get Block.
     *
     * @return string
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * Set Block.
     *
     * @param string $block
     *
     * @return Address
     */
    public function setBlock($block)
    {
        $this->block = $block;

        return $this;
    }

    /**
     * Get FlatType.
     *
     * @return string
     */
    public function getFlatType()
    {
        return $this->flatType;
    }

    /**
     * Set FlatType.
     *
     * @param string $flatType
     *
     * @return Address
     */
    public function setFlatType($flatType)
    {
        $this->flatType = $flatType;

        return $this;
    }

    /**
     * Get FlatTypeFull.
     *
     * @return string
     */
    public function getFlatTypeFull()
    {
        return $this->flatTypeFull;
    }

    /**
     * Set FlatTypeFull.
     *
     * @param string $flatTypeFull
     *
     * @return Address
     */
    public function setFlatTypeFull($flatTypeFull)
    {
        $this->flatTypeFull = $flatTypeFull;

        return $this;
    }

    /**
     * Get Flat.
     *
     * @return string
     */
    public function getFlat()
    {
        return $this->flat;
    }

    /**
     * Set Flat.
     *
     * @param string $flat
     *
     * @return Address
     */
    public function setFlat($flat)
    {
        $this->flat = $flat;

        return $this;
    }

    /**
     * Get FlatArea.
     *
     * @return string
     */
    public function getFlatArea()
    {
        return $this->flatArea;
    }

    /**
     * Set FlatArea.
     *
     * @param string $flatArea
     *
     * @return Address
     */
    public function setFlatArea($flatArea)
    {
        $this->flatArea = $flatArea;

        return $this;
    }

    /**
     * Get SquareMeterPrice.
     *
     * @return string
     */
    public function getSquareMeterPrice()
    {
        return $this->squareMeterPrice;
    }

    /**
     * Set SquareMeterPrice.
     *
     * @param string $squareMeterPrice
     *
     * @return Address
     */
    public function setSquareMeterPrice($squareMeterPrice)
    {
        $this->squareMeterPrice = $squareMeterPrice;

        return $this;
    }

    /**
     * Get FlatPrice.
     *
     * @return string
     */
    public function getFlatPrice()
    {
        return $this->flatPrice;
    }

    /**
     * Set FlatPrice.
     *
     * @param string $flatPrice
     *
     * @return Address
     */
    public function setFlatPrice($flatPrice)
    {
        $this->flatPrice = $flatPrice;

        return $this;
    }

    /**
     * Get PostalBox.
     *
     * @return string
     */
    public function getPostalBox()
    {
        return $this->postalBox;
    }

    /**
     * Set PostalBox.
     *
     * @param string $postalBox
     *
     * @return Address
     */
    public function setPostalBox($postalBox)
    {
        $this->postalBox = $postalBox;

        return $this;
    }

    /**
     * Get FiasId.
     *
     * @return string
     */
    public function getFiasId()
    {
        return $this->fiasId;
    }

    /**
     * Set FiasId.
     *
     * @param string $fiasId
     *
     * @return Address
     */
    public function setFiasId($fiasId)
    {
        $this->fiasId = $fiasId;

        return $this;
    }

    /**
     * Get FiasLevel.
     *
     * @return string
     */
    public function getFiasLevel()
    {
        return $this->fiasLevel;
    }

    /**
     * Set FiasLevel.
     *
     * @param string $fiasLevel
     *
     * @return Address
     */
    public function setFiasLevel($fiasLevel)
    {
        $this->fiasLevel = $fiasLevel;

        return $this;
    }

    /**
     * Get KladrId.
     *
     * @return string
     */
    public function getKladrId()
    {
        return $this->kladrId;
    }

    /**
     * Set KladrId.
     *
     * @param string $kladrId
     *
     * @return Address
     */
    public function setKladrId($kladrId)
    {
        $this->kladrId = $kladrId;

        return $this;
    }

    /**
     * Get CapitalMarker.
     *
     * @return string
     */
    public function getCapitalMarker()
    {
        return $this->capitalMarker;
    }

    /**
     * Set CapitalMarker.
     *
     * @param string $capitalMarker
     *
     * @return Address
     */
    public function setCapitalMarker($capitalMarker)
    {
        $this->capitalMarker = $capitalMarker;

        return $this;
    }

    /**
     * Get Okato.
     *
     * @return string
     */
    public function getOkato()
    {
        return $this->okato;
    }

    /**
     * Set Okato.
     *
     * @param string $okato
     *
     * @return Address
     */
    public function setOkato($okato)
    {
        $this->okato = $okato;

        return $this;
    }

    /**
     * Get Oktmo.
     *
     * @return string
     */
    public function getOktmo()
    {
        return $this->oktmo;
    }

    /**
     * Set Oktmo.
     *
     * @param string $oktmo
     *
     * @return Address
     */
    public function setOktmo($oktmo)
    {
        $this->oktmo = $oktmo;

        return $this;
    }

    /**
     * Get TaxOffice.
     *
     * @return string
     */
    public function getTaxOffice()
    {
        return $this->taxOffice;
    }

    /**
     * Set TaxOffice.
     *
     * @param string $taxOffice
     *
     * @return Address
     */
    public function setTaxOffice($taxOffice)
    {
        $this->taxOffice = $taxOffice;

        return $this;
    }

    /**
     * Get TaxOfficeLegal.
     *
     * @return string
     */
    public function getTaxOfficeLegal()
    {
        return $this->taxOfficeLegal;
    }

    /**
     * Set TaxOfficeLegal.
     *
     * @param string $taxOfficeLegal
     *
     * @return Address
     */
    public function setTaxOfficeLegal($taxOfficeLegal)
    {
        $this->taxOfficeLegal = $taxOfficeLegal;

        return $this;
    }

    /**
     * Get Timezone.
     *
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set Timezone.
     *
     * @param string $timezone
     *
     * @return Address
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get GeoLat.
     *
     * @return string
     */
    public function getGeoLat()
    {
        return $this->geoLat;
    }

    /**
     * Set GeoLat.
     *
     * @param string $geoLat
     *
     * @return Address
     */
    public function setGeoLat($geoLat)
    {
        $this->geoLat = $geoLat;

        return $this;
    }

    /**
     * Get GeoLon.
     *
     * @return string
     */
    public function getGeoLon()
    {
        return $this->geoLon;
    }

    /**
     * Set GeoLon.
     *
     * @param string $geoLon
     *
     * @return Address
     */
    public function setGeoLon($geoLon)
    {
        $this->geoLon = $geoLon;

        return $this;
    }

    /**
     * Get BeltwayHit.
     *
     * @return string
     */
    public function getBeltwayHit()
    {
        return $this->beltwayHit;
    }

    /**
     * Set BeltwayHit.
     *
     * @param string $beltwayHit
     *
     * @return Address
     */
    public function setBeltwayHit($beltwayHit)
    {
        $this->beltwayHit = $beltwayHit;

        return $this;
    }

    /**
     * Get BeltwayDistance.
     *
     * @return string
     */
    public function getBeltwayDistance()
    {
        return $this->beltwayDistance;
    }

    /**
     * Set BeltwayDistance.
     *
     * @param string $beltwayDistance
     *
     * @return Address
     */
    public function setBeltwayDistance($beltwayDistance)
    {
        $this->beltwayDistance = $beltwayDistance;

        return $this;
    }

    /**
     * Get QcGeo.
     *
     * @return int
     */
    public function getQcGeo()
    {
        return $this->qcGeo;
    }

    /**
     * Set QcGeo.
     *
     * @param int $qcGeo
     *
     * @return Address
     */
    public function setQcGeo($qcGeo)
    {
        $this->qcGeo = $qcGeo;

        return $this;
    }

    /**
     * Get QcComplete.
     *
     * @return int
     */
    public function getQcComplete()
    {
        return $this->qcComplete;
    }

    /**
     * Set QcComplete.
     *
     * @param int $qcComplete
     *
     * @return Address
     */
    public function setQcComplete($qcComplete)
    {
        $this->qcComplete = $qcComplete;

        return $this;
    }

    /**
     * Get QcHouse.
     *
     * @return int
     */
    public function getQcHouse()
    {
        return $this->qcHouse;
    }

    /**
     * Set QcHouse.
     *
     * @param int $qcHouse
     *
     * @return Address
     */
    public function setQcHouse($qcHouse)
    {
        $this->qcHouse = $qcHouse;

        return $this;
    }

    /**
     * Get Qc.
     *
     * @return int
     */
    public function getQc()
    {
        return $this->qc;
    }

    /**
     * Set Qc.
     *
     * @param int $qc
     *
     * @return Address
     */
    public function setQc($qc)
    {
        $this->qc = $qc;

        return $this;
    }

    /**
     * Get UnparsedParts.
     *
     * @return string
     */
    public function getUnparsedParts()
    {
        return $this->unparsedParts;
    }

    /**
     * Set UnparsedParts.
     *
     * @param string $unparsedParts
     *
     * @return Address
     */
    public function setUnparsedParts($unparsedParts)
    {
        $this->unparsedParts = $unparsedParts;

        return $this;
    }
}
