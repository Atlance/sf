<?php

declare(strict_types=1);

namespace App\Form\Pack;

use App\Entity\GeneratorPackInfo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class GeneratorPackInfoType.
 */
class GeneratorPackInfoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('version', TextType::class)
            ->add('metadata', CollectionType::class, [
                'constraints' => [
                    new Callback([
                        'callback' => [$this, 'checkAbonCode'],
                    ]),
                ],
                'entry_type' => TextType::class,
                'allow_add' => true,
                'error_bubbling' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GeneratorPackInfo::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * @param mixed                     $metadata
     * @param ExecutionContextInterface $context
     */
    public function checkAbonCode($metadata, ExecutionContextInterface $context)
    {
        if (!$metadata) {
            return;
        }

        if (!\in_array('abonCode', $metadata)) {
            $context->getObject()->addError(new FormError('`abonCode` must be defined in metadata'));
        }
    }
}
