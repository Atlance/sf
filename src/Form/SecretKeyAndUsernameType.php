<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Constraint;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class SecretKeyAndUsernameType extends AbstractType
{
    /**
     * @var string
     */
    private $secretKey;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     * @param string                 $secretKey
     */
    public function __construct(EntityManagerInterface $em, string $secretKey)
    {
        $this->secretKey = $secretKey;
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Constraint\NotBlank(),
                    new Constraint\Callback([
                        'callback' => [$this, 'checkUsername'],
                    ]),
                ],
            ])
            ->add('X-AUTH-TOKEN', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Constraint\NotBlank(),
                    new Constraint\Callback([
                        'callback' => [$this, 'checkSecretKey'],
                    ]),
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }

    /**
     * @param mixed                     $secretKey
     * @param ExecutionContextInterface $context
     */
    public function checkSecretKey($secretKey, ExecutionContextInterface $context)
    {
        if (!$secretKey) {
            return;
        }

        if ($this->secretKey !== $secretKey) {
            $context->getObject()->addError(new FormError('Не верное значение'));
        }
    }

    /**
     * @param mixed                     $username
     * @param ExecutionContextInterface $context
     */
    public function checkUsername($username, ExecutionContextInterface $context)
    {
        if (!$username) {
            return;
        }

        if (!$this->em->getRepository(User::class)->findOneBy(['username' => $username])) {
            $context->getObject()->addError(new FormError('Такой пользователь не найден'));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
