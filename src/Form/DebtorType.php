<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Cluster;
use App\Entity\Debtor;
use App\Entity\DistrictOperator;
use App\Entity\Filial;
use App\Entity\HouseType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DebtorType.
 */
class DebtorType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('abonCode')
            ->add('filial', EntityType::class, [
                'class' => Filial::class,
            ])
            ->add('cluster', EntityType::class, [
                'class' => Cluster::class,
            ])
            ->add('saldoGaz', TextType::class)
            ->add('saldoDateGaz', DateType::class, [
                'empty_data' => null,
                'widget' => 'single_text',
                'format' => DateType::HTML5_FORMAT,
            ])
            ->add('saldoTo', TextType::class)
            ->add('saldoDateTo', DateType::class, [
                'empty_data' => null,
                'widget' => 'single_text',
                'format' => DateType::HTML5_FORMAT,
            ])
            ->add('pack', TextType::class, [
                'empty_data' => $options['pack'],
            ])
            ->add('districtOperator', EntityType::class, [
                'class' => DistrictOperator::class,
            ])
            ->add('houseType', EntityType::class, [
                'class' => HouseType::class,
            ])
            ->add('houseFiasGuid', TextType::class)
            ->add('streetFiasGuid', TextType::class)
            ->add('cityFiasGuid', TextType::class)
            ->add('houseFiasGuid', TextType::class)
            ->add('gprs', CheckboxType::class)
            ->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'onPreSubmit'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Debtor::class,
            'csrf_protection' => false,
            'pack' => null,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * @param FormEvent $event
     *
     * @throws \Exception
     */
    public function onPreSubmit(FormEvent $event)
    {
        if ($data = $event->getData()) {
            if (\array_key_exists('filialId', $data)) {
                $data['filial'] = $data['filialId'];
                unset($data['filialId']);
            }

            if ($data['saldoGaz'] && $data['saldoGaz'][0] === '.') {
                $data['saldoGaz'] = "0{$data['saldoGaz']}";
            }

            if ($data['saldoTo'] && $data['saldoTo'][0] === '.') {
                $data['saldoTo'] = "0{$data['saldoTo']}";
            }

            if (\array_key_exists('idOtdel', $data)) {
                $data['districtOperator'] = $data['idOtdel'];
                unset($data['idOtdel']);
            }

            if (\array_key_exists('idKathouse', $data)) {
                $data['houseType'] = $data['idKathouse'];
                unset($data['idKathouse']);
            }

            if (\array_key_exists('houseguid', $data)) {
                $data['houseFiasGuid'] = $data['houseguid'];
                unset($data['houseguid']);
            }

            if (\array_key_exists('streetguid', $data)) {
                $data['streetFiasGuid'] = $data['streetguid'];
                unset($data['streetguid']);
            }

            if (\array_key_exists('cityguid', $data)) {
                $data['cityFiasGuid'] = $data['cityguid'];
                unset($data['cityguid']);
            }

            if (\array_key_exists('gprs', $data)) {
                $data['gprs'] = $data['gprs'] === 'F';
            }

            $this->setCluster($data);
            $event->setData($data);
        }
    }

    /**
     * @param array $data
     *
     * @throws \Exception
     */
    private function setCluster(array &$data)
    {
        $date2monthsAgo = new \DateTime('now -2 months');
        $saldoDateGaz = new \DateTime($data['saldoDateGaz']);

        if ($date2monthsAgo > $saldoDateGaz) {
            if ((float) $data['saldoGaz'] >= Cluster::MALICIOUS_TYPE) {
                $data['cluster'] = Cluster::MALICIOUS_CLUSTER;
            } else {
                $data['cluster'] = Cluster::ORDINARY_CLUSTER;
            }
        }
    }
}
