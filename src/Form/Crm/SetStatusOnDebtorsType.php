<?php

declare(strict_types=1);

namespace App\Form\Crm;

use App\Entity\CrmStatus;
use App\Entity\Debtor;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class SetStatusOnDebtorsType.
 */
class SetStatusOnDebtorsType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('abon_codes', CollectionType::class, [
                'constraints' => [
                    new NotNull(),
                ],
                'entry_type' => TextType::class,
                'allow_add' => true,
                'error_bubbling' => false,
            ])
            ->add('crm_status', EntityType::class, [
                'class' => CrmStatus::class,
                'choice_label' => 'id',
                'choice_value' => 'id',
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, [$this, 'setCrmStatus'])
        ;

        $builder->get('crm_status')->addViewTransformer(
            new CallbackTransformer(
                function ($v) {
                    return $v ? $v : null;
                },
                function ($v) {
                    return $v ? $v : null;
                }
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * @param FormEvent $event
     *
     * @throws NonUniqueResultException
     */
    public function setCrmStatus(FormEvent $event)
    {
        if ($event->getForm()->isValid()) {
            /** @var null | CrmStatus $crmStatus */
            [$abonCodes, $crmStatus] = \array_values($event->getData());
            $paginator = $this->em->getRepository(Debtor::class)->findByConditions([
                'where' => ['in' => ['debtors_abon_code' => \implode(',', $abonCodes)]],
            ]);

            /** @var Debtor $debtor */
            foreach ($paginator->getItems() as $debtor) {
                $this->em->persist($debtor->setCrmStatus($crmStatus));
            }

            $this->em->flush();
        }
    }
}
