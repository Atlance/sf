<?php

declare(strict_types=1);

namespace App\BranchLoader;

/**
 * Class GitLoader.
 */
class GitLoader
{
    /** @var string|null */
    private $projectDir;

    /**
     * @param string $projectDir
     */
    public function __construct(string $projectDir = null)
    {
        $this->projectDir = $projectDir;
    }

    /**
     * @return string
     */
    public function getBranchName()
    {
        $gitHeadFile = $this->projectDir.'/.git/HEAD';
        $branchname = 'no branch name';

        $stringFromFile = \file_exists($gitHeadFile) ? \file($gitHeadFile, FILE_USE_INCLUDE_PATH) : '';

        if (isset($stringFromFile) && \is_array($stringFromFile)) {
            //get the string from the array
            $firstLine = $stringFromFile[0];
            //seperate out by the "/" in the string
            $explodedString = \explode('/', $firstLine, 3);

            $branchname = \trim($explodedString[2]);
        }

        return $branchname;
    }

    /**
     * @return string
     */
    public function getLastCommitMessage()
    {
        $gitCommitMessageFile = $this->projectDir.'/.git/COMMIT_EDITMSG';
        $commitMessage = \file_exists($gitCommitMessageFile) ? \file($gitCommitMessageFile, FILE_USE_INCLUDE_PATH) : '';

        return \is_array($commitMessage) ? \trim($commitMessage[0]) : '';
    }

    /**
     * @return array
     */
    public function getLastCommitDetail()
    {
        $logs = [];
        $gitLogFile = $this->projectDir.'/.git/logs/HEAD';
        $gitLogs = \file_exists($gitLogFile) ? \file($gitLogFile, FILE_USE_INCLUDE_PATH) : '';

        $logExploded = \explode(' ', \end($gitLogs));
        $logs['author'] = $logExploded[2] ?? 'not defined';
        $logs['date'] = isset($logExploded[4]) ? \date('Y/m/d H:i', (int) $logExploded[5]) : 'not defined';

        return $logs;
    }
}
